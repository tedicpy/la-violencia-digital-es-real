module.exports = {
  siteTitle: 'La Violencia Digital es Real',
  // languages
  defaultLocale: 'es',
  locales: ['es', 'en'],

  colors: {
    'brand-red-1': '#B60000',
    'brand-red-2': '#FF3309',
    'brand-red-3': '#FF9F80',
    'brand-red-4': '#FFEFDA',
    'brand-indigo-1': '#282663',
    'brand-indigo-2': '#4022B9',
    'brand-indigo-3': '#6642F9',
    'brand-indigo-4': '#BBA9FF',
    'brand-indigo-5': '#DAD0FF',
    'brand-green-1': '#038043',
    'brand-green-2': '#00B25B',
    'brand-green-3': '#A1DD9B',
    'brand-green-4': '#CFFECA',
    'brand-pink-1': '#7D023E',
    'brand-pink-2': '#AD0455',
    'brand-pink-3': '#E6006F',
    'brand-pink-4': '#FF91CC',
    'brand-pink-5': '#FFE5F3',
    'brand-blue-1': '#06319E',
    'brand-blue-2': '#2B5CD9',
    'brand-blue-3': '#9BB9F3'
  }
}
