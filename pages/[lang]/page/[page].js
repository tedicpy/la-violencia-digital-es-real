import React from 'react'
import ReactMarkdown from 'react-markdown'
import Layout from '../../../components/layout'
import Header from '../../../components/header'
import Footer from '../../../components/footer'
import withLocale from '../../../lib/translations/withLocale.hoc'
import config from '../../../config'
import { readLocaleStrings } from '../../../lib/translations/readLocaleStrings'
import useTranslation, { getLocaledUrlByLocale, getDefaultUrlByLocale } from '../../../lib/translations/useTranslation.hook'

export function Page({ page }) {
  const { t, getDefaultUrl } = useTranslation()
  const defaultUrl = getDefaultUrl(page)
  
  return (
    <Layout title={t(`common.pages.${defaultUrl}`)}>
      <Header id="header"></Header>
  
      <section>
        <div className="p-12 pt-20 mx-auto prose-lg text-black max-w-screen lg:prose-xl xl:prose-2xl lg:p-28">
          <ReactMarkdown>{t(`${defaultUrl}.content`)}</ReactMarkdown>
        </div>
      </section>

      <Footer id="footer"></Footer>
    </Layout>
  )
}

export async function getStaticPaths () {
  const defaultUrls = [
    'que-es',
    'politica-de-privacidad',
    'terminos-y-condiciones',
    'contactos'
  ]
  const paths = []
  defaultUrls.forEach(page => {
    config.locales.forEach(locale => {
      paths.push({ params: { 
          lang: locale,
          page: getLocaledUrlByLocale(page, locale)
        } 
      })
    })
  })
  return {
    paths, fallback: false
  }
}

export async function getStaticProps ( props ) {
  const requestedPage = getDefaultUrlByLocale(props.params.page, props.params.lang)
  return {
    props: {
      locale: props.params.lang,
      localeStrings: readLocaleStrings(props.params.lang, ['common', requestedPage]),
      page: props.params.page
    }
  }
}

export default withLocale(Page)