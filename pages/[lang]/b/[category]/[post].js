import React from 'react'
import Layout from '../../../../components/layout'
import withLocale from '../../../../lib/translations/withLocale.hoc'
import config from '../../../../config'
import { readLocaleStrings } from '../../../../lib/translations/readLocaleStrings'
import useTranslation, {
  getLocalizedPathsOfAllBlogArticlesByCategory,
  getPostContentByLocaledURL,
  getCategoryContentIndex
} from '../../../../lib/translations/useTranslation.hook'
import truncate from 'lodash.truncate'
import includes from 'lodash.includes'
import EmpresasSumadasGalery from "../../../../components/empresasSumadasGalery";
import Collapsable from '../../../../components/Collapsable'
import GrillaPostsPorCategoriaConModal from '../../../../components/grillaPostsPorCategoriaConModal'
import SeccionEncabezadoDePagina from "../../../../components/seccionEncabezadoDePagina";
import SeccionContenido from "../../../../components/seccionContenido";
import SeccionFooter from "../../../../components/seccionFooter";
import SeccionLitigiosEstrategicos from '../../../../components/seccionLitigiosEstrategicos'
import SeccionViolenciaDeGeneroEnParaguay from '../../../../components/seccionViolenciaDeGeneroEnParaguay'
import SeccionVisualizacionProcedimientosJudiciales from '../../../../components/seccionVisualizacionProcedimientosJudiciales'
import SeccionVisualizacionProcedimientosJudicialesLarge from '../../../../components/seccionVisualizacionProcedimientosJudicialesLarge'
import SeccionGaleria from '../../../../components/seccionGaleria'

export function Post(props) {
  const { t } = useTranslation()
  const { postDefaultPath, category, postContent,
    // post utilizados como páginas
    pagesContentIndex,
    // post especiales
    empresasSumadasContentIndex,
    tiposDeViolenciaContentIndex,
    tiposDeDesafiosContentIndex,
    tiposDePerpetradoresContentIndex,
    galeriaContent,
    // post de litigios estratégicos
    litigiosEstrategicosContentIndex,
    operadoresJudicialesContentIndex,
    procedimientosJudicialesContentIndex,
    // utilizado en el menú
    guiaContentIndex
  } = props;

  const localedPathMap = config.locales.map(locale => {
    return {
      locale,
      path: `/${locale}/b/${category}/${postContent[locale]}`
    }
  })

  const parsedOperadoresJudiciales = operadoresJudicialesContentIndex && operadoresJudicialesContentIndex.map(({ nombre, bio, cargo, imageFile, cargoClass, documentoEnlace, enlaceExterno }) => ({
    title: nombre,
    description: truncate(bio, { length: 160 }),
    content: bio,
    tag: cargo,
    imagePath: imageFile && `/blog-files/caso-belen-operadores-judiciales/${imageFile}`,
    tagClass: cargoClass,
    documentLink: documentoEnlace,
    externalLink: enlaceExterno
  }))

  return (
    <Layout title={postContent.title} localedPathMap={localedPathMap} postCategoryItems={{ guiaContentIndex, pagesContentIndex, litigiosEstrategicosContentIndex }}>
      {!postContent.customHeader ? (<>
        <SeccionEncabezadoDePagina imageUrl={`/blog-files/${category}/${postContent.mainImage}`} {...postContent} />
        {postContent.content && (
          <SeccionContenido {...postContent} />
        )}
      </>) : null}

      {/*****************************************
      ** Componentes de artículos especiales
      *******************************************/}

      {/* Tipos de violencia de género digital */}
      {postDefaultPath === 'tipos-de-violencia-de-genero-digital' ? (
        <div className="pb-28 -mt-24 grid w-4/5 m-auto grid-cols-2 gap-3 md:gap-4 lg:gap-5 lg:px-26 xl:px-32 2xl:px-36">
          {tiposDeViolenciaContentIndex.map((post, index) => (
            <div className="grid col-span-full lg:col-auto" key={index}>
              <Collapsable {...post}>{post.content}</Collapsable>
            </div>
          ))}
        </div>
      ) : ''}

      {/* Tipos de perpetradores */}
      {postDefaultPath === 'tipos-de-perpetradores' ? (
        <div className="pb-28 -mt-24 grid w-4/5 m-auto grid-cols-2 gap-3 md:gap-4 lg:gap-5 lg:px-26 xl:px-32 2xl:px-36">
          {tiposDePerpetradoresContentIndex.map((post, index) => (
            <div className="grid col-span-full lg:col-auto" key={index}>
              <Collapsable {...post}>{post.content}</Collapsable>
            </div>
          ))}
        </div>
      ) : ''}

      {/* Desafíos para responder a la VGFT */}
      {postDefaultPath === 'desafios-para-responder-a-la-vgft' ? (
        <div className="pb-28 -mt-24 grid w-4/5 m-auto grid-cols-2 gap-3 md:gap-4 lg:gap-5 lg:px-26 xl:px-32 2xl:px-36">
          {tiposDeDesafiosContentIndex.map((post, index) => (
            <div className="grid col-span-full lg:col-auto" key={index}>
              <Collapsable {...post}>{post.content}</Collapsable>
            </div>
          ))}
        </div>
      ) : ''}

      {/* Empresas sumadas */}
      {postDefaultPath === 'empresas-sumadas' ? (
        <EmpresasSumadasGalery page={postContent} posts={empresasSumadasContentIndex} />
      ) : ''}

      {/* Violencia de género en Internet en Paraguay */}
      {postDefaultPath === 'violencia-de-genero-en-internet-en-paraguay' ? (<>
        <SeccionViolenciaDeGeneroEnParaguay data={postContent}></SeccionViolenciaDeGeneroEnParaguay>
        <SeccionGaleria images={galeriaContent.images}></SeccionGaleria>
      </>) : ''}

      {/* Caso Belén */}
      {postDefaultPath === 'caso-belen' ? (<>
        <div className='text-center lg:text-left px-8 pt-14 pb-6 lg:pb-8 lg:px-36 xl:px-64'>
          <h3 className='text-3xl uppercase md:w-80 lg:text-4xl mb-4 lg:mb-6 text-brand-indigo-2'>{t('common.operadores-judiciales')}</h3>
          <p className='text-lg lg:text-xl'>{t('common.operadores-judiciales-subtitulo')}</p>
        </div>
        <GrillaPostsPorCategoriaConModal category="caso-belen-operadores-judiciales" gridData={parsedOperadoresJudiciales}></GrillaPostsPorCategoriaConModal>
        <SeccionVisualizacionProcedimientosJudiciales post={postContent} categoryContentIndex={litigiosEstrategicosContentIndex}></SeccionVisualizacionProcedimientosJudiciales>
        <SeccionGaleria images={galeriaContent.images}></SeccionGaleria>
      </>) : ''}

      {/* Visualización de Procedimientos Judiciales  */}
      {includes(postDefaultPath, '-procedimientos-judiciales') ? (<>
        <SeccionVisualizacionProcedimientosJudicialesLarge post={postContent} categoryContentIndex={procedimientosJudicialesContentIndex} operadoresContentIndex={operadoresJudicialesContentIndex}></SeccionVisualizacionProcedimientosJudicialesLarge>
        <SeccionGaleria images={galeriaContent.images}></SeccionGaleria>
      </>) : ''}

      <SeccionLitigiosEstrategicos posts={litigiosEstrategicosContentIndex}></SeccionLitigiosEstrategicos>

      <SeccionFooter id="footer" page={postDefaultPath}></SeccionFooter>
    </Layout>
  )
}

// Machear con el artículo correcto
// a partir del defaultUrl y locale
export async function getStaticPaths () {
  const paths = getLocalizedPathsOfAllBlogArticlesByCategory([
    'paginas',
    'guia',
    'litigios-estrategicos'
  ]);
  return {
    paths, fallback: false
  }
}

export async function getStaticProps ( props ) {
  let { lang, post, category } = props.params
  const postContent = getPostContentByLocaledURL(category, post, lang)
  const postDefaultPath = postContent[config.defaultLocale]
  const casosDeLitigios = [
    'caso-belen'
  ]

  // listados internos de páginas especiales (obtenidos de las carpetas de )
  let articulosEspecialesContenido = {}
  if (postDefaultPath === 'empresas-sumadas') {
    articulosEspecialesContenido.empresasSumadasContentIndex = getCategoryContentIndex('empresas-sumadas', lang, { fullDescription: true })
  }
  if (postDefaultPath === 'tipos-de-violencia-de-genero-digital') {
    articulosEspecialesContenido.tiposDeViolenciaContentIndex = getCategoryContentIndex('tipos-de-violencia', lang, { fullDescription: true })
  }
  if (postDefaultPath === 'desafios-para-responder-a-la-vgft') {
    articulosEspecialesContenido.tiposDeDesafiosContentIndex = getCategoryContentIndex('guia/tipos-de-desafios', lang, { fullDescription: true })
  }
  if (postDefaultPath === 'tipos-de-perpetradores') {
    articulosEspecialesContenido.tiposDePerpetradoresContentIndex = getCategoryContentIndex('guia/tipos-de-perpetradores', lang, { fullDescription: true })
  }

  // litigios estratégicos
  if (includes(casosDeLitigios, postDefaultPath)) {
    // Obtener lista de operadores judiciales y procedimientos judiciales
    articulosEspecialesContenido.operadoresJudicialesContentIndex = getCategoryContentIndex(`${postDefaultPath}-operadores-judiciales`, lang, { fullDescription: true })
  }
  if (includes(postDefaultPath, '-procedimientos-judiciales')) {
    // Obtener lista de eventos de procedimientos judiciales
    articulosEspecialesContenido.procedimientosJudicialesContentIndex = getCategoryContentIndex(postDefaultPath, lang, { fullDescription: true })
    // Obtener lista de operadores judiciales y procedimientos judiciales
    const caseDefaultPath = postDefaultPath.split('-procedimientos-judiciales')[0]
    articulosEspecialesContenido.operadoresJudicialesContentIndex = getCategoryContentIndex(`${caseDefaultPath}-operadores-judiciales`, lang, { fullDescription: true })
  }

  articulosEspecialesContenido.litigiosEstrategicosContentIndex = getCategoryContentIndex('litigios-estrategicos', lang, {})
  articulosEspecialesContenido.guiaContentIndex = getCategoryContentIndex('guia', lang, { truncateContentLength: 0 })
  articulosEspecialesContenido.pagesContentIndex = getCategoryContentIndex('paginas', lang, { truncateContentLength: 0 })
  articulosEspecialesContenido.galeriaContent = getPostContentByLocaledURL('paginas', 'galeria', lang)

  return {
    props: {
      locale: lang,
      localeStrings: readLocaleStrings(lang, ['common']),
      postDefaultPath,
      post: post,
      category: category,
      postContent,
      ...articulosEspecialesContenido
    }
  }
}

export default withLocale(Post)