import React from 'react'
import Layout from '../../components/layout'
import Header from '../../components/header'
import withLocale from '../../lib/translations/withLocale.hoc'
import config from '../../config'
import { readLocaleStrings } from '../../lib/translations/readLocaleStrings'
import useTranslation, { getCategoryContentIndex, getPostContentByLocaledURL } from '../../lib/translations/useTranslation.hook'
import GrillaPostsPorCategoria from '../../components/grillaPostsPorCategoria'
import _ from 'lodash'
import SeccionLitigiosEstrategicos from '../../components/seccionLitigiosEstrategicos'
import SeccionGaleria from '../../components/seccionGaleria'
import SeccionFooter from '../../components/seccionFooter'
import SeccionEmpresasSumadas from '../../components/seccionEmpresasSumadas'

export function BlogPost(props) {
  const { t, locale } = useTranslation()
  const { guiaContentIndex, pagesContentIndex, litigiosEstrategicosContentIndex, galeriaContent, empresasSumadasContent, introContent } = props
  
  const gridData = guiaContentIndex.map(post => {
    return {
      ...post,
      description: post.content,
      destUrl: `/${locale}/b/guia/${post[locale]}`
    }
  });

  return (
    <Layout home postCategoryItems={{ guiaContentIndex, pagesContentIndex, litigiosEstrategicosContentIndex }}>
      <Header id="header" data={introContent}></Header>
      
      <GrillaPostsPorCategoria category="guia" gridData={gridData}></GrillaPostsPorCategoria>

      <SeccionEmpresasSumadas empresasSumadasContent={empresasSumadasContent} pagesContentIndex={pagesContentIndex}></SeccionEmpresasSumadas>

      <SeccionGaleria images={galeriaContent.images}></SeccionGaleria>
      
      <SeccionLitigiosEstrategicos posts={litigiosEstrategicosContentIndex}></SeccionLitigiosEstrategicos>

      <SeccionFooter id="footer" page={'home'}></SeccionFooter>
    </Layout>
  )
}

export async function getStaticPaths () {
  const paths = []
  config.locales.forEach(locale => {
    paths.push({ params: { lang: locale } })
  })
  return {
    paths, fallback: false
  }
}

export async function getStaticProps ( props ) {
  let { lang } = props.params
  return {
    props: {
      locale: lang,
      localeStrings: readLocaleStrings(lang, ['common', 'inicio']),
      guiaContentIndex: getCategoryContentIndex('guia', lang, { truncateContentLength: 80 }),
      pagesContentIndex: getCategoryContentIndex('paginas', lang, { truncateContentLength: 0 }),
      litigiosEstrategicosContentIndex: getCategoryContentIndex('litigios-estrategicos', lang, {}),
      galeriaContent: getPostContentByLocaledURL('paginas', 'galeria', lang),
      introContent: getPostContentByLocaledURL('paginas', 'violencia-de-genero', lang),
      empresasSumadasContent: getCategoryContentIndex('empresas-sumadas', lang, {} )
    }
  }
}

export default withLocale(BlogPost)