import '../styles/globals.css'
import withLayout from '../components/withLayout.hoc'
import { MatomoProvider, createInstance } from '@datapunt/matomo-tracker-react'

export function App({ Component, pageProps }) {
  const instance = createInstance({
    urlBase: 'https://matomo.tedic.org/',
    siteId: 97
  })

  return <>
    <MatomoProvider value={instance}>
      <Component {...pageProps} />
    </MatomoProvider>
  </>
}

export default withLayout(App)