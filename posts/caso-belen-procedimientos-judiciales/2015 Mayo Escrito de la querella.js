export default {
    // en
    'title_en': "Plaintiff’s brief.",
    'date_en': 'May 2015',
    'content_en': `
  New request for a hearing with the Attorney General
    `,
  
    // es
    'title_es': "Escrito de la querella.",
    'date_es': 'Mayo 2015',
    'content_es': `
  Reitera el pedido de audiencia con el Fiscal General
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_05_28_Reitera_Pedido_de_Audiencia.pdf'
  };