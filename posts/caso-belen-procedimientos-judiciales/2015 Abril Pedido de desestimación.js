export default {
    // en
    'title_en': "Motion to dismiss.",
    'date_en': 'April 2015',
    'content_en': `
  The prosecutor presents a request for dismissal of the case on the grounds of not considering the denounced behaviors as punishable.
    `,
  
    // es
    'title_es': "Pedido de desestimación.",
    'date_es': 'Abril 2015',
    'content_es': `
  El agente fiscal presenta un pedido de desestimación de la causa por no considerar a las conductas denunciadas como punibles.
    `,
  };