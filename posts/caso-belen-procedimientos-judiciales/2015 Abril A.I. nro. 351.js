export default {
    // en
    'title_en': "A.I. no. 351",
    'date_en': 'April 2015',
    'content_en': `
  Request approved and dossier sent to the General Prosecutor's Office in order for it to ratify or rectify the request for dismissal presented by the prosecutor.
    `,
  
    // es
    'title_es': "A.I. nro. 351.",
    'date_es': 'Abril 2015',
    'content_es': `
  Hace lugar al pedido y remite el expediente a la Fiscalía General a efectos de que ésta ratifique o rectifique el requerimiento de desestimación presentado por el Agente Fiscal.
    `,
  };