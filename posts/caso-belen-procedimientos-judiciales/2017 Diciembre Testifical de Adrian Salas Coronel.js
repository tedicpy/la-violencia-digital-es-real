export default {
  // en
  'title_en': "Testimony of Adrian Salas Coronel.",
  'date_en': 'December 2017',
  'content_en': `
Was a member at that time of the Council of the Magistracy and of the Jury for the Prosecution of Magistrates. Testified by written testimony.
  `,

  // es
  'title_es': "Testifical de Adrian Salas Coronel.",
  'date_es': 'Diciembre 2017',
  'content_es': `
Esta persona era en ese momento miembro del consejo de la Magistratura y del Jurado de Enjuiciamiento de Magistrados. Testificó vía oficio.
  `,
};