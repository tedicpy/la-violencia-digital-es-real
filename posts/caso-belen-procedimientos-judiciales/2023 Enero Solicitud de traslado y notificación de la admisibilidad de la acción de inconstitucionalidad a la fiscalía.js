export default {
  // en
  'title_en': "Request for transfer and notification of the admissibility of the action of unconstitutionality to the Prosecutor General's Office",
  'date_en': '2023',
  'content_en': `
  On January 6, the defence of Belén requested the authorisation of the judicial fair and that the transfer to the prosecutor's office of the admission of the unconstitutionality action of the Court be sent..
  `,

  // es
  'title_es': "Solicitud de traslado y notificación de la admisibilidad de la acción de inconstitucionales a la fiscalía",
  'date_es': '2023',
  'content_es': `
  El 6 de enero la defensa de Belén solicitó el pedido de habilitación de feria judicial y que se corra el traslado a la fiscalía de la admisión de la acción de Inconstiucionales de la Corte.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2023_Defensa_solicita_traslado_y_habilitación.pdf'
};
