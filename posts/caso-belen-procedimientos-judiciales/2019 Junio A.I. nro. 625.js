export default {
  // en
  'title_en': "A.I. no. 625.",
  'date_en': 'June 2019',
  'content_en': `
Resolution by means of which she states the declaration of default against Belén, Victor Oviedo and Leonard Chunf, and issues arrest orders. She also orders Belén’s defense attorneys to step down, stating that they must be dissociated from the Judisoft computer system which gives access to the dossier. Finally, she orders the interruption of the process. This ruling was never notified and this, added to the cancellation of the defense, prevented potential legal remedies from being filed. The declaration of default did not follow what is established in the legislation, since, of the specific causes allowing the use of this figure, the only one applicable to people who are not under arrest or detention measures is non-appearance. This does not apply to Belén, since she appeared in court every time she was summoned.
  `,

  // es
  'title_es': "A.I. nro. 625.",
  'date_es': 'Junio 2019',
  'content_es': `
Resolución por medio de la cual declara la rebeldía de Belén, de Victor Oviedo y Leonard Chunf, ordenando la respectiva captura de las tres personas en cuestión. Así mismo ordena cancelar la intervención de los abogados defensores de Belén estableciendo que se debe disponer la desvinculación de los mismos del sistema informático Judisoft. Por último ordena disponer la interrupción del plazo máximo de duración del proceso. Esta providencia nunca fue notificada y ello, sumado a la cancelación de la defensa, impidió que se interpusieran los recursos de ley. La declaración de rebeldía no correspondió a lo establecido en la legislación dado que de las causales taxativas se preve para esta figura la única aplicable a personas que no se encuentran con medidas de detención o aprehensión, es la incomparencencia; acción que no fue realizada por Belén en tanto compareció a todas las citaciones.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Compra_Notas/2019_06_21_Auto_Rebeld%C3%ADa.pdf'
};