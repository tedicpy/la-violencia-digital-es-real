export default {
  // en
  'title_en': "Claim of unconstitutionality.",
  'date_en': 'August 2019',
  'content_en': `
Brief submitted by the defense through which they promote a claim of unconstitutionality and request the imposition of the precautionary measure of suspension of the effects of the appealed resolution.
  `,

  // es
  'title_es': "Acción de inconstucionalidad.",
  'date_es': 'Agosto 2019',
  'content_es': `
Escrito presentado por la Defensa por medio del cual promueve acción de inconstitucionalidad y solicita que se imponga la medida cautelar de suspensión de los efectos de la resolución recurrida.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Compra_Notas/2019_08_12_Accio%CC%81n_de_Inconstitucionalidad.pdf'
};