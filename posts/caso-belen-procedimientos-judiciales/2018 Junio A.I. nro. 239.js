export default {
  // en
  'title_en': "A.I. no. 239.",
  'date_en': 'June 2018',
  'content_en': `
Resolution by which the First Instance Judge Vivian López decided to exclude the points of expertise Nos. 1 to 60 (cellphone expertise) from those proposed by the defendant, admitting only the expertise report points regarding the publications or comments posted on the social media network Facebook. The judge argues that the evidence provided is not related to the disputed facts, declaring them superabundant since they have been previously treated in a criminal court.
  `,

  // es
  'title_es': "A.I. nro. 239.",
  'date_es': 'Junio 2018',
  'content_es': `
Resolución mediante la cual la Jueza de Primera Instancia Vivian López resuelve excluir los puntos de la pericia 1 al número 60 de los propuestos por la demandada (pericia del celular), admite solamente la pericia en relación a publicaciones o comentarios en la red social facebook. La Jueza argumenta que la prueba ofrecida no está relacionada con los hechos controvertidos; declarándolas sobreabundantes al haber sido tratadas previamente en sede penal.
  `,

  'documentoEnlace': ''
};