export default {
    // en
    'title_en': "Resolution no. 6/15.",
    'date_en': 'March 2015 ',
    'content_en': `
Approval of the expertise dictated by prosecutor Centurion, where the analysis of the cellphones is confirmed, stating in point 3 that the extraction of WhatsApp messages covers the years 2013 and 2014
    `,
  
    // es
    'title_es': "Resolución nro. 6/15.",
    'date_es': 'Marzo 2015',
    'content_es': `
Se dispone la realización de la pericia dictada por el Fiscal Centurión donde se confirma la pericia del celular estableciendo en el punto 3 que la extracción de los mensajes vía whatsapp comprende los años 2013 y 2014
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_03_31_Resoluci%C3%B3n_Confirma_Peritaje.pdf'
  };