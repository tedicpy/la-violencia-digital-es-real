export default {
  // en
  'title_en': "Clarification appeal.",
  'date_en': 'Diciembre 2018',
  'content_en': `
Belén’s lawyer presents a clarification appeal against A.I. no. 531, demanding that the Court clarify whether points 6 to 59 of the expert report had also been acknowledged by Kriskovich (these being the points of the expertise that highlighted the harassment messages).
  `,

  // es
  'title_es': "Recurso de aclaratoria.",
  'date_es': 'Diciembre 2018',
  'content_es': `
La defensa de belen presenta recurso de aclaratoria contra el A.I. nro. 531 exigiendo que el Tribunal aclare si los puntos 6 al 59 de la pericia también habían sido admitidos por Kriskovich. (estos son los puntos que destacaban los mensajes de acoso y hostigamiento)
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Indemnizacion/2018_12_26_Recurso_de_Aclaratoria.pdf'
}