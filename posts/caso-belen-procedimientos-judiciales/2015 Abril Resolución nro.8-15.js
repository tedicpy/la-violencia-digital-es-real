export default {
    // en
    'title_en': "Resolution no.8/15",
    'date_en': 'April 2015',
    'content_en': `
  Approval of the execution of digital expertise. Prosecutor Centurión agreed with the proposed points of expertise, dates were set for the appointment of the expert witness (04/24/15) and the delivery of the mobile device (04/27/15)
    `,
  
    // es
    'title_es': "Resolución nro. 8/15.",
    'date_es': 'Abril 2015',
    'content_es': `
  Se dispone la realización de la pericia informática. El fiscal Centurión admitió los puntos de pericia propuestos, se fijaron fechas para la aceotación de cargo de perito (24/04/15) y entrega del aparato celular (27/04/15)
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_04_17_Resoluci%C3%B3n_Aprueba_Puntos_de_Peritaje.pdf'
  };