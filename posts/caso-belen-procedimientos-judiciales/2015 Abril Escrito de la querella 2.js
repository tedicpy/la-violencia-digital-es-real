export default {
    // en
    'title_en': "Plaintiff’s brief.",
    'date_en': 'April 2015',
    'content_en': `
  New request for the delivery of the mobile device of Mr. Kriskovich for conducting the expertise
    `,
  
    // es
    'title_es': "Escrito de la querella.",
    'date_es': 'Abril 2015',
    'content_es': `
  Se reitera la entrega del aparato celular del Sr. Kriskovich para la realización de la pericia
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_04_21-I_Escrito_Reitera_Pedido_de_Peritaje.pdf'
  };