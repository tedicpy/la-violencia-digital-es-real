export default {
  // en
  'title_en': "A.I. no. 313.",
  'date_en': 'June 2018',
  'content_en': `
Resolves to partially allow the clarification appeal and consequently establish that the admitted expert evidence be presented jointly and in a single dossier by the three designated experts.
  `,

  // es
  'title_es': "A.I. nro. 313.",
  'date_es': 'Junio 2018',
  'content_es': `
Resuelve hacer lugar parcialmente al recurso de aclaratoria y en consecuencia establecer que la prueba pericial admitida sea practicada de manera conjunta y en un solo acto entre los tres peritos designados.
  `,
};