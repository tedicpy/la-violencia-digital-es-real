export default {
  // en
  'title_en': "Evidence offering brief.",
  'date_en': 'October 2017',
  'content_en': `
Belen's defense presents a brief in order to offer evidence. Among these: cellphone expert reports and expertise points (the same points provided in the sexual harassment case).
  `,

  // es
  'title_es': "Escrito ofrecimiento de pruebas.",
  'date_es': 'Octubre 2017',
  'content_es': `
La defensa de Belen presenta un escrito con el objeto de ofrecer pruebas. Entre ellas: la pericia del teléfono celular y puntos de pericia (los mismos puntos ofrecidos en la causa de acoso sexual).
  `,
};