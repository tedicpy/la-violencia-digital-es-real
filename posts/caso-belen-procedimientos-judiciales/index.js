export default [
  {
    'key': "2014 Septiembre Kriskovich realiza una declaración jurada",
    'year': '2014',
    'operadores': ['cristian-kriskovich']
  }, {
    'key': "2014 Octubre Resolución nro 99",
    'year': '2014',
    'operadores': ['belen-whittingslow']
  }, {
    'key': "2014 Noviembre Se presenta denuncia de acoso sexual ante el Ministerio Público",
    'year': '2014',
    'operadores': ['rodrigo-cuevas', 'martin-barba', 'belen-whittingslow'] // UCA
  }, {
    'key': "2015 Escrito presentado por la querella",
    'year': '2015',
    'operadores': []
  }, {
    'key': "2015 Marzo Escrito presentado por Belén solicitando se fije fecha para el peritaje de teléfonos celulares y se designe perito",
    'year': '2015',
    'operadores': ['rodrigo-cuevas', 'martin-barba']
  }, {
    'key': "2015 Marzo Escrito de denuncia contra la Fiscal Liz Nadine Portillo dirigido a la Inspectoría del Ministerio Público",
    'year': '2015',
    'operadores': ['rodrigo-cuevas', 'martin-barba', 'nadine-portillo']
  }, {
    'key': "2015 Marzo Resolución nro. 6-15",
    'year': '2015',
    'operadores': ['rodolfo-fabian-centurion']
  }, {
    'key': "2015 Abril Escrito de la querella",
    'year': '2015',
    'operadores': ['nadine-portillo']
  }, {
    'key': "2015 Abril Resolución nro.8-15",
    'year': '2015',
    'operadores': []
  }, {
    'key': "2015 Abril Escrito de la querella 2",
    'year': '2015',
    'operadores': ['nadine-portillo']
  }, {
    'key': "2015 Abril Escrito presentado por la defensa de Kriskovich",
    'year': '2015',
    'operadores': []
  }, , {
    'key': "2015 Abril Escrito de la querella 3",
    'year': '2015',
    'operadores': ['nadine-portillo']
  }, {
    'key': "2015 Abril Resolución nro. 9-15",
    'year': '2015',
    'operadores': []
  }, {
    'key': "2015 Abril Pedido de desestimación",
    'year': '2015',
    'operadores': []
  }, {
    'key': "2015 Abril A.I. nro. 351",
    'year': '2015',
    'operadores': []
  }, {
    'key': "2015 Mayo Escrito de la querella",
    'year': '2015',
    'operadores': ['nadine-portillo']
  }, {
    'key': "2015 Junio Escrito del Fiscal Adjunto Jorge Sosa",
    'year': '2015',
    'operadores': ['jorge-sosa']
  }, {
    'key': "2015 Junio A.I. nro. 423",
    'year': '2015',
    'operadores': []
  }, {
    'key': "2015 junio Acta de imputación a Belen presentada por la Agente Fiscal Claudia Morys",
    'year': '2015',
    'operadores': ['claudia-morys']
  }, {
    'key': "2015 Diciembre Presentación de la acusación",
    'year': '2015',
    'operadores': ['belen-whittingslow', 'claudia-morys']
  }, {
    'key': "2016 Octubre Escrito de demanda",
    'year': '2016',
    'operadores': ['cristian-kriskovich']
  }, {
    'key': "2017 Julio Escrito de la defensa",
    'year': '2017',
    'operadores': ['belen-whittingslow']
  }, {
    'key': "2017 Julio Audiencia preliminar",
    'year': '2017',
    'operadores': ['teresa-sosa-laconich']
  }, {
    'key': "2017 Octubre Escrito ofrecimiento de pruebas",
    'year': '2017',
    'operadores': ['rodrigo-cuevas', 'martin-barba']
  }, {
    'key': "2017 Noviembre Escrito de oposición",
    'year': '2017',
    'operadores': ['cristian-kriskovich']
  }, {
    'key': "2017 Diciembre Testifical de Adrian Salas Coronel",
    'year': '2017',
    'operadores': ['adrian-salas']
  }, {
    'key': "2017 Diciembre Testifical de Enrique Riera",
    'year': '2017',
    'operadores': ['enrique-riera']
  }, {
    'key': "2018 Junio A.I. nro. 239",
    'year': '2018',
    'operadores': ['vivian-lopez']
  }, {
    'key': "2018 Junio Recurso de aclaratoria",
    'year': '2018',
    'operadores': ['cristian-kriskovich']
  }, {
    'key': "2018 Junio A.I. nro. 313",
    'year': '2018',
    'operadores': []
  }, {
    'key': "2018 Diciembre A.I. nro. 531",
    'year': '2018',
    'operadores': []
  }, {
    'key': "2018 Diciembre Escrito de denuncia",
    'year': '2018',
    'operadores': ['rodrigo-cuevas', 'martin-barba']
  }, {
    'key': "2018 Diciembre Recurso de aclaratoria",
    'year': '2018',
    'operadores': ['rodrigo-cuevas', 'martin-barba']
  }, {
    'key': "2019 Recurso de nulidad y apelación",
    'year': '2019',
    'operadores': ['rodrigo-cuevas', 'martin-barba']
  }, {
    'key': "2019 Mayo Resolución Cámara de Senadores",
    'year': '2019',
    'operadores': ['cristian-kriskovich']
  }, {
    'key': "2019 Junio A.I. nro. 625",
    'year': '2019',
    'operadores': ['lici-sanchez']
  }, {
    'key': "2019 Julio Escrito de la defensa",
    'year': '2019',
    'operadores': ['lici-sanchez']
  }, {
    'key': "2019 Agosto Acción de inconstucionalidad",
    'year': '2019',
    'operadores': ['lici-sanchez', 'rodrigo-cuevas']
  }, {
    'key': "2021 Noviembre Presentación ante Corte Interamericana",
    'year': '2021',
    'operadores': []
  }, {
    'key': "2021 Noviembre Urgimiento de pronto despacho a la Corte Suprema - número 9",
    'year': '2021',
    'operadores': []
  }, {
    'key': "2022 Abril Urgimiento de pronto despacho a la Corte Suprema - número 10",
    'year': '2022',
    'operadores': []
  }, {
    'key': "2022 Diciembre Acción de encadenamiento de la Madre de Belén frente al Poder Judicial",
    'year': '2022',
    'operadores': []
  }, {
    'key': "2022 Diciembre Admisibilidad de la acción de inconstitucionalidad por parte de la Corte Suprema",
    'year': '2022',
    'operadores': []
  }, {
    'key': "2023 Enero Solicitud de traslado y notificación de la admisibilidad de la acción de inconstitucionalidad a la fiscalía",
    'year': '2023',
    'operadores': []
  }, {
    'key': "2023 La Fiscalía General solicita interrupción del plazo para contestar la acción de inconstitucionalidad",
    'year': '2023',
    'operadores': []
  }, {
    'key': "2023 Declaración de incostitucionalidad",
    'year': '2023',
    'operadores': []
  }, {
    'key': "2023 Visita de enviados del Vaticano",
    'year': '2023',
    'operadores': []
  }
]
