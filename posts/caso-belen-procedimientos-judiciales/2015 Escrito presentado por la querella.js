export default {
    // en
    'title_en': "brief presented by the plaintiff",
    'date_en': '2015',
    'content_en': `
A procedure to oppose the dismissal is addressed to judge Julián López at the appellate court no. 12.
        `,
  
    // es
    'title_es': "Escrito presentado por la querella.",
    'date_es': '2015',
    'content_es': `
se solicita trámite de oposición a la desestimación ante el Juzgado de garantías nro. 12 Julián López.
        `,
  };