export default {
    // en
    'title_en': "Brief submitted by Belén requesting that a date be set for the expertise of cellphone records and an expert witness be appointed.",
    'date_en': 'March 2015 ',
    'content_en': `
An expertise is requested on the cellphones of Belén and Mr. Kriskovich and points of expertise are presented where certain messages sent from Mr. Kriskovich's cellphone are highlighted, as well as messages sent by Belén reflecting her opposition and nuisance.
    `,
  
    // es
    'title_es': "Escrito presentado por Belén solicitando se fije fecha para el peritaje de teléfonos celulares y se designe perito.",
    'date_es': 'Marzo 2015',
    'content_es': `
Se solicita se realice una pericia del teléfono celular de Belén y del Sr. Kriskovich y se ofecen puntos de pericia donde se destacan ciertos mensajes enviados desde el celular del Sr. Kriskovich asi como mensajes de oposición y molestia enviados por Belén.
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_03_03_Escrito_Solicitud_de_Peritaje.pdf'
  };