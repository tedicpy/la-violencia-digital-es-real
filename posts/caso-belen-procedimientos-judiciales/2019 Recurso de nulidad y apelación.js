export default {
  // en
  'title_en': "Nullity writ and appeal.",
  'date_en': '2019',
  'content_en': `
Presented by Belén’s defense.
  `,

  // es
  'title_es': "Recurso de nulidad y apelación.",
  'date_es': '2019',
  'content_es': `
Presentado por la defensa de Belén.
  `,
};