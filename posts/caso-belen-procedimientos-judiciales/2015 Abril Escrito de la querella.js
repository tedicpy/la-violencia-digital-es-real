export default {
    // en
    'title_en': "Plaintiff’s brief",
    'date_en': 'April 2015',
    'content_en': `
Brief requesting the data extraction from K's cellphone, proposing points of expertise and appointment of expert witnesses
        `,
  
    // es
    'title_es': "Escrito de la querella.",
    'date_es': 'Abril 2015',
    'content_es': `
Escrito solicitando el pedido de extracción de datos del celular de K, proponer puntos de pericia y designación de peritos
    `,
  };