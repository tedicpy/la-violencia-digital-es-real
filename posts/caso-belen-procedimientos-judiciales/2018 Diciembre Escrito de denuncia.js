export default {
  // en
  'title_en': "Complaint brief.",
  'date_en': 'Diciembre 2018',
  'content_en': `
Belen's defense presents a brief in order to file a complaint for serious irregularities against Kriskovich, Alberto Martinez Simón and Linneo Ynsfran, alleging influence peddling.
  `,

  // es
  'title_es': "Escrito de denuncia.",
  'date_es': 'Diciembre 2018',
  'content_es': `
La defensa de Belen presenta un escrito con el objeto de formular denuncia por graves irregularidades contra Kriskovich, Alberto Martinez Simón y Linneo Ynsfran alegando tráfico de influencias.
  `,
};