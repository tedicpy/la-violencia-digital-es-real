export default {
  // en
  'title_en': "Beseechment for prompt delivery to the Supreme Court -  number 10",
  'date_en': '2022',
  'content_en': `
  Belén’s legal defense introduces for the tenth time the urgency to address the brief filed by the Defense in 2019 whereby it claims an act of unconstitutionality and requests the enforcement of the precautionary measure of suspension of the legal effects of the appealed resolution.
  `,

  // es
  'title_es': "Urgimiento de pronto despacho a la Corte Suprema - número 10",
  'date_es': '2022',
  'content_es': `
  La defensa de Belén presenta por décima vez el urgimiento para atender el escrito presentado por la Defensa en el año 2019 por medio del cual promueve acción de inconstitucionalidad y solicita que se imponga la medida cautelar de suspensión de los efectos de la resolución recurrida.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2022_Caso_Belen_10mo_urgimiento.jpg'
};
