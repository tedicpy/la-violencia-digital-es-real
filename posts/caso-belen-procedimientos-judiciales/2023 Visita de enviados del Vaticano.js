export default {
  // en
  'title_en': "Visit from Vatican envoy",
  'date_en': '2023',
  'content_en': `
  Envoys from the Vatican came to Paraguay to speak with the lawyers of Belén and her family. In this meeting, documentation was handed over regarding the persecution they are suffering from the Catholic University and Paraguayan Justice.
  `,

  // es
  'title_es': "Visita de enviados del Vaticano",
  'date_es': '2023',
  'content_es': `
  Enviados del Vaticano vinieron a Paraguay para conversar con los abogados de Belén y su familia, en esta reunión fueron entregadas documentaciones de la persecusión que sufren por parte de la Universidad Católica y la Justicia paraguaya.
  `,

  'enlaceExterno': 'https://www.abc.com.py/nacionales/2023/05/19/abogado-y-madre-de-belen-whittingslow-se-reunieron-con-emisarios-del-vaticano/'
};
