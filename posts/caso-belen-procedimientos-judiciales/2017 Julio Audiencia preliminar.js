export default {
    // en
    'title_en': "Preliminary hearing.",
    'date_en': 'July 2017',
    'content_en': `
Prosecutor Teresa Sosa ratifies accusation no. 23, dated December 11, 2015, and lodges an incident of Probationary Inclusion of testimonial and expert witnesses statements
(report of the company Mobile Cash, report of the company Vox, data records of two phone lines belonging to a student and to an administrative clerk). Furthermore, she 
requests that alternative measures to imprisonment be implemented (fixed address, prohibition of leaving the country or other personal or real caution that the court considers
pertinent). Belén’s defense attorney, Rodrigo Cuevas, states that there is no documentation in the file that supports the prosecutor’s intervention. He then requests that the 
incidents formulated in that same act be resolved in separate files to those corresponding to the resolution of establishing an oral and public trial. He indicates that the 
incidents present irregularities regarding the accusation, since the ruling for the accusation to be presented is in place but the defense has not been able to access the
dossier because it is not listed in the writ. Finally, he requests the unification of the preliminary hearings with those of the other defendants. The prosecutor states 
that she has been appointed by the State Attorney General and requests that the documentation be added to the dossier. She does not object to the request for 
unification of the preliminary hearings. The judge orders to add to the dossier resolution No. 562 dated 03/27/17, which endorses the status of prosecutor Teresa Sosa. 
He decides to deny the nullity incident of the accusation given that, although he admits that an error occurred in naming prosecutor Teresa Sosa as the one who requested 
the accusation, when it was Prosecutor Claudia Morys, said circumstance does not violate the right to defense of the defendant, who was duly notified. Regarding the Unification 
Incident, he decided to deny the request on the basis that the complaints against the defendants were made on different dates. The defendant files a Reconsideration and Appeal 
writ on the basis that the Public Ministry has not opposed the request to unify the preliminary hearings. Moreover, they request that authenticated copies of the resolutions 
rejecting the challenge presented against prosecutor Teresa Sosa be attached. The prosecutor expresses that a simple copy of the electronic notification of A.I. No. 562 has 
already been attached, whereby the dismissal request was not made. She further requests that the judge review his position regarding the unification of hearings. The judge 
decides not to allow the motion of reconsideration, stating that his decision is based on the procedures in the Criminal Procedure Code. The defense attorney requests that 
the judge refrain from continuing to participate in the case, alleging that he has a personal interest in it.
    `,
  
    // es
    'title_es': "Audiencia preliminar.",
    'date_es': 'Julio 2017',
    'content_es': `
La Agente Fiscal Teresa Sosa ratifica la acusación nro. 23 de fecha 11 de diciembre del 2015. Así mismo, interpone un incidente de Inclusión Probatoria de declaraciones testimoniales, 
periciales (informe de la empresa movile cash, informe de la empresa vox, la desgrabación de los datos de dos líneas telefónicas pertenecientes a una alumna y a una persona de administración)
Por último, solicita que se apliquen medidas alternativas a la prisión (fijación de domicilio, prohibición de salida del país o alguna caución personal o real que el juzgado considere pertinente).
La Defensa de Belén (Abog. Rodrigo Cuevas) manifiesta en primer término que no existe documentación alguna en el expediente que avale la intervención de la Agente Fiscal. Seguidamente solicita sean
resueltos los incidentes planteados1 en ese mismo acto, en autos separados a los que corresponden a la resolución de elevación a juicio oral y público. Señala que los incidentes hacen referencia a 
irregularidades en referencia a la acusación en tanto se cuenta con la providencia que tiene por presentada la acusación pero la defensa no ha podido acceder a dicha acusación en tanto no se encuentra
agregada a autos. Por último solicita la unificación de audiencia preliminares con los demás imputados. La Agente Fiscal manifiesta que ha sido designada por la FGE y solicita sea agregada la documentación
a autos. Así mismo no opone reparos contra la solicitud de unificación de audiencias. El Juez ordena agregar a autos la resolución N.º 562 de fecha 27/03/17 que avala la calidad de parte de la 
Fiscal Teresa Sosa. Resuelve negar el incidente de nulidad de la acusación dado que si bien admite que se consignó un error en dicha acta de consignar a la Agente Fiscal Teresa Sosa como quien
solicitare la acusación, habiendo sido la Agente Fiscal Claudia Morys, dicha circunstancia no vulnera el derecho de defensa de la acusada quien fue debidamente notificada. Con respecto al Incidente
de Unificación, resuelve negar el pedido en base a que las acusaciones de los imputados fueron realizados en fechas diferentes. La Defensa interpone Recurso de Reposición y Apelación en subsidio en
base a que el Ministerio Público no se ha opuesto al requerimiento de unificar las audiencias preliminares. Así mismo solicita se agregen copias autenticadas de las resoluciones que rechazan la 
recusación presentada por su parte contra la Fiscal Teresa Sosa. La Agente Fiscal expresa que ya se ha acompañado fotocopia simple de la notificación electrónica del AI N.º 562 por la cual no se
hizo lugar al pedido de recusación. Finaliza solicitando que el Juez revea su posición respecto a la unificación de audiencias. El juez resuelve no hacer lugar al recurso de reposición señalando
que su decisión esta basada en los procedimientos obrantes en el CPP. La Defensa solicita que el Juez se inhiba de seguir resolviendo en la causa alegando que existe un interés personal en la misma.
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Compra_Notas/2017_07_11-I_Audiencia_Preliminar_Otros.pdf'
  };