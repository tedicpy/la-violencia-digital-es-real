export default {
    // en
    'title_en': "Complaint brief against prosecutor Liz Nadine Portillo addressed to the Inspectorate of the Public Ministry.",
    'date_en': 'March 2015',
    'content_en': `
The plaintiff alleges influence peddling and revictimization of Belén. As a consequence, the investigation was transferred to prosecutor Rodolfo Centurión, District Attorney’s Office Unit No. 2, Asunción
    `,
  
    // es
    'title_es': "Escrito de denuncia contra la Fiscal Liz Nadine Portillo dirigido a la Inspectoría del Ministerio Público.",
    'date_es': 'Marzo 2015',
    'content_es': `
La denuncia alega tráfico de influencias y revictimización hacia Belén. Como consecuencia,la investigación pasó a ser llevada por el Agente Fiscal Rodolfo Centurión de la Unidad Fiscal N°2 de Asunción.
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_03_18_Denuncia_Fiscal_Liz_Portillo_MP.pdf'
  };