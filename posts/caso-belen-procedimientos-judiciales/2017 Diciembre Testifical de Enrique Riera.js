export default {
  // en
  'title_en': "Testimony of Enrique Riera.",
  'date_en': 'December 2017',
  'content_en': `
Was at that time Minister of Education and a former member of the Council of the Magistracy. Testified by written testimony.
  `,

  // es
  'title_es': "Testifical de Enrique Riera.",
  'date_es': 'Diciembre 2017',
  'content_es': `
Era en ese momento Ministro de Educación y ex miembro del Consejo de la Magistratura. Testificó vía oficio.
  `,
};