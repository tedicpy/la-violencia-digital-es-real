export default {
  // en
  'title_en': "The Prosecutor General's Office requests an interruption of the deadline to reply to the unconstitutionality action",
  'date_en': '2023',
  'content_en': `
  On January 11, Assistant Prosecutor Federico Espinoza, representing the State Attorney General's Office, requested an interruption of the term to answer the unconstitutionality action when the Supreme Court had not yet notified the unconstitutionality action for the prosecution to answer.
  `,

  // es
  'title_es': "La Fiscalía General solicita interrupción del plazo para contestar la acción de inconstitucionalidad",
  'date_es': '2023',
  'content_es': `
  El 11 de enero, el fiscal adjunto Federico Espinoza, en representación de la Fiscalía General del Estado, solicitó interrupción del plazo para contestar la acción de inconstitucionalidad, cuando aún la Corte Suprema no notificó la acción de inconstitucionalidad para que la fiscalía conteste.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2023_Contestación_Fiscalia_traslado_pedido_habilitación_feria.pdf'
};
