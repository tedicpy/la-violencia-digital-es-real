export default {
  // en
  'title_en': "Resolution no. 99.",
  'date_en': 'October 2014',
  'content_en': `
Resolution no. 99
      `,

  // es
  'title_es': "Resolución nro. 99.",
  'date_es': 'Octubre 2014',
  'content_es': `
Resolución nro. 99
      `,
};
