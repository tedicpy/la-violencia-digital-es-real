export default {
    // en
    'title_en': "Defense brief.",
    'date_en': 'July 2017',
    'content_en': `
Requests the definitive dismissal, offers evidence and lodges the nullity incident of the accusation and exception for lack of action.
    `,
  
    // es
    'title_es': "Escrito de la defensa.",
    'date_es': 'Julio 2017',
    'content_es': `
Solicita el sobreseiminento definitivo, ofrece pruebas e interponer incidente de nulidad de la acusación y excepción por falta de acción
    `,

    'documentoEnlace': ''
  };