export default {
  // en
  'title_en': "Unconstitutional declaration",
  'date_en': '2023',
  'content_en': `
  The Supreme Court of Justice declares unconstitutional the resolution that ordered the arrest warrant against Belén Whittingslow.
  `,

  // es
  'title_es': "Declaración de incostitucionalidad",
  'date_es': '2023',
  'content_es': `
  La Corte Suprema de Justicia declara inconstitucional la resolución que dispuso rebeldía y orden de captura contra Belén Whittingslow.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2023_accioninconst.pdf'
};
