export default {
    // en
    'title_en': "Brief of deputy prosecutor Jorge Sosa.",
    'date_en': 'June 2015',
    'content_en': `
Ratifies the request to dismiss the complaint and file the case, qualifying the denounced events as “flirting or courtship”. In the resolution he emphasized that there was no teacher-student link at the time of the exchange of messages, ignoring the statement made by Mr. Kriskovich in a public deed, where he refers to Belén as “his pupil”. He recognizes the probative value of the copies of the messages provided by Belén but denies the existence of sexual harassment because he considers the exchange as being consensual, without mentioning or analyzing the various manifestations of refusal present in those messages, and completely dismissing the value of everything declared by Belén in her testimonial statements and in the different writings provided during the process.
    `,
  
    // es
    'title_es': "Escrito del Fiscal Adjunto Jorge Sosa.",
    'date_es': 'Junio 2015',
    'content_es': `
  Ratifica el requerimiento de desestimar la denuncia y archivar la causa, calificando el hecho denunciado como “galanteo o cortejo”. En dicha resolución hace hincapié en que no existía vínculo profesor-alumna al momento del intercambio de mensajes, desconociendo las manifestaciones realizadas por el Sr. Kriskovich en la escritura pública donde se refiere a Belén como “su discípula”, reconoce un valor probatorio a las copias de los mensajes aportadas por Belén pero niega la existencia del acoso sexual por considerar como consensuado el intercambio, sin mencionar ni analizar las diversas manifestaciones de negativa contenidos en esos mensajes, y dejando sin valor absoluto todo lo declarado por Belén en sus declaraciones testimoniales y en los diferentes escritos aportados en el proceso.
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_06_01_Ratificaci%C3%B3n_Desestimaci%C3%B3n.pdf'
  };