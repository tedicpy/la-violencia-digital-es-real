export default {
  // en
  'title_en': "Complaint brief.",
  'date_en': 'October 2016',
  'content_en': `
brief filed by Kriskovich's lawyers in order to promote a lawsuit for compensation for damages claiming the payment of 450,000 USD in concept of non-material damage arising as a result of “false statements” made against him which resulted in harm to his public image.
  `,

  // es
  'title_es': "Escrito de demanda.",
  'date_es': 'Octubre 2016',
  'content_es': `
Escrito presentado por abogados de Kriskovich con el objeto de promover demanda ordinaria de indemnización de daños y perjuicios reclamando la suma de 450.000 USD alegando daño moral surgido a consecuencia de las "declaraciones falsas" hechas en contra de su persona que dañaren su imagen pública.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Indemnizacion/2016_10_28_escrito_de_demanda_k_vs_bew_indemnizaci%C3%B3n.pdf'
};