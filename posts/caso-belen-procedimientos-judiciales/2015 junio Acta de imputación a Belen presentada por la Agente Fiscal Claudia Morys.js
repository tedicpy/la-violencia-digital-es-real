export default {
    // en
    'title_en': "Bill of indictment against Belén presented by prosecutor Claudia Morys.",
    'date_en': 'June 2015',
    'content_en': `
  Is accused of the punishable acts of “Production of non-authentic document” and “Alteration of data relevant to the test”. Four months of investigation are required.
    `,
  
    // es
    'title_es': "Acta de imputación a Belen presentada por la Agente Fiscal Claudia Morys.",
    'date_es': 'Junio 2015',
    'content_es': `
  Se le imputan los hechos punibles de producción de documento no auténtico y alteración de datos relevantes para la prueba. Se requiere 4 meses de investigación.
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Compra_Notas/2015_06_11_Acta_de_Imputaci%C3%B3n.pdf'
  };