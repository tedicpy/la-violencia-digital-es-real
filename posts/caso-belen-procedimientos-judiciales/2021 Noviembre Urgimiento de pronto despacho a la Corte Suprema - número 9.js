export default {
  // en
  'title_en': "Beseechment for prompt delivery to the Supreme Court – number 9",
  'date_en': '2021',
  'content_en': `
  Belén’s legal defense introduces for the ninth time the urgency to address the brief filed by the Defense in 2019 whereby it claims an act of unconstitutionality and requests the enforcement of the precautionary measure of suspension of the legal effects of the appealed resolution.
  `,

  // es
  'title_es': "Urgimiento de pronto despacho a la Corte Suprema - número 9",
  'date_es': '2021',
  'content_es': `
  La defensa de Belén presenta por novena vez el urgimiento para atender el escrito presentado por la Defensa en el año 2019 por medio del cual promueve acción de inconstitucionalidad y solicita que se imponga la medida cautelar de suspensión de los efectos de la resolución recurrida.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2021_Caso_Belen_9no_urgimiento_ y_comunicacion_peticion.jpg'
};
