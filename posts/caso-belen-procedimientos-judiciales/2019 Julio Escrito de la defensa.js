export default {
  // en
  'title_en': "Defense brief.",
  'date_en': 'July 2019',
  'content_en': `
Belén’s defense presents a complaint to the Superintendency against judge Lici Sánchez for serious irregularities. The irregularities refer to the cancellation of legal status without having issued the corresponding notification.
  `,

  // es
  'title_es': "Escrito de la defensa.",
  'date_es': 'Julio 2019',
  'content_es': `
La defensa de Belén presenta denuncia a la Superintendencia contra la Jueza Lici Sánchez por graves irregularidades. La irregularidades refieren a la cancelación de personería sin haberse realizado la notificación correspondiente.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Compra_Notas/2019_07_Denuncia_Jueza_Lici_S%C3%A1nchez_SC_07-19.pdf'
};