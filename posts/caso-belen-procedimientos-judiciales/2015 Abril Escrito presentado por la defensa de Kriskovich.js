export default {
    // en
    'title_en': "Brief presented by Kriskovich’s defense.",
    'date_en': 'April 2015',
    'content_en': `
  Request for the expertise of cellphones to be called off. Attached to that brief is an authenticated copy of Public Deed dated 9/6/14, claiming that the results of the technical expertise ordered by the Prosecutor's Office would be *superabundant*, since the existence of the messages is not contested by the defense.
    `,
  
    // es
    'title_es': "Escrito presentado por la defensa de Kriskovich.",
    'date_es': 'Abril 2015',
    'content_es': `
  Se solicita que se deje sin efecto el peritaje de los teléfonos celulares. A ese escrito adjunta la fotocopia autenticada de la Escritura Pública de fecha 6/09/14 alegando que el resultado de la pericia técnica ordenada por la Fiscalía sería *sobreabundante* en tanto la existencia de los mensajes no es objetado por la defensa.
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_04_21-II_Escrito_Agrega_Escritura_P%C3%BAblica_Kriskovich.pdf'
  };