export default {
    // en
    'title_en': "A.I. no. 423",
    'date_en': 'June 2015',
    'content_en': `
  The dismissal is ratified
    `,
  
    // es
    'title_es': "A.I. nro. 423",
    'date_es': 'Junio 2015',
    'content_es': `
  Se hace lugar a la desestimación
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_06_08_Auto_Hace_Lugar_a_Desestimaci%C3%B3n.pdf'
  };