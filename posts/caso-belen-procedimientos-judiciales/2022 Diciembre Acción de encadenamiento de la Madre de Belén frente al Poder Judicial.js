export default {
  // en
  'title_en': "Chaining action of Belens Mother against the Judiciary",
  'date_en': '2022',
  'content_en': `
  Mónica Cañete, Belén's mother, chained herself in front of the Judiciary, demanding the admissibility of the unconstitutionality action against the arrest warrant for her daughter carried out in 2019.
  `,

  // es
  'title_es': "Acción de encadenamiento de la Madre de Belén frente al Poder Judicial",
  'date_es': '2022',
  'content_es': `
  Mónica Cañete, la madre de Belén, se encadenó frente al Poder Judicial exigiendo la admisibilidad de la acción de inconstitucionalidad contra la orden de captura de su hija realizado en el año 2019.
  `,

  'enlaceExterno': 'https://www.ultimahora.com/madre-belen-whittingslow-se-encadena-frente-al-poder-judicial-n3040726.html'
};
