export default {
  // en
  'title_en': "Opposition brief.",
  'date_en': 'November 2017',
  'content_en': `
Brief presented by Kriskovich's lawyer where he opposes the expert evidence and objects to the points of expertise.
  `,

  // es
  'title_es': "Escrito de oposición.",
  'date_es': 'Noviembre 2017',
  'content_es': `
Escrito presentado por la representación de Kriskovich donde formula oposición a la prueba pericial y objeta los puntos de pericia.
  `,
};