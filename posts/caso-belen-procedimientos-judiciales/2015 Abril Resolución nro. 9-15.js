export default {
    // en
    'title_en': "Resolution no. 9/15",
    'date_en': 'April 2015',
    'content_en': `
  The expertise is called off, alleging that based on the brief submitted by the defense the performance of the technical expertise is unnecessary.
    `,
  
    // es
    'title_es': "Resolución nro. 9/15.",
    'date_es': 'Abril 2015',
    'content_es': `
  Se deja sin efecto la realización de la pericia alegando que en base al escrito presentado por la defensa, la realización de la pericia informática es innecesaria.
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_04_23_Resoluci%C3%B3n_Deja_sin_Efecto_Pericia.pdf'
  };