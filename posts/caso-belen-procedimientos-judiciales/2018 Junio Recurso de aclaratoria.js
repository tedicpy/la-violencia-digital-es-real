export default {
  // en
  'title_en': "Clarification appeal.",
  'date_en': 'June 2018',
  'content_en': `
Appeal filed by Mr. Kriskovich's lawyers.
  `,

  // es
  'title_es': "Recurso de aclaratoria.",
  'date_es': 'Junio 2018',
  'content_es': `
Recurso presentado por los abogados del Sr. Kriskovich.
  `,
};