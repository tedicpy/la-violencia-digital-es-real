export default {
  // en
  'title_en': "Senate Chamber Resolution.",
  'date_en': 'May 2019',
  'content_en': `
Resolution requesting the resignation of Kriskovich by virtue of the complaints presented.
  `,

  // es
  'title_es': "Resolución Cámara de Senadores.",
  'date_es': 'Mayo 2019',
  'content_es': `
Resolución solicitando renuncia de Kriscovich en virtud a las denuncias presesntadas.
  `,

  'documentoEnlace': ''
};