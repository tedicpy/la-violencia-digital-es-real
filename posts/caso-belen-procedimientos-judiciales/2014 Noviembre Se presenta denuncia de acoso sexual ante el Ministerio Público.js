export default {
    // en
    'title_en': "Asexual harassment complaint is filed before the Public Ministry",
    'date_en': 'November 2014',
    'content_en': `
Belén files a formal complaint of sexual harassment against Cristian Kriskovich before the Public Ministry.
        `,
  
    // es
    'title_es': "Se presenta denuncia de acoso sexual ante el Ministerio Público.",
    'date_es': 'Noviembre 2014',
    'content_es': `
Belén realiza una denuncia formal de acoso sexual contra Cristian Kriskovich ante el Ministerio Público.
        `,
  };