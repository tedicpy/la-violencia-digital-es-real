export default {
  // en
  'title_en': "A.I. no. 531.",
  'date_en': 'Diciembre 2018',
  'content_en': `
The Appeal Court of the Sixth District resolves to confirm Resolution No. 239 and its clarification (A.I. No. 318 dated June 29, 2018), and dismisses the lodged nullity motion. The Court bases its arguments on the fact that the expertise is “superfluous and superabundant”, since it had been debated in a criminal court.
  `,

  // es
  'title_es': "A.I. nro. 531.",
  'date_es': 'Diciembre 2018',
  'content_es': `
El tribunal de Apelación de la Sexta Sala resuelve confirmar la resolución nro. 239 y su aclaratoria (A.I. Nro. 318 de fecha 29 de junio del 2018) desestima el recurso de nulidad interpuesto. El Tribunal basa sus argumentos en que la pericia es “superflua y sobreabundante” por haber sido debatida en sede penal.
  `,
};