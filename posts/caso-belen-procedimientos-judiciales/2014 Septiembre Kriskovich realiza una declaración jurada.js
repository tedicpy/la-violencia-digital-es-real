export default {
  // en
  'title_en': "Kriskovich makes a sworn statement.",
  'date_en': 'September 2014',
  'content_en': `
Cristian Kriskovich makes an affidavit before a notary public, embodied in public deed no. 21 dated September 6, 2014, where he declared being aware of the fact that Belén was going to accuse him of sexual harassment and acknowledged the exchange of messages but questioned their scope. Moreover, he refers to Belén as his "pupil".
      `,

  // es
  'title_es': "Kriskovich realiza una declaración jurada.",
  'date_es': 'Septiembre 2014',
  'content_es': `
Cristian Kriskovich realiza ante escribanía una declaración jurada plasmada en la escritura pública nro. 21 de fecha 6 de Septiembre del 2014 donde deja constancia que en vista a que se ha enterado que Belen lo iba a denunciar por acoso; admite el intercambio de mensajes pero cuestiona el alcance de los mismos. Así mismo, se refiere a Belén como su "discipula".
      `,
};
