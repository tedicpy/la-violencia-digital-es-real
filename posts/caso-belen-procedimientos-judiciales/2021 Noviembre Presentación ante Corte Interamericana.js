export default {
  // en
  'title_en': "CIDH petition.",
  'date_en': '2021',
  'content_en': `
  TEDIC and CEJIL will file a petition before the Inter-American Commission on Human Rights (IACHR) against the Paraguayan state for discrimination, lack of access to justice, and lack of judicial independence in the case of Belén Whittingslow.
  `,

  // es
  'title_es': "Presentación ante Corte Interamericana.",
  'date_es': '2021',
  'content_es': `
TEDIC y CEJIl presentan la petición ante la Comisión del Sistema de la Corte Interamericana de Derechos Humanos.
  `,

  'enlaceExterno': 'https://www.tedic.org/tedic-y-cejil-presentan-una-peticion-ante-la-cidh-contra-el-estado-paraguayo/'
};
