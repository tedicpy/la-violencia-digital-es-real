export default {
    // en
    'title_en': "Plaintiff’s brief.",
    'date_en': 'April 2015',
    'content_en': `
Request to add instruments and request a hearing with the State Attorney General Javier Diaz Verón
    `,
  
    // es
    'title_es': "Escrito de la querella.",
    'date_es': 'Abril 2015',
    'content_es': `
Solicita agregar instrumentales y solicitar audiencia con el Fiscal General del Estado Javier Diaz Verón
    `,

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015%2005%2020%20Agrega%20Instrumentales%20Oposici%C3%B3n.pdf'
  };