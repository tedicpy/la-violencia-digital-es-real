export default {
  // en
  'title_en': "Unconstitutionality action admissibility by the Supreme Court",
  'date_en': '2022',
  'content_en': `
  On December 29, 2022, the Court admitted the Unconstitutional action to analyze the arrest warrant against Belén and its unconstitutionality.
  `,

  // es
  'title_es': "Admisibilidad de la acción de inconstitucionalidad por parte de la Corte Suprema",
  'date_es': '2022',
  'content_es': `
  El 29 de diciembre de 2022 la Corte admite la acción de Inconstitucional para analizar la orden de captura contra Belén y su inconstitucionalidad.
  `,

  'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2022_Admisión_de_Acción_de_Inconstitucionalidad_AI_2137_de_29_diciembre_2022.pdf'
};
