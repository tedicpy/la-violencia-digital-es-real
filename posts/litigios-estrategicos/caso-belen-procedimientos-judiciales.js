export default {
  // en
  'title_en': "Visualization of court proceedings",
  'content_en': `
This section presents the judicial procedures of the three files that have been mentioned in the previous section. By clicking on any entry you can access the documents presented by the Belén's legal team as well as the sentences.
    `,
    'visualizacion_en': {
        'descriptionResumen': "This section presents the judicial procedures of the three files that have been mentioned in the previous section. By clicking you can access the documents presented by the defense of Belén as well as the sentences.",
        'eventosPrincipales':
        [{
            'title': 'File a complaint',
            'caso': 'Harassment Case',
            'color': 'brand-indigo-3',
            'year': '2014'
        }, {
            'title': 'Invetigative Report override',
            'caso': 'Harassment Case',
            'color': 'brand-indigo-3',
            'year': '2015'
        }, {
            'title': 'Denial of complaint',
            'caso': 'Harassment Case',
            'color': 'brand-indigo-3',
            'year': '2015'
        }, {
            'title': 'Imputation of Belén',
            'caso': 'Catholic University Case',
            'color': 'brand-pink-3',
            'year': '2015',
        }, {
            'title': 'Confirm dismissal',
            'caso': 'Harassment case',
            'color': 'brand-indigo-3',
            'year': '2015'
        }, {
            'title': 'Accusation against Belén',
            'caso': 'Catholic University Case',
            'color': 'brand-pink-3',
            'year': '2015'
        }, {
            'title': 'Demand compensation',
            'caso': 'Indemnification Case',
            'color': 'brand-green-2',
            'year': '2016'
        }, {
            'title': 'Mobile phone report exclusion',
            'caso': 'Indemnification Case',
            'color': 'brand-green-2',
            'year': '2018'
        }, {
            'title': 'Dismisses nullity',
            'caso': 'Indemnification Case',
            'color': 'brand-green-2',
            'year': '2018'
        }, {
            'title': 'Capture warrant against Belén',
            'caso': 'Catholic University Case',
            'color': 'brand-pink-3',
            'year': '2019'
        }, {
            'title': 'Case is presented to the IACHR',
            'caso': 'Harassment Case',
            'color': 'brand-green-2',
            'year': '2021'
        }, {
            'title': 'The IACHR system registers the case in the registration section and notifies the Paraguayan State to present its observations. At this stage, the Commission will analyze whether or not the case is admissible in Court',
            'caso': 'Harassment Case',
            'color': 'brand-green-2',
            'year': '2022'
        }, {
            'title': 'Chaining action of Belen\'s Mother against the Judiciary',
            'caso': 'Harassment Case',
            'color': 'brand-green-2',
            'year': '2022'
        }, {
            'title': 'Unconstitutionality action admissibility by the Supreme Court',
            'caso': 'Harassment Case',
            'color': 'brand-green-2',
            'year': '2022'
        }, {
            'title': 'Request for transfer and notification of the admissibility of the action of unconstitutionality to the Prosecutor General\'s Office',
            'caso': 'Harassment Case',
            'color': 'brand-green-2',
            'year': '2023'
        }, {
            'title': 'The Prosecutor General\'s Office requests an interruption of the deadline to reply to the unconstitutionality action',
            'caso': 'Harassment Case',
            'color': 'brand-green-2',
            'year': '2023'
        }, {
            'title': 'Unconstitutional declaration',
            'caso': 'Harassment Case',
            'color': 'brand-green-2',
            'year': '2023'
        }, {
            'title': 'Visit from Vatican envoy',
            'caso': 'Harassment Case',
            'color': 'brand-green-2',
            'year': '2023'
        }]
    },

    // es
    'title_es': "Visualización de los procedimientos judiciales",
    'content_es': `
En esta sección se exponen los procedimientos judiciales de los tres expedientes que han sido mencionados en el apartado anterior. Clickeando podes acceder a los documentos presentados por la defensa de Belén así como a las sentencias.
    `,
    'visualizacion_es': {
        'descriptionResumen': "En esta sección se exponen los procedimientos judiciales de los tres expedientes que han sido mencionados en el apartado anterior. Clickeando podes acceder a los documentos presentados por la defensa de Belén así como a las sentencias.",
        'eventosPrincipales': [{
            'title': 'Presenta denuncia',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2014'
        }, {
            'title': 'Anulación pericia',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2015'
        }, {
            'title': 'Desestimación denuncia',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2015'
        }, {
            'title': 'Imputación a Belén',
            'caso': 'Caso “Compra-notas”',
            'color': 'brand-pink-3',
            'year': '2015',
        }, {
            'title': 'Confirma  desestimación',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2015'
        }, {
            'title': 'Acusación contra Belén',
            'caso': 'Caso “Compra-notas”',
            'color': 'brand-pink-3',
            'year': '2015'
        }, {
            'title': 'Demanda indemnización',
            'caso': 'Caso Indemnización',
            'color': 'brand-green-2',
            'year': '2016',
        }, {
            'title': 'Exclusión pericia celular',
            'caso': 'Caso Indemnización',
            'color': 'brand-green-2',
            'year': '2018'
        }, {
            'title': 'Desestima nulidad',
            'caso': 'Caso Indemnización',
            'color': 'brand-green-2',
            'year': '2018'
        }, {
            'title': 'Orden de captura contra Belén',
            'caso': 'Caso “Compra-notas”',
            'color': 'brand-pink-3',
            'year': '2019'
        }, {
            'title': 'Se presenta el caso ante la Comisión del Sistema de la CIDH',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2021'
        }, {
            'title': 'El sistema de la CIDH realiza el registro del caso en la sección de registro y notifica al Estado paraguayo para que presente sus observaciones. En esta etapa la Comisión analizará si es admisible o no el caso ante la Corte',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2022'
        }, {
            'title': 'Acción de encadenamiento de la madre de Belén frente al Poder Judicial',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2022'
        }, {
            'title': 'Admisibilidad de la acción de inconstitucionalidad por parte de la Corte Suprema',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2022'
        }, {
            'title': 'Solicitud de traslado y notificación de la admisibilidad de la acción de inconstitucionales a la fiscalía',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2023'
        }, {
            'title': 'La Fiscalía General solicita interrupción del plazo para contestar la acción de inconstitucionalidad',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2023'
        }, {
              'title': 'Declaración de incostitucionalidad',
              'caso': 'Caso acoso',
              'color': 'brand-indigo-3',
              'year': '2023'
        }, {
            'title': 'Visita de enviados del Vaticano',
            'caso': 'Caso acoso',
            'color': 'brand-indigo-3',
            'year': '2023'
        }]
    }
};
