export default [
  { // item referenciado desde el menú, mantener posición en la lista
    'en': "belen-case",
    'es': "caso-belen",
    'mainImage': "litigio1.png",
    'mainImageThumb': "litigio1-thumb.jpg",
    'mainImageClass': '-mt-10 w-80 lg:-mt-12 lg:w-1/3 ',
    'bgColor': "brand-green-3",
    'contentClass': 'bg-brand-indigo-1 text-white',
    'contentTitleColor':'text-brand-green-2 ',
    'heroImage': 'mensajes-con-angulos.jpg'
  },
  { // item referenciado desde "caso-belen", mantener posición en la lista
    'en': "case-belen-judicial-proceedings",
    'es': "caso-belen-procedimientos-judiciales",
    'mainImage': "litigio1.png",
    'mainImageThumb': "litigio1-thumb.jpg",
    'bgColor': "brand-green-3",
    'hidden': true // no se mostrará en las listas de posts
  },
  {
    'en': "censorship-of-publication-on-sexist-violence",
    'es': "censura-a-publicacion-sobre-violencia-machista",
    'mainImage': "litigio2.png",
    'mainImageThumb': "litigio3-thumb.jpg",
    'bgColor': "brand-green-3",
    'externalPageUrl': 'https://www.tedic.org/buscan-censurar-una-publicacion-de-tedic-sobre-violencia-machista-en-internet/'
  },
  {
    'en': "court-revokes-sentence",
    'es': "tribunal-revoca-sentencia",
    'mainImage': "litigio3.png",
    'mainImageThumb': "litigio2-thumb.jpg",
    'bgColor': "brand-green-3",
    'externalPageUrl': 'https://www.tedic.org/buena-noticia-tribunal-revoca-sentencia-que-censuraba-a-tedic-por-denunciar-violencia-de-genero/'
  }
]
