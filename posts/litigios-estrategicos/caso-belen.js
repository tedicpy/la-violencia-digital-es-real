export default {
  // en
  'title_en': "Belén Whittingslow case",
  'content_en': `
### Case Summary

The events reported by Belén date back to 2013, where she claimed that during her time as a student of Cristian Kriskovich, he sexually harassed her through messages and images sent through WhatsApp [3].

Despite having ordered a report and investigation of the mobile phones - the case's main evidence supporting Belén's claim - the investigation ceased and the case was dismissed after documentation on behalf of the defense of Mr. Kriskovich was presented to the prosecutor [4].

Such decision was ratified by the deputy prosecutor, who considered that the actions of Mr. Kriskovich did not constitute sexual harassment, categorizing it as "romancing and courtship" [5] in a contra legem resolution. This is how, in this process, the Public Ministry completely rejected Belén's responses clearly indicating her opposition and rejection of the type of exchanges tainted with gender bias and the clear avoidance of the analysis of the position of power held by Mr. Kriskovich [6].

After the cases' dismissal, Belén was involved in two legal proceedings against her, which continue to date. Belén was accused of being part of a criminal case investigated at her university regarding the "production of non-authentic documentation" [7] and as  defendant in the civil sphere where she was sued by Mr. Kriskovich for  compensation for damages in which he claims the payment of USD 450,000  [8].

In both legal processes, Belén has offered the reports of mobile phones as proof of the alleged sexual harassment and the connection with the subsequent processes. This offer was denied in both cases. Within the former case presented (involving the alleged "production of non-authentic documentation")  a resolution has been issued declaring Belén's rebellion, ordering her capture, and canceling her defense, and ordering the removal of her lawyers from the computer system granting access to files, without any legal grounds [9]. An unconstitutionality action was presented before the aforementioned actions, which has not been resolved to date.

Faced with the fear caused by the inconsistency of the justice system in Paraguay, the possibility of an arrest, and her vulnerable position, Belén is now in Uruguay where she has submitted a refugee status request. Her wish is not to evade the proceedings against her, but to have the necessary guarantees that she will be able to have an oral trial with full access to her legal defense and full compliance of her human rights. 

The Inter-American Commission on Human Rights (IACHR) has become aware of Belén's case in 2019. This has been possible through a joint action between TEDIC, CEJIL and Belén's family who have send a Letter Article 41 requesting information from Paraguay on the measures adopted within her case.

*[3] Case No. 8830/2014, titled “Cristian Kriskovich on Sexual Harassment”.*

*[4] Resolution No. 9 of April 23, 2015, issued by the prosecutor Centurión, Case No. 8830/2014.*

*[5] Opinion No. 735, delivered by the deputy prosecutor Jorge Sosa, folios 4 and 5, Case No. 8830/2014.*

*[6] Opinion No. 735 of June 1, 2015, issued by the deputy prosecutor Jorge Sosa, Case No. 8830/2014.*

*[7] Case No. 2882/14 titled "Víctor David Arce Y otros S / Production of Non-Authentic Documents"*

*[8] Case No. 445/2016 titled "Cristian Daniel Kriskovich de Vargas c/ María Belén Whittingslow Castañé s/ indemnización de daños y perjuicios por responsabilidad extracontractual.”*

*[9] A.I N ° 625 of June 21, 2019, Case N ° 2882/14.*

      `,
    'pageDescription_en': `
**This case has the power to shape our understanding of gender based online violence and bring to light the lack of access to justice in Paraguay.**

Belén Whittingslow reported Cristian Kriskovich, her professor the Catholic University of Asunción, for sexual harassment through digital means. 

Cristian Kriskovich is not only representative of the University before the Council of the Judiciary (central authority in the designation and appointment of judges and prosecutors [1]), but also serves in the Jury for the Prosecution of Magistrates (authority with power to sanction magistrates and prosecutors [2]).

Belén's case was dismissed and she was forced, under the circumstances, to seek asylum in Uruguay after being prosecuted under false allegations.

*[1] Article 264 of the National Constitution of Paraguay.*

*[2] Article 11 of Law No. 3759/2009.*
    `,
    'heroImageDescription_en': "Text messaged offered as part of the reports on Belén's mobile phone.",

  // es
  'title_es': "Caso Belén Whittingslow",
  'content_es': `
### Resumen del Caso

Los hechos denunciados por Belén se remontan al año 2013, cuando alegó que, siendo alumna de la Universidad Católica, el Sr. Kriskovich – quien fuere su profesor- la acosó sexualmente a través de mensajes e imágenes enviados por la aplicación de mensajería instantánea WhatsApp[3]. A pesar de haberse ordenado la realización de la pericia de los celulares –principal prueba del acoso denunciado–, tras un escrito presentado por la defensa del Sr. Kriskovich, el fiscal del caso decidió desconvocar las diligencias de la pericia y desestimar la causa[4]. Tal decisión fue ratificada por el fiscal adjunto, quien consideró que el hecho denunciado no configuraba acoso sexual, calificándolo como “galanteo o cortejo”[5] en una resolución contra legem. Es así como, en este proceso, el Ministerio Público desestimó por completo las respuestas de Belén que indican con claridad su molestia y oposición a ese tipo de intercambio en base a argumentos sustentados en prejuicios de género y evitando analizar la posición de poder ostentada por el Sr. Kriskovich[6].

Luego de la desestimación, Belén fue involucrada en dos procesos judiciales en su contra, que continúan hasta la fecha, como imputada en el ámbito penal dentro de la causa investigada en la Facultad Católica de “producción de documento no auténtico”[7] y como demandada en el ámbito civil consistente en una demanda por parte del Sr. Kriskovich por indemnización de daños y perjuicios en la que reclama el pago de 450.000 USD[8]. En ambos procesos Belén ha ofrecido la pericia de los celulares como prueba del acoso sexual alegado y la conexión con los procesos posteriores; siendo le denegado en ambos procedimientos. Dentro del procedimiento penal mencionado, se ha dictado una resolución donde se declara la rebeldía de Belén, se ordena su captura y se cancela su defensa disponiendo la desvinculación de sus abogados del sistema informático de acceso al expediente sin fundamentos legales[9]. Ante dicha resolución se presentó una acción de inconstitucionalidad que no ha sido resuelta hasta la fecha.
![Título de la imágen {image-center}](/blog-files/litigios-estrategicos/belen-blog.png)

Ante el temor causado por la arbitrariedad de la justicia, la posibilidad de una detención y su situación de indefensión, Belén se encuentra ahora en Uruguay donde tramita una solicitud de refugio. Su deseo no es eludir los procesos, sino contar con las garantías necesarias de que podrá tener un juicio oral con pleno acceso a su defensa legal y sus derechos.

La Comisión Interamericana de Derechos Humanos (CIDH) ha tomado conocimiento del caso de Belén ya que CEJIL, TEDIC y la familia han enviado con fecha 31 de octubre del 2019 una Carta artículo 41 para que la CIDH solicite información a Paraguay sobre las medidas que está adoptando en el caso. Dicha carta fue tramitada por la CIDH, aunque desconocemos la información aportada por el Estado en respuesta de esta.

*[3] Causa N.o 8830/2014, caratulada “Cristian Kriskovich s/ Acoso sexual”.*

*[4] Resolución N.o 9 del 23 de abril de 2015, proferida por el fiscal Centurión, Causa N.o 8830/2014.*

*[5] Dictamen N.o 735, proferido por el fiscal adjunto Jorge Sosa, folios 4 y 5, Causa N.o 8830/2014.*

*[6] Dictamen N.o 735 del 1 de junio del 2015, proferido por el fiscal adjunto Jorge Sosa, Causa N.o 8830/2014.*

*[7] Causa N°2882/14 caratulada “Víctor David Arce Y Otros S/ Producción De Documentos No Auténticos”*

*[8] Causa N°445/2016 caratulada “Cristian Daniel Kriskovich de Vargas c/ María Belén Whittingslow Castañé s/ indemnización de daños y perjuicios por responsabilidad extracontractual”*

*[9] A.I N°625 del 21 de junio del 2019, Causa N°2882/14.*
      `,
    'pageDescription_es':`
**Este caso configuraría violencia de género en línea e ilustra la falta de acceso a la justicia en Paraguay.**

Belén Whittingslow denunció a Cristian Kriskovich, profesor de la Universidad Católica de Asunción y representante de la misma ante el Consejo de la Magistratura, órgano central en el proceso de designación y nombramiento de jueces y agentes fiscales[1], y el Jurado de Enjuiciamiento de Magistrados, órgano con facultades para sancionar a magistrados y agentes fiscales[2] por acoso sexual (llevado a cabo a través de medios digitales). El caso fue desestimado y ella terminó siendo solicitante de refugio en Uruguay tras alegar haber sido perseguida judicialmente.

*[1] Artículo 264 de la Constitución Nacional del Paraguay.*

*[2] Artículo 11 de la Ley N.o 3759/2009.*
    `,

    'heroImageDescription_es': 'Texto de mensajes ofrecidos como parte de la pericia telefónica provenientes del celular de Belén.'
};
