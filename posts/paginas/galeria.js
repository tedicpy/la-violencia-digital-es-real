export default {
  // en
  // 'images_en': [{
  //   file: 'galeryimage1.png'
  // }],

  // es
  // 'images_en': [{
  //   file: 'galeryimage1.png'
  // }],

  'images': [
    { file: "lanzamiento.png" },
    { file: "mensaje 2 - 01.png" },
    { file: "mensaje 2 - 02.png" },
    { file: "mensaje 2 - 03.png" },
    { file: "mensaje 3 - 01.png" },
    { file: "mensaje 3 - 02.png" },
    { file: "mensaje 3 - 03.png" },
    { file: "mensaje 3 - 04.png" },
    { file: "mensaje 3 - 05.png" },
    { file: "mensaje 3 - 06.png" },
    { file: "mensaje 4 - 01.png" },
    { file: "mensaje 4 - 02.png" },
    { file: "mensaje 4 - 03.png" },
    { file: "mensaje 4 - 04.png" },
    { file: "mensaje 4 - 05.png" },
    { file: "mensaje 5 - 01.png" },
    { file: "mensaje 5 - 02.png" },
    { file: "mensaje 6 - 01.png" },
    { file: "mensaje 6 - 02.png" },
    { file: "mensaje 6 - 03.png" },
    { file: "mensaje 7 - 01.png" },
    { file: "mensaje 7 - 02.png" },
  ]
  
};
