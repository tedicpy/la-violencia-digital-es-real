export default {
  'customHeader_en': true,
  'customHeader_es': true,

  // en
  'investigaciones_title_en': "RESEARCH",
  'investigaciones_description_en': "Discover and download all our research on technology-facilitated gender violence conducted by TEDIC.",
  'descargar_investigacion_en': "download research",
  'spanish_download_en': "spanish",
  'english_download_en': "english",
  'title_en': "Gender violence on the Internet in Paraguay",
  'description_en': "Discover and download all our research on technology-facilitated gender violence conducted by TEDIC.",
  'investigaciones_en': [
    {
      titulo: "Technology-facilitated gender-based violence against women politicians",
      fecha: "October 15th, 2024",
      descripcion: "Technology-facilitated gender violence is a global issue that goes beyond the physical, with tangible impacts on its victims. This research analyzes the specific patterns in Paraguay to develop evidence-based social, legal, and policy solutions, considering both survivors and perpetrators.",
      imagenSrc: "/assets/investigaciones/1_eng.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/09/Violencia-de-genero-a-mujeres-politicas-WEB-1.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/09/WEB.pdf"
    },
    {
      titulo: "Trafficking in persons and sexual exploitation as they intersect with ICTs in Paraguay",
      fecha: "August 26th, 2024",
      descripcion: "The use of Information and Communication Technologies (ICT) in recruiting human trafficking victims for sexual exploitation—both offline and online—is a current issue in our country that calls for direct, evidence-based action from public policy makers.",
      imagenSrc: "/assets/investigaciones/2_eng.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/08/Investigacion-Luna-Nueva-v03.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/08/Investigacion-Luna-Nueva-ENG.pdf"
    },
    {
      titulo: "Common ontology for definitions of online gender-based violence and other terms",
      fecha: "July 4th, 2024",
      descripcion: "The 'Better Data' project aims to unify terminologies and adapt local frameworks to understand and address online gender violence from an inclusive, data-driven perspective, prioritizing initiatives from the Global South.",
      imagenSrc: "/assets/investigaciones/3_eng.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/07/Definiciones-Violencia-de-Genero-Online-1.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/07/Framework-definitions-OGBV-2.pdf"
    },
    {
      titulo: "Perpetrators of online gender-based violence",
      fecha: "July 4th, 2024",
      descripcion: "This project seeks to understand and close the knowledge gaps regarding online gender violence perpetrators through a research roadmap that synthesizes existing studies, identifies key areas for future research, and proposes effective strategies and methodologies to guide interventions.",
      imagenSrc: "/assets/investigaciones/4_eng.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/07/Perpetradores-de-violencia-de-genero-online-1.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/07/Perpetrators-of-gender-based-violence-online.pdf"
    },
    {
      titulo: "Possible misrepresentations of law 5777/16",
      fecha: "May 7th, 2024",
      descripcion: "This study analyzes Law 5777/16 in Paraguay, highlighting its impact on freedom of expression and the need for a balance between the protection of women's rights and democratic principles, through a constitutional, international, and relevant legal case review.",
      imagenSrc: "/assets/investigaciones/5_eng.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/05/Tergiversaciones-Ley-5777-web.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/05/Tergiversaciones-Ley-5777-EN-WEB.pdf"
    },
    {
      titulo: "Digital gender violence against journalists in Paraguay",
      fecha: "October 31st, 2023",
      descripcion: "This research delves into digital violence against women journalists in Paraguay, analyzing its causes, consequences, and impact on freedom of expression as well as the role of the media. It aims to propose recommendations for public policies and protective strategies.",
      imagenSrc: "/assets/investigaciones/6_eng.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2023/10/Violencia-Genero-Periodistas-TEDIC-2023-web-2.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2023/10/Violencia-Genero-Periodistas-TEDIC-2023-ENG-web-1.pdf"
    },
    {
      titulo: "Gender Violence on the Internet in Paraguay",
      fecha: "August 14th, 2021",
      descripcion: "This groundbreaking study examines online gender violence in Paraguay, revealing its manifestations, impact, and challenges. It seeks to raise awareness, inform, and propose recommendations to address this emerging issue and protect women's rights in the digital realm.",
      imagenSrc: "/assets/investigaciones/7_eng.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2021/08/Violencia-Digital-TEDIC-WRO-2021-ES-v01.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2021/08/Violencia-Digital-TEDIC-WRO-2021-EN.pdf"
    },
    {
      titulo: "Non-consensual Distribution of Intimate Images",
      fecha: "June 6th, 2021",
      descripcion: "This study examines the issue of non-consensual distribution of intimate images in Paraguay, analyzing the legal framework, victims' experiences, and recommendations for addressing it. It aims to raise awareness, promote prevention, and improve institutional responses to this form of digital violence.",
      imagenSrc: "/assets/investigaciones/8_eng.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2021/09/Imagen-no-consentida-Tedic-web.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2021/09/Imagen-no-consentida-Tedic-web-EN.pdf"
    }
  ],

  // es
  'investigaciones_title_es': "INVESTIGACIONES",
  'investigaciones_description_es': "Conocé y descargá todas nuestras investigaciones sobre violencia de género facilitada por la tecnología realizadas por TEDIC.",
  'descargar_investigacion_es': "descargar investigación",
  'spanish_download_es': "español",
  'english_download_es': "inglés",
  'title_es': "Violencia de género en Internet en Paraguay",
  'description_es': "Conocé y descargá todas nuestras investigaciones sobre violencia de género facilitada por la tecnología realizadas por TEDIC.",
  'investigaciones_es': [
    {
      titulo: "Violencia de género facilitada por la tecnología a mujeres políticas",
      fecha: "15 de octubre, 2024",
      descripcion: "La violencia de género facilitada por la tecnología es una problemática global que trasciende lo físico, con impactos tangibles para las víctimas. Esta investigación analiza los patrones específicos de Paraguay para desarrollar soluciones sociales, jurídicas y políticas basadas en evidencia, comprendiendo tanto a las sobrevivientes como a los perpetradores.",
      imagenSrc: "/assets/investigaciones/1.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/09/Violencia-de-genero-a-mujeres-politicas-WEB-1.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/09/WEB.pdf"
    },
    {
      titulo: "Trata de personas y explotación sexual en su intersección con las TIC en Paraguay",
      fecha: "26 de agosto, 2024",
      descripcion: "El uso de las Tecnologías de Información y Comunicación (TIC) para procesos de captación de víctimas de trata, para fines de explotación sexual y explotación sexual en línea, está vigente en nuestro país y requiere de la atención de hacedores de políticas públicas para una lucha frontal, basada en la evidencia.",
      imagenSrc: "/assets/investigaciones/2.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/08/Investigacion-Luna-Nueva-v03.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/08/Investigacion-Luna-Nueva-ENG.pdf"
    },
    {
      titulo: "Marco común para definiciones de la violencia de género en línea y otros términos",
      fecha: "4 de julio, 2024",
      descripcion: 'El proyecto "Better Data" busca unificar terminologías y adaptar marcos locales para comprender y abordar la violencia de género en línea desde una perspectiva inclusiva y basada en datos, priorizando iniciativas del Sur Global.',
      imagenSrc: "/assets/investigaciones/3.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/07/Definiciones-Violencia-de-Genero-Online-1.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/07/Framework-definitions-OGBV-2.pdf"
    },
    {
      titulo: "Perpetradores de violencia de género en línea",
      fecha: "4 de julio, 2024",
      descripcion: "El proyecto busca comprender y cerrar las lagunas de conocimiento sobre los perpetradores de violencia de género en línea, mediante una hoja de ruta de investigación que sintetiza estudios existentes, identifica áreas clave para futuras investigaciones y propone estrategias y metodologías efectivas para guiar intervenciones.",
      imagenSrc: "/assets/investigaciones/4.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/07/Perpetradores-de-violencia-de-genero-online-1.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/07/Perpetrators-of-gender-based-violence-online.pdf"
    },
    {
      titulo: "Posibles tergiversaciones de la ley 5777/16",
      fecha: "7 de mayo, 2024",
      descripcion: "Este estudio analiza la Ley 5777/16 en Paraguay, destacando su impacto en la libertad de expresión y la necesidad de un equilibrio entre la protección de los derechos de las mujeres y los principios democráticos, a través de un examen constitucional, internacional y de casos legales relevantes.",
      imagenSrc: "/assets/investigaciones/5.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2024/05/Tergiversaciones-Ley-5777-web.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2024/05/Tergiversaciones-Ley-5777-EN-WEB.pdf"
    },
    {
      titulo: "Violencia digital de género a periodistas en Paraguay",
      fecha: "31 de octubre, 2023",
      descripcion: "Esta investigación profundiza en la violencia digital contra mujeres periodistas en Paraguay, analizando las causas, consecuencias e impacto en la libertad de expresión y el rol de los medios. Busca proponer recomendaciones para políticas públicas y estrategias de protección.",
      imagenSrc: "/assets/investigaciones/6.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2023/10/Violencia-Genero-Periodistas-TEDIC-2023-web-2.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2023/10/Violencia-Genero-Periodistas-TEDIC-2023-ENG-web-1.pdf"
    },
    {
      titulo: "Violencia de género en Internet en Paraguay",
      fecha: "14 de agosto, 2021",
      descripcion: "Este estudio pionero examina la violencia de género en línea en Paraguay, revelando sus manifestaciones, impacto y desafíos. Busca sensibilizar, informar y proponer recomendaciones para abordar esta problemática emergente y proteger los derechos de las mujeres en el entorno digital.",
      imagenSrc: "/assets/investigaciones/7.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2021/08/Violencia-Digital-TEDIC-WRO-2021-ES-v01.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2021/08/Violencia-Digital-TEDIC-WRO-2021-EN.pdf"
    },
    {
      titulo: "Difusión de imagen íntima no consentida",
      fecha: "6 de junio, 2021",
      descripcion: "Este estudio examina la problemática de la difusión no consentida de imágenes íntimas en Paraguay, analizando el marco legal, las experiencias de las víctimas y las recomendaciones para abordarla. Busca generar conciencia, promover la prevención y mejorar la respuesta institucional ante este tipo de violencia digital.",
      imagenSrc: "/assets/investigaciones/8.png",
      descargaEsUrl: "https://www.tedic.org/wp-content/uploads/2021/09/Imagen-no-consentida-Tedic-web.pdf",
      descargaEnUrl: "https://www.tedic.org/wp-content/uploads/2021/09/Imagen-no-consentida-Tedic-web-EN.pdf"
    }
  ]
};
