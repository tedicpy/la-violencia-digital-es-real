export default [
  { // item referenciado desde el menú, mantener posición en la lista
    'en': "gender-based-violence-on-the-internet-in-paraguay",
    'es': "violencia-de-genero-en-internet-en-paraguay",
    'mainImage': "hero-i.png",
    'bgColor': "brand-pink-3",
    'titleColor': "text-white",
  },
  { // item referenciado desde la sección empresas sumadas de la portada, mantener posición en la lista
    'en': "empresas-sumadas",
    'es': "empresas-sumadas",
    'mainImage': "acompanada.png",
    'bgColor': "brand-red-4",
  },
  {
    'en': "gender-violence",
    'es': "violencia-de-genero",
    'mainImage': "hero-a.png",
    'bgColor': "brand-indigo-3",
    'titleColor': "text-white",
  },
  {
    'en': "galeria",
    'es': "galeria"
  }
];