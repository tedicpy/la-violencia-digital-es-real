export default {
  // en
  'title_en': "United we are stronger to resist on the Internet!",
  'content_en': `
At a time when technologies are practically extensions of what we do and think, the digital context also becomes a hostile space for women and other vulnerable populations.

This campaign is to speak, identify and know the impact of this phenomenon in our lives and join forces to mitigate and win among all.    
    `,

  // es
  'title_es': "¡Unidas somos mas fuerte para resistir en Internet!",
  'content_es': `
En una época en la que las tecnologías son prácticamente extensiones de lo que hacemos y pensamos, el contexto digital también se convierte en un espacio hostil para las mujeres y otras poblaciones vulneradas.

Esta campaña es para hablar, identificar y conocer el impacto de este fenómeno en nuestras vidas y unir más fuerza para mitigar y vencer entre todes.
    `,
};
