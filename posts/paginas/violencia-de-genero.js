export default {
    // en
    'title_en': "WHAT IS GENDER VIOLENCE?",
    'intro_en': `
Technology-facilitated gender-based violence is “any act that is committed, assisted, aggravated or amplified by  the  use  of  information communication  technologies  or  other  digital  tools  which   results   in   or   is   likely   to   result   in   physical,   sexual,   psychological,   social,   political   or   economic  harm or other infringements of rights and freedoms. **([UN, 2023](https://unric.org/en/how-technology-facilitated-gender-based-violence-impacts-women-and-girls/))**

Digital, or online, gender-based violence refers to acts of gender-based violence committed, instigated, or aggravated, in part or entirely, by the use of Information and Communication Technologies (ICT), social networking platforms and email. **([APC 2015](https://www.genderit.org/articles/impunity-justice-exploring-corporate-and-legal-remedies-technology-related-violence-against))**

There is still no global consensus on the definition of digital gender-based violence. However, it happens when it is committed and spread through digital media, against a woman -for being a woman-, or when it affects women disproportionately. **([UN Women, 2020](https://www.unwomen.org/-/media/headquarters/attachments/sections/library/publications/2020/brief-online-and-ict-facilitated-violence-against-women-and-girls-during-covid-19-en.pdf?la=en&vs=2519))**

This violence causes psychological and emotional damage, reinforces prejudice, damages reputation, causes economic loss and poses barriers to participation in public life. It can also lead to sexual and other forms of physical violence. **([Luchadoras 2017](https://luchadoras.mx/wp-content/uploads/2017/12/Informe_ViolenciaEnLineaMexico_InternetEsNuestra.pdf))**
    `,
    'content_en': `

![Violence as a monster attacking women {image-right}](/blog-files/paginas/blog-00.png)

"Digital or online gender violence refers to acts of gender violence committed instigated or aggravated, in part or totally, by the use of Information and Communication Technologies (ICT), social media platforms and email." **([APC 2015](https://www.genderit.org/articles/impunity-justice-exploring-corporate-and-legal-remedies-technology-related-violence-against))**

"There is still no global consensus on the definition of gender violence facilitated by technology. However, it is understood when it is committed and spreads through digital media, against a woman –because she is a woman–, or when it affects women disproportionately." **([UN Women, 2020](https://www.unwomen.org/-/media/headquarters/attachments/sections/library/publications/2020/brief-online-and-ict-facilitated-violence-against-women-and-girls-during-covid-19-en.pdf?la=en&vs=2519))**

"Technology-facilitated gender-based violence is “any act that is committed, assisted, aggravated or amplified by  the  use  of  information communication  technologies  or  other  digital  tools  which   results   in   or   is   likely   to   result   in   physical,   sexual,   psychological,   social,   political   or   economic  harm or other infringements of rights and freedoms." **([UN, 2023](https://unric.org/en/how-technology-facilitated-gender-based-violence-impacts-women-and-girls/))**

"This violence causes psychological and emotional damage, reinforces prejudices, damages reputation, causes economic loss, and poses barriers to participation in public life. It can also lead to forms of sexual violence and other forms of physical violence." **([Luchadoras, 2017](https://luchadoras.mx/wp-content/uploads/2024/10/Informe_ViolenciaEnLineaMexico_InternetEsNuestra.pdf))**

## Background

### 2022: Agreement on Protective Measures for Women in Situations of Gender-Based Violence

-  During the Pro-Tempore Presidency held by Argentina, Mercosur issued the "Mutual regional recognition of protection measures for women in gender-based violence situations". This agreement extends the protection for women victims of gender-based violence across Mercosur countries and Associated States. **([Mercosur, 2022](https://normas.mercosur.int/simfiles/normativas/73855_REC_004-2019_ES_Reconocimiento%20mutuo%20medidas%20proteccion.pdf))**

### 2021:  Disproportionate Impact of Technology-Facilitated Violence on Freedom of Expression

-  In her report to the General Assembly, UN Special Rapporteur Irene Khan highlighted the disproportionate impact of digital gender-based violence on freedom of opinion and expression. She made recommendations to create a safe digital environment.
**([UN, 2021](https://undocs.org/es/A/76/258))**

### 2020:  Eradicating technology-facilitated gender-based violence against women journalists

-  Dubravka Šimonović, Special Rapporteur on violence against women, outlined the specific risks faced by women journalists due to technology-facilitated gender-based violence.
**([UN, 2020](https://undocs.org/es/A/HRC/44/52))**

### 2018:  Definition of digital gender-based violence

-  In her report, the UN Special Rapporteur on violence against women, Dubravka Šimonović, defined online violence against women as any act of gender-based violence against women that is committed, assisted or aggravated in part or fully by the use of ICT, such as mobile phones and smartphones, the Internet, social media platforms or email, against a woman because she is a woman, or affects women disproportionately. **([A/HRC/38/47, par. 23](https://digitallibrary.un.org/record/1641160/files/A_HRC_38_47-ES.pdf))**

### 2017: Digital gender-based violence in contexts of the digital divide

-  **The UN High Commissioner delves into digital gender violence in contexts of the digital divide. And it is the first report to make the first approximations of the definition of digital violence:**
“Online violence against women encompasses acts of gender-based violence that are committed, facilitated or aggravated by the use of ICTs, including online threats and harassment and gross and demeaning breaches of privacy, such as “revenge pornography”. **([UN, 2017](https://documents-dds-ny.un.org/doc/UNDOC/LTD/G15/140/16/PDF/G1514016.pdf?OpenElement))**

-  **CEDAW General Recommendation No. 35.**
The Committee on the Elimination of Discrimination against Women (CEDAW) issued General Recommendation No. 35 on gender-based violence against women, which updates General Recommendation No. 19. This document acknowledges gender-based violence as a form of historically unequal power relationship between women and men and reaffirms women’s right to live free from violence and discrimination. It also emphasizes the need to eliminate all forms of discrimination against women, including those occurring in digital environments. **([CEDAW, 2017](https://tbinternet.ohchr.org/_layouts/15/treatybodyexternal/Download.aspx?symbolno=CEDAW/C/GC/35&Lang=en))**

### 2015: Human rights council resolution

-  The UN Human Rights Council adopted a resolution to accelerate efforts to eliminate all forms of violence against women, noting that this violence includes acts such as cyberbullying and cyberstalking. **([UN, 2015](https://documents-dds-ny.un.org/doc/UNDOC/LTD/G15/140/16/PDF/G1514016.pdf?OpenElement))**

### 2013: Resolutions and recommendations on technology and gender

-  **Freedom of opinion and expression:**
The UN approved a resolution regarding the role of freedom of opinion and expression in women’s empowerment and urged States to implement gender-sensitive technology. **([UN, 2013](https://documents-dds-ny.un.org/doc/UNDOC/GEN/G13/150/80/PDF/G1315080.pdf?OpenElement))**

-  **Women's participation:**
The UN Council published the “Report of the Working Group on the issue of discrimination against women in law and in practice”. This document addressed the importance of inclusion for women's participation on the Internet and the risks of violence they face online. **([UN, 2013](https://undocs.org/es/A/HRC/23/50))**

-  **Protecting women human rights defenders:**
In the same year, the General Assembly approved a resolution for the protection of women human rights defenders and women's rights defenders.This document acknowledges the violence women face on the Internet and its impact on the full exercise of their rights. **([UN, 2013](https://www.un.org/en/ga/search/view_doc.asp?symbol=A/RES/68/181&Lang=S))**

### 2012: Human Rights on the Internet

-  The UN Human Rights Council approved a resolution on the rights of individuals on the Internet. It stated that human rights must be protected in all environments: both online and offline. **([UN, 2012](https://undocs.org/es/A/HRC/RES/20/8))**

### 2011: Human Rights on the Internet

-  The report of the UN Special Rapporteur on Freedom of Opinion and Expression highlighted the unique and transformative nature of the Internet. It not only enables individuals to exercise their right to freedom of opinion and expression, but also a range of other human rights, promoting the progress of society as a whole. **([UN, 2011](https://www.un.org/ga/search/view_doc.asp?symbol=A/HRC/17/27&Lang=S))**

### 2006: The first mention of technology-facilitated gender-based violence

-  The first time it was stated in international instruments that the use of ICTs also propagates structural problems of gender-based violence was in 2006, in a UN report: “In-depth study on all forms of violence against women”. **([UN, 2006](https://undocs.org/es/A/61/122/Add.1))**

### 1994: Convention of Belém do Pará

-  This convention was ratified by Paraguay through Law 605/1995. It was the first international treaty on violence against women that affirms that this violence constitutes a violation of human rights. The Convention establishes for the first time the development of mechanisms for the protection and defense of women's human rights, in the fight to eliminate violence against their physical, sexual and psychological integrity, both in the public and private spheres. It also provides recommendations for measures and public policies that seek to eradicate violence against women. **([OAS, 1994](http://www.oas.org/juridico/spanish/tratados/a-61.html))**

### 1970: Convention on the Elimination of All Forms of Discrimination against Women (CEDAW)

-  It establishes state responsibility in eliminating violence against women. This convention was approved in 1979 and ratified by Paraguay through Law No. 1215/8. It highlights violence as a form of historically unequal power relationship between women and men, emphasizing that women have the right to live free from violence and discrimination. In this regard, General Recommendation 19/1992 stands out, as it was the first to make the States responsible for situations of violence against women and oblige them to adopt the necessary measures for its elimination. **([UN, 1979](https://www.ohchr.org/es/instruments-mechanisms/instruments/convention-elimination-all-forms-discrimination-against-women))**

      `,

    // es
    'title_es': "¿QUÉ ES LA VIOLENCIA DE GÉNERO FACILITADA POR LA TECNOLOGÍA?",
    'intro_es': `
La violencia de género facilitada por la tecnología es todo acto cometido, asistido, agravado o amplificado por el uso de tecnologías de la información y la comunicación u otras herramientas digitales que tenga o pueda tener como resultado un daño físico, sexual, psicológico, social, político o económico u otras vulneraciones de los derechos y libertades **([ONU, 2023](https://unric.org/es/violencia-de-genero-facilitada-por-la-tecnologia/))**.

La violencia de género facilitada por la tecnología, o en línea, refiere a actos de violencia de género cometidos instigados o agravados, en parte o totalmente, por el uso de las Tecnologías de la Información y la Comunicación (TIC) plataformas de redes sociales y correo electrónico **([APC 2015](https://www.genderit.org/articles/impunity-justice-exploring-corporate-and-legal-remedies-technology-related-violence-against))**.

Aún no existe un consenso global sobre la definición de violencia digital de género. Sin embargo se entiende cuando se comete y se expande a través de medios digitales, contra una mujer –por ser mujer–, o cuando afecta a las mujeres de manera desproporcionada **([UN Women, 2020](https://asiapacific.unwomen.org/-/media/field%20office%20eseasia/docs/publications/2020/10/ap-wps-brief-covid-19-and-online-misogyny-hate-speech_final.pdf?la=en&vs=2206))**.

Esta violencia causa daño psicológico y emocional, refuerza prejuicios, daña la reputación, causa pérdidas económicas y plantea barreras a la participación en la vida pública. Además puede conducir a formas de violencia sexual y otras formas de violencia física **([Luchadoras 2017](https://luchadoras.mx/wp-content/uploads/2024/10/Informe_ViolenciaEnLineaMexico_InternetEsNuestra.pdf)).**

    `,
    'content_es': `

![Violencia como monstruo que ataca mujeres {image-right}](/blog-files/paginas/blog-00.png)

“La violencia de género facilitada por la tecnología es todo acto cometido, asistido, agravado o amplificado por el uso de tecnologías de la información y la comunicación u otras herramientas digitales que tenga o pueda tener como resultado un daño físico, sexual, psicológico, social, político o económico u otras vulneraciones de los derechos y libertades” **([ONU, 2023)](https://unric.org/es/violencia-de-genero-facilitada-por-la-tecnologia/)**.

"La violencia de género facilitada por la tecnología, o en línea, refiere a actos de violencia de género cometidos instigados o agravados, en parte o totalmente, por el uso de las Tecnologías de la Información y la Comunicación (TIC) plataformas de redes sociales y correo electrónico" **([APC 2015](https://www.genderit.org/articles/impunity-justice-exploring-corporate-and-legal-remedies-technology-related-violence-against)).**

"Aún no existe un consenso global sobre la definición de violencia digital de género. Sin embargo se entiende cuando se comete y se expande a través de medios digitales, contra una mujer –por ser mujer–, o cuando afecta a las mujeres de manera desproporcionada" **([UN Women, 2020](https://www.unwomen.org/-/media/headquarters/attachments/sections/library/publications/2020/brief-online-and-ict-facilitated-violence-against-women-and-girls-during-covid-19-en.pdf?la=en&vs=2519)).**

"Esta violencia causa daño psicológico y emocional, refuerza prejuicios, daña la reputación, causa pérdidas económicas y plantea barreras a la participación en la vida pública. Además puede conducir a formas de violencia sexual y otras formas de violencia física" **([Luchadoras 2017](https://luchadoras.mx/wp-content/uploads/2017/12/Informe_ViolenciaEnLineaMexico_InternetEsNuestra.pdf)).**

## Antecedentes y consecuencias

### 2022: Acuerdo sobre reconocimiento de medidas de protección para mujeres en situación de violencia de género

- Durante la Presidencia Pro-Tempore Argentina, el Mercosur emitió el "Reconocimiento regional mutuo de medidas de protección para mujeres en situación de violencia basada en género". Este acuerdo extiende la protección de las mujeres víctimas de violencia de género entre los países del Mercosur y Estados asociados **([Mercosur, 2022](https://normas.mercosur.int/simfiles/normativas/73855_REC_004-2019_ES_Reconocimiento%20mutuo%20medidas%20proteccion.pdf)).**

### 2021: Impacto desproporcionado de la violencia facilitada por la tecnología en la libertad de expresión

- La Relatora Especial de la ONU, Irene Khan, en su informe a la Asamblea General destacó el impacto desproporcionado de la violencia de género digital en la libertad de opinión y expresión. Realizó recomendaciones para crear un entorno digital seguro **([ONU, 2021](https://undocs.org/es/A/76/258))**.

### 2020: La erradicación de la violencia de género facilitada por la tecnología contra las periodistas

- Dubravka Šimonović, Relatora Especial sobre la violencia contra la mujer, expuso los riesgos específicos que enfrentan las periodistas debido a la violencia de género facilitada por la tecnología **([ONU, 2020](https://undocs.org/es/A/HRC/44/52))**.

### 2018: Definición de violencia digital de género

- En su informe, la Relatora Especial de la ONU sobre violencia contra las mujeres, Dubravka Šimonović, definió la violencia en línea contra las mujeres como todo acto de violencia por razón de género contra las mujeres cometido, con la asistencia, en parte o en su totalidad, del uso de las tecnologías de la información y las comunicaciones (TIC), o agravado por este, como los teléfonos móviles y los teléfonos inteligentes, Internet, plataformas de medios sociales o correo electrónico, dirigida contra una mujer porque es mujer o que la afecta en forma desproporcionada **([A/HRC/38/47, párr. 23](https://digitallibrary.un.org/record/1641160/files/A_HRC_38_47-ES.pdf))**.

### 2017: Violencia digital de género en contextos de brecha digital

- **El Alto Comisionado de la ONU profundiza la violencia digital de género en contextos de brecha digital. Y es el primer informe que realiza las primeras aproximaciones de la definición de violencia digital:** "La violencia contra la mujer en línea abarca los actos de violencia por razón de género que se cometen, facilitan o agravan por el uso de las TIC, incluidas las amenazas y el acoso en Internet y las violaciones manifiestas y degradantes de la intimidad en línea, como la "pornografía de venganza"" **([ONU, 2017](https://documents-dds-ny.un.org/doc/UNDOC/LTD/G15/140/16/PDF/G1514016.pdf?OpenElement))**.

- **Recomendación general N.º 35 de CEDAW:**
El Comité para la Eliminación de la Discriminación contra la Mujer (CEDAW) publicó la Recomendación General N.º 35 sobre la violencia por razón de género contra las mujeres, que actualiza la Recomendación General N.º 19. Este documento destaca que la violencia de género es una manifestación de relaciones de poder históricamente desiguales entre mujeres y hombres, reafirmando el derecho de las mujeres a vivir libres de violencia y discriminación. Asimismo, subraya la necesidad de eliminar todas las formas de discriminación contra las mujeres, incluyendo aquellas que ocurren en entornos digitales **([CEDAW, 2017](https://tbinternet.ohchr.org/_layouts/15/treatybodyexternal/Download.aspx?symbolno=CEDAW/C/GC/35&Lang=en))**.

### 2015: Resolución del consejo de derechos humanos

- El Consejo de derechos humanos de la ONU aprueba la resolución para acelerar los esfuerzos para eliminar todas las formas de violencia contra las mujeres, mencionando que esta violencia puede incluir actos como el ciberacoso **([ONU, 2015](https://documents-dds-ny.un.org/doc/UNDOC/LTD/G15/140/16/PDF/G1514016.pdf?OpenElement))**.

### 2013: Resoluciones y recomendaciones sobre tecnología y género

- **Libertad de opinión y expresión:**
La ONU aprobó una resolución respecto a la contribución de la libertad de opinión y de expresión al empoderamiento de la mujer. Y sugiere a los Estados a implementar la tecnología con perspectiva de género **([ONU, 2013](https://documents-dds-ny.un.org/doc/UNDOC/GEN/G13/150/80/PDF/G1315080.pdf?OpenElement))**.

- **Participación de las mujeres:**
El Consejo de la ONU publica el "Informe de Grupo de Trabajo sobre la cuestión de la discriminación contra la mujer en la legislación y en la práctica. En este documento se desarrollaron la importancia de la inclusión para la participación de las mujeres en Internet y el riesgo de violencia que corren las mujeres en Internet **([ONU, 2013](https://undocs.org/es/A/HRC/23/50))**.

- **Protección a defensoras de derechos humanos:**
En este mismo año se encuentra la resolución que aprobó la Asamblea General para la protección de las defensoras de los derechos humanos y de la mujer. En este documento se reconoce la violencia que las mujeres reciben en internet y cómo afecta el ejercicio pleno de sus derechos **([ONU, 2013](https://www.un.org/en/ga/search/view_doc.asp?symbol=A/RES/68/181&Lang=S))**.

### 2012: Derechos humanos en Internet

- El Consejo de los Derechos Humanos de la ONU aprobó un la resolución sobre los derechos de las personas en Internet. Que establecía que los derechos humanos deben protegerse en todos los espacios: online y offline **([ONU, 2012](https://undocs.org/es/A/HRC/RES/20/8))**.

### 2011: Derechos humanos en Internet

- El informe de la Relatoría Especial de la ONU sobre la Libertad de Expresión destacó el carácter único y transformador de Internet. No solo permite a las personas ejercer su derecho a la libertad de opinión y expresión, sino que también facilita el ejercicio de otros derechos humanos y promueve el progreso de la sociedad en su conjunto **([ONU, 2011](https://www.un.org/ga/search/view_doc.asp?symbol=A/HRC/17/27&Lang=S))**.

### 2006: Primera mención de violencia de género facilitada por la tecnología

- La primera vez que en instrumentos internacionales se afirmó que el uso de las TICs también reproducen problemas estructurales de violencia de género fue en el 2006 en el informe de la ONU "Estudio a fondo sobre todas las formas de violencia contra la mujer" **([ONU, 2006](https://undocs.org/es/A/61/122/Add.1))**.

### 1994: Convención de Belém do Pará

- Esta convención fue ratificada por Paraguay a través de la Ley 605/1995. Fue el primer tratado internacional sobre violencia contra la mujer que afirma que esta violencia constituye una violación de los derechos humanos. La Convención establece por primera vez el desarrollo de mecanismos de protección y defensa de los derechos humanos de las mujeres, en la lucha para eliminar la violencia contra su integridad física, sexual y psicológica, tanto en el ámbito público como en el privado. Además establece recomendaciones sobre la generación de medidas y políticas públicas que busquen erradicar la violencia contra las mujeres **([OEA, 1994](http://www.oas.org/juridico/spanish/tratados/a-61.html))**.

### 1970: Convención sobre la eliminación de todas las formas de discriminación contra la mujer (CEDAW)

- Establece la responsabilidad estatal en la eliminación de la violencia contra las mujeres. Esta convención aprobada en 1979 y fue ratificada por Paraguay a través de la ley N.º 1215/8 pone de manifiesto la violencia como una forma de relación de poder históricamente desigual entre mujeres y hombres, remarcando que las mujeres tienen el derecho a vivir libres de violencia y discriminación. En ese sentido se resalta la Recomendación General 19/1992 por la cual responsabiliza por primera vez a los Estados sobre las situaciones de violencia hacia las mujeres y obliga a los mismos adoptar medidas necesarias para su eliminación **([ONU, 1979](https://www.ohchr.org/es/instruments-mechanisms/instruments/convention-elimination-all-forms-discrimination-against-women))**.

      `,
  };
