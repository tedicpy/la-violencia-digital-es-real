export default {
    // en
    'nombre_en': "Nadine Portillo",
    'bio_en': `
First prosecutor in the case “Cristian Kriskovich on Sexual Harassment”. Based on certain actions, Belén and her lawyer Cuevas requested the recusal of Prosecutor Portillo and filed a complaint against her before the Attorney General's Office.

At Belén’s hearing, Prosecutor Portillo denied Belén's lawyer Rodrigo Cuevas from entering the proceedings, while granting entry to defense attorney Alfredo Montanaro (Mr. Kriskovich's lawyer). During the proceedings, Belén was harassed and re-victimized by Montanaro, to the point of being psychologically affected by it and having to request a new date to continue with the procedure. After this, Belén and her lawyer repeatedly requested a forensic analysis to be carried out on the cellphones of both her and Mr. Kriskovich, in order to extract the information necessary to verify the sexual harassment she reported. Despite the requests and having submitted simple copies of some of the messages and images sent to her by Mr. Kriskovich, prosecutor Portillo refused to have his cellphone checked.
    `,
    'cargo_en': 'Prosecutor',

    // es
    'nombre_es': "Nadine Portillo",
    'bio_es': `
Es la primera fiscala en el caso “Cristian Kriskovich s/ Acoso sexual” En base a ciertas actuaciones, Belén y el abogado Cuevas solicitaron la recusación de la Fiscal Portillo y formularon una denuncia en su contra ante la Fiscalía General.

En la Audiencia Testifical de Belén, la Fiscal Portillo negó el ingreso a la diligencia al abogado de Belén Rodrigo Cuevas permitiendo el ingreso al abogado Alfredo Montanaro (Abogado del Sr. Kriskovich) Durante la diligencia Belén fue hostigada y revictimizada por el abogado Montanaro al punto de que se vio afectada y tuvo que solicitar una nueva fecha para continuar con el procedimiento. Posterior a esto, Belén y su abogado solicitaron en reiteradas ocasiones la realización de un peritaje a los celulares tanto de ella como del Sr. Kriskovich, con el fin de extraer la información necesaria para constatar el acoso sexual que denunció. A pesar de los pedidos y de haber presentado copias simples de algunos de los mensajes e imágenes que le había enviado el Sr. Kriskovich, la Fiscal Portillo negó la realización de la pericia del celular de este.
    `,
    'cargo_es': 'Fiscala',

    'documentoEnlace': [
        'https://violenciadigital.tedic.org/docs/2014_11_24-II_Audiencia_Testifical_Bel%C3%A9n.pdf',
        'https://violenciadigital.tedic.org/docs/2015_03_19_Resoluci%C3%B3n_que_Aprueba_Peritaje.pdf'
    ]
};
