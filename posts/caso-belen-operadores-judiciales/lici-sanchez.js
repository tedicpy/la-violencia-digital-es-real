export default {
    // en
    'nombre_en': "Lici Sánchez",
    'bio_en': `
Judge who decreed Belén's defiance without any legal substantiation, and canceled her defense

On June 21, 2019, through A.I. No 625, she issued  decreed Belén's defiance, ordered her capture, canceled her defense and ordered a ban on her lawyers’ access to the computer system that allows access to the dossier . This ruling was never notified and this, added to the cancellation of the defense, prevented the use of legal tools. The decree of defiance was issued without complying with any of the specific grounds that the legislation provides for this figure, as there was no failure to appear on Belén's part (only invocable cause given that other causes refer to people with previous measures of detention or apprehension). Belén's lawyers filed a complaint against the judge addressed to the Superintendency Council of the Supreme Court of Justice, which led to a Management Audit being carried out on the judicial file of the case and resulted in a report dated September 13, 2019. The report concluded that the decision that led to revoke Belén's legal representation and deny them access to the electronic file was illegitimate and constituted an impediment to the exercise of the procedural powers of the defense. Consequently, the report recommended the referral of the records to the General Superintendency of Justice and to the Jury for the Prosecution of Magistrates, regarding the actions of judge Lici Teresita Sánchez, and to the General Superintendency of Justice regarding the actions of judicial clerk Yanina Vallejos. Still, none of this has been processed.
    `,
    'cargo_en': 'Judge',

    // es
    'nombre_es': "Lici Sánchez",
    'bio_es': `
Jueza que decretó la rebeldía de Belén sin fundamentos legales y canceló su defensa

El 21 de junio del 2019 mediante A.I N°625 declaró la rebeldía de Belén, ordenó su captura, canceló su defensa y dispuso la desvinculación de sus abogados del sistema informático de acceso al expediente. Esta providencia nunca fue notificada y ello, sumado a la cancelación de la defensa, impidió que se interpusieran los recursos de ley. La declaración de rebeldía fue dictada sin cumplirse ninguna de las causales taxativas que prevé la legislación para esta figura, en tanto no existió incomparencencia de Belén; única causal invocable dado que las demás refieren a personas con medidas de detención o aprehensión. Como consecuencia de la declaración de rebeldía la jueza Sánchez ordenó la captura de Belén. Los abogados de Belén interpusieron una denuncia contra la jueza a instancias del Consejo de Superintendencia de la Corte Suprema de Justicia, lo que llevó a que se hiciera una Auditoría de Gestión al expediente judicial de la causa y que finalizó con un Informe de fecha 13 de septiembre del 2019. El informe concluyó que la decisión que condujo a revocar la representación legal de Belén y negarle acceso al expediente electrónico era ilegítima y constituyó un impedimento para el ejercicio de las facultades procesales de la defensa. En consecuencia, recomendó la remisión de los antecedentes a la Superintendencia General de Justicia y al Jurado de Enjuiciamiento de Magistrados con relación a las actuaciones de la magistrada Lici Teresita Sánchez y a la Superintendencia General de Justicia con relación a la actuaria judicial Yanina Vallejos. Hasta el momento no se ha dado trámite.
    `,
    'cargo_es': 'Jueza',

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Compra_Notas/2019_06_21_Auto_Rebeld%C3%ADa.pdf'
};
