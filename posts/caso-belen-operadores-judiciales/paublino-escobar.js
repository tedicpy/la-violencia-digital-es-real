export default {
    // en
    'nombre_en': "Paublino Escobar",
    'bio_en': `
Belén’s defense made a recusation request alleging partiality since the judge was also a professor at the Catholic University, an institution that was part of the complaint adhered to the case. He was later reported by Belén’s defense for poor performance of his duties, but the complaint was rejected by the Council of the Judiciary, presided at that time by Mr. Kriskovich.

Escobar sanctioned lawyers Rodrigo Cuevas and Graciela Moreno for "delaying action", for having submitted disqualification requests in the case “production of non-authentic documents”, and fined them for 7,850,400 guaraníes (approximately 1,120 USD).
    `,
    'cargo_en': 'Deputy Defense Attorney',

    // es
    'nombre_es': "Paublino Escobar",
    'bio_es': `
La defensa de Belén realizó un pedido de recusación alegando parcialidad en tanto el Juez era a su vez docente de la Universidad Católica, institución que conformaba la querella adhesiva de la causa. Posteriormente fue denunciado por mal desempeño de sus funciones por la defensa de Belén, pero fue rechazado por el Consejo de la Magistratura del cual en ese momento el Sr. Kriskovich era presidente.

Sanciona a los abogados Rodrigo Cuevas y Graciela Moreno por ´actitud dilatoria´ tras haber presentado pedidos de recusación, en la causa de ´producción de documento no auténtico´. Lo multa por el equivalente a 7.850.400gs.
    `,
    'cargo_es': 'Defensor adjunto',

    'enlaceExterno': 'http://www.ucguaira.edu.py/acto-graduacion-juridicas/'
};
