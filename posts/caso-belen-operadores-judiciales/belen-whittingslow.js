export default {
  // en
  'nombre_en': "Belén Whittingslow",
  'bio_en': `
Belén was a student at the Catholic University of Asunción when she alleges that she was sexually harassed by Mr. Kriskovich. She is currently applying for refugee status in Uruguay.    
  `,
  'cargo_en': 'Victim',

  // es
  'nombre_es': "Belén Whittingslow",
  'bio_es': `
Belén era alumna de la Universidad Católica cuando alega haber sido acosada sexualmente por el Sr. Kriskovich. Actualmente se encuentra siendo solicitante de refugio en Uruguay.
  `,
  'cargo_es': 'Víctima'
};
