export default {
    // en
    'nombre_en': "Martin Barba",
    'bio_en': `
Belén's current lawyer
    `,
    'cargo_en': "Belén's Lawyer",

    // es
    'nombre_es': "Martin Barba",
    'bio_es': `
Actual abogado de Belén.
    `,
    'cargo_es': 'Abogado de Belén',

    'enlaceExterno': 'https://www.rdn.com.py/2019/07/02/caso-kriskovich-abogado-denuncia-que-jueces-apanan-a-acusado/'
};
