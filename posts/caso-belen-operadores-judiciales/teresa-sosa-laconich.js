export default {
    // en
    'nombre_en': "Teresa Sosa Laconich",
    'bio_en': `
Prosecutor who intervenes in the case “Production of Non-Authentic Document”. Ratifies the accusation against Belén.

Following the sentence by Judge Lici Sanchez, prosecutor Sosa declared in the media that an international arrest warrant had been issued. This information was later rebutted. Moreover, the prosecutor declared to the media that the events denounced by Belén against Mr. Kriskovich did not constitute harassment, despite the fact that he was not involved in the harassment case.
    `,
    'cargo_en': 'Prosecutor',
  
    // es
    'nombre_es': "Teresa Sosa Laconich",
    'bio_es': `
Fiscala que interviene en el caso de "producción de documento no auténtico" ratifica la acusación contra Belén.

Posterior a la sentencia de la Jueza Lici Sanchez, la Fiscala Sosa declaró en medios de comunicación que se había dictado una orden de captura internacional. Dicha información fue desmentida posteriormente. A su vez, la Fiscala declaró ante medios de comunicación que los hechos denunciados de Belén contra el Sr. Kriskovich no configuraban acoso a pesar de no tener intervención en la causa de acoso.
    `,
    'cargo_es': 'Fiscala',

    'enlaceExterno': [
        'https://www.ultimahora.com/fiscala-pide-orden-captura-refugiada-uruguay-n2837643.html',
        'https://www.lanacion.com.py/judiciales/2019/08/13/captura-internacional-para-joven-que-denuncio-acoso-de-kriskovich',
        'https://www.hoy.com.py/nacionales/en-caso-kriskovich-no-hubo-acoso-mensajes-fueron-entre-mujer-hombre-no-profesor-alumna-afirma-fiscala'
    ]
};