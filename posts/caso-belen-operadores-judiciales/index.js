export default [
  {
    'key': "belen-whittingslow",
    'imageFile': "04-belen.png",
    'cargoClass': "bg-brand-green-4"
  },
  {
    'key': "cristian-kriskovich",
    'imageFile': "05-kriskovich.png",
    'cargoClass': "bg-brand-red-3"
  },
  {
    'key': "nadine-portillo",
    'imageFile': "06-nadine.png",
    'cargoClass': "bg-brand-indigo-5"
  },
  {
    'key': "rodolfo-fabian-centurion",
    'imageFile': "07-rodolfo.png",
    'cargoClass': "bg-brand-indigo-5"
  },
  {
    'key': "jorge-sosa",
    'imageFile': "08-jorge.png",
    'cargoClass': "bg-brand-indigo-5"
  },
  {
    'key': "claudia-morys",
    'imageFile': "09-claudia.png",
    'cargoClass': "bg-brand-indigo-5"
  },
  {
    'key': "teresa-sosa-laconich",
    'imageFile': "10-teresa.png",
    'cargoClass': "bg-brand-indigo-5"
  },
  {
    'key': "julian-lopez",
    'imageFile': "11-julian.png",
    'cargoClass': "bg-brand-pink-5"
  },
  {
    'key': "paublino-escobar",
    'imageFile': "12-paublino.png",
    'cargoClass': "bg-brand-pink-5"
  },
  {
    'key': "alberto-martinez-simon",
    'imageFile': "13-alberto.png",
    'cargoClass': "bg-brand-pink-5"
  },
  {
    'key': "carlos-escobar",
    'imageFile': "16-carlos.png",
    'cargoClass': "bg-brand-pink-5"
  },
  {
    'key': "linneo-ynsfran",
    'imageFile': "15-linneo.png",
    'cargoClass': "bg-brand-pink-5"
  },
  {
    'key': "lici-sanchez",
    'imageFile': "14-lici.png",
    'cargoClass': "bg-brand-pink-5"
  },
  {
    'key': "vivian-lopez",
    'imageFile': "17-vivian.png",
    'cargoClass': "bg-brand-pink-5"
  },
  {
    'key': "rodrigo-cuevas",
    'cargoClass': "bg-brand-green-4"
  },
  {
    'key': "martin-barba",
    'cargoClass': "bg-brand-green-4"
  },
  {
    'key': "alfredo-montanaro",
    'cargoClass': "bg-brand-red-3"
  },
  {
    'key': "adrian-salas",
    'cargoClass': ""
  },
  {
    'key': "enrique-riera",
    'cargoClass': "bg-brand-red-4"
  },
];