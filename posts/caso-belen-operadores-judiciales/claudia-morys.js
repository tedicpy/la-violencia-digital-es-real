export default {
    // en
    'nombre_en': "Claudia Morys",
    'bio_en': `
Files a complaint against Belén in the case “Victor Oviedo et al. on Production of Non-Authentic Document” on June 11, 2015.

The prosecutor bases the accusation against Belén on an expert report made on the basis of a DVD provided by the Catholic University of Asunción, which was part of the Final Audit Report carried out by the Audit Commission of the Faculty of Legal and Diplomatic Sciences. The objective was to review the minutes of Belén’s exams involved in the process, to determine if they had been modified. However, the expert report did not provide any evidentiary conclusion on this. On the contrary, it concluded that “it is not possible to determine user, date, time, load and modifications of the object of study ‘Web Server Minutes’ because it was carried out using a single generic user that is shared by the employees of the Secretariat who scanned and uploaded the transcripts to the web server. Furthermore, the web system did not record these operations in an audit log. These verifications must be carried out in the academic management system "AS400", where each employee logs into the system with a user account and password and loads/modifies the results data according to subject and student ID number”.
    `,
    'cargo_en': 'Prosecutor',

    // es
    'nombre_es': "Claudia Morys",
    'bio_es': `
Formula imputación contra Belén en la causa ´Victor Oviedo y otros s/ producción de documento no auténtico y otro´ en fecha 11 de junio del 2015.

La Fiscal basa la la imputación de Belén en un informe pericial realizado con base a un DVD proveído por la UCA, que hacía parte del Informe de la Auditoría Final realizado por la Comisión Interventora de la FCJD. El objetivo era revisar las actas de los exámenes de Belén implicados en el proceso para determinar si fueron modificadas. Sin embargo, el peritaje no tuvo conclusión probatoria alguna sobre esto. De hecho, concluyó: “No es posible determinar del objeto de estudio “Actas del Servidor Web” el usuario, fecha, hora, carga y modificaciones porque el mismo se realizaba con un único usuario genérico que es compartido por los empleados de la Secretaría que escaneaba y cargaban al servidor web las actas de calificaciones, además el sistema web no registraba estas operaciones en un archivo de auditoría. Se debe realizar estas verificaciones en el sistema de gestión académica “AS400” donde cada empleado ingresa al sistema con una cuenta de usuario y contraseña donde (cargan, modifican) los datos de las calificaciones por materia y matrícula del alumno´
    `,
    'cargo_es': 'Fiscala',

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Compra_Notas/2015_11_30_Informe_Pericial.pdf'
};
