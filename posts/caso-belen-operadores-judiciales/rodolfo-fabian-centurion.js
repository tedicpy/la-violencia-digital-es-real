export default {
    // en
    'nombre_en': "Rodolfo Fabián Centurión",
    'bio_en': `
Assumes as prosecutor in the case on sexual harassment following the complaints made against Liz Nadine Portillo.

On April 17, 2015, he decided to allow the report requested by defense attorney Cuevas, and set April 27, 2015 as the deadline for the delivery of Belén’s cellphone. On April 21, 2015, Mr. Kriskovich provided a public deed where he acknowledged the exchange of messages with Belén, but questioned their scope. Immediately afterwards, on April 23, 2015, in a single resolution, prosecutor Centurión decided to call off the proceeding and dismiss the case, citing as justification the argumentation presented by defense attorney Montanaro in the brief accompanying the public deed.
    `,
    'cargo_en': 'Prosecutor',

    // es
    'nombre_es': "Rodolfo Fabián Centurión",
    'bio_es': `
Asume como fiscal de la causa de acoso sexual tras las denuncias formuladas contra Liz Nadine Portillo.

El 17 de abril del 2015 dipuso la realización de la pericia solicitada por el abogado Cuevas, y fijó como fecha para la diligencia de entrega del celular de Belén el 27 de abril del 2015. El 21 de abril del 2015 el Sr. Kriskovich aportó una escritura pública donde reconoció el intercambio de mensajes con Belén, pero cuestionó el alcance de los mismos. Acto seguido, el 23 de abril del 2015, en una sola resolución el Fiscal Centurión decidió desconvocar a las diligencias de pericia y desestimar la causa, usando como justificación la argumentación expuesta por el abogado Montanaro en el escrito con el que acompañó la escritura pública.
    `,
    'cargo_es': 'Fiscal',

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_04_23_Resoluci%C3%B3n_Deja_sin_Efecto_Pericia.pdf'
};
