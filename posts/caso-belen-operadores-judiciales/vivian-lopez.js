export default {
    // en
    'nombre_en': "Vivian López",
    'bio_en': `
Judge who dismissed the evidence offered by Belén's defense regarding the examination of her cellphone.

After the presentation by Belén's lawyers of the brief where the report of the cellphones is offered as evidence in order to verify the alleged sexual harassment, judge López resolves through A.I. No. 239 to reject the points N. 1 to 60 (cellphone report), and only admits the report related to publications or comments on the social media network Facebook. The judge argues that the evidence offered has no relation with the disputed facts, declaring them ‘superabundant’ since they have been previously treated in a criminal court.
    `,
    'cargo_en': 'Judge',

    // es
    'nombre_es': "Vivian López",
    'bio_es': `
Jueza que niega la prueba ofrecida por la defensa de Belén referente a la pericia de su celular.

Tras la presentación de la defensa de Belén del escrito donde se ofrece como prueba la pericia de los celulares a fin de constatarse el acoso alegado, la Jueza López resuelve mediante A.I. Nro. 239 excluir los puntos de la pericia 1 al número 60 de los propuestos por la demandada (pericia del celular), admite solamente la pericia en relación a publicaciones o comentarios en la red social facebook. La Jueza argumenta que la prueba ofrecida no está relacionada con los hechos controvertidos; declarándolas sobreabundantes al haber sido tratadas previamente en sede penal.
    `,
    'cargo_es': 'Jueza de primera instancia',

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/Compra_Notas/2019_06_21_Auto_Rebeld%C3%ADa.pdf'
};
