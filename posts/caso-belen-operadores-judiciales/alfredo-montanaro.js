export default {
    // en
    'nombre_en': "Alfredo Montanaro",
    'bio_en': `
Belén alleges having been harassed by Defense Attorney Montanaro.
    `,
    'cargo_en': "Kriskovich's Lawyer",

    // es
    'nombre_es': "Alfredo Montanaro",
    'bio_es': `
Belén alega haber sido hostigada por el Abogado Montanaro.
    `,
    'cargo_es': 'Abogado de Kriskovich',

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2014_11_26_Reiteraci%C3%B3n_de_Denuncia_y_Aporte_de_Documento.pdf'
};
