export default {
    // en
    'nombre_en': "Julian López",
    'bio_en': `
    Confirms the request for case dismissal.
    
    Sustained the request for initial dismissal through a decision on June 8, 2015.
    `,
    'cargo_en': 'Judge',

    // es
    'nombre_es': "Julian López",
    'bio_es': `
Confirma el pedido de desestimación de la causa

Hace lugar al pedido de desestimación inicial mediante proveído del 08 de junio del 2015.
    `,
    'cargo_es': 'Juez',

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2015_06_08_Auto_Hace_Lugar_a_Desestimaci%C3%B3n.pdf'
};
