export default {
    // en
    'nombre_en': "Adrián Salas",
    'bio_en': `
Member of the Council of the Magistracy.

Kriskovich's witness. He testified via written testimony (questions were sent to him to answer in writing)
    `,
    'cargo_en': 'Member of the Council of the Judiciary',

    // es
    'nombre_es': "Adrián Salas",
    'bio_es': `
Testigo de Kriskovich. Testificó vía oficio (se le remitieron las preguntas para que conteste en forma escrita)
    `,
    'cargo_es': 'Miembro del Consejo de la Magistratura',

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2014_11_26_Reiteraci%C3%B3n_de_Denuncia_y_Aporte_de_Documento.pdf'
};
