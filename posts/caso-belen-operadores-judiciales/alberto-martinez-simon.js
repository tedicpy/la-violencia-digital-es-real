export default {
    // en
    'nombre_en': "Alberto Martínez Simón",
    'bio_en': `
Judge at the Court that resolves the appeal of the first instance resolution denying the examination of the mobile phones involved. At the time of resolving the appeal, he was running for Justice of the Supreme Court. On December 20, 2018, Belén’s defense attorneys presented a brief addressed to the Council of the Judiciary where they filed a complaint against Mr. Kriskovich, Alberto Martínez Simón and Linneo Ynsfrán alleging serious irregularities.

Member of the sixth district Court of Appeals who resolves through A.I. No. 531, dated December 19, 2018, to sustain the first instance resolution denying the mobile phones expertise, considering it “superfluous and overabundant.”
    `,
    'cargo_en': 'Court Minister',

    // es
    'nombre_es': "Alberto Martínez Simón",
    'bio_es': `
Juez integrante del Tribunal que resuelve la apelación de la resolución de primera instancia que niega la pericia de los teléfonos celulares. Al momento de resolver la apelación se encontraba en terna para Ministro de la Corte Suprema. En fecha 20 de diciembre del 2018 los abogados defensores de Belén presentan una nota dirigida al consejo de la Magistratura donde formulan denuncia contra el Sr. Kriskovich, Alberto Martínez Simón y Linneo Ynsfrán alegando graves irregularidades.

Integra el Tribunal de Apelaciones de la sexta sala que resuelve mediante A.I. Nro. 531 de fecha 19 de diciembre del 2018 confirmar la resolución de primera instancia que niega la pericia de los teléfonos celulares por considerarla ´superflua y sobreabundante´ en atención a haber sido debatida en sede penal.
    `,
    'cargo_es': 'Ministro de la corte',

    'enlaceExterno': 'https://www.rdn.com.py/2019/07/02/caso-kriskovich-abogado-denuncia-que-jueces-apanan-a-acusado/'
};
