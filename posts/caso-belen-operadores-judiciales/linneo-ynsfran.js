export default {
    // en
    'nombre_en': "Linneo Ynsfrán",
    'bio_en': `
Incorporated as a member of the sixth district Court of Appeals in the case of compensation for damages against Mr. Kriskovich.
    `,
    'cargo_en': 'Attorney',

    // es
    'nombre_es': "Linneo Ynsfrán",
    'bio_es': `
Entra como parte del tribunal de apelación de la sexta sala en el caso de daños y perjuicios
    `,
    'cargo_es': 'Abogado',

    'enlaceExterno': 'https://www.rdn.com.py/2019/07/02/caso-kriskovich-abogado-denuncia-que-jueces-apanan-a-acusado/'
};
