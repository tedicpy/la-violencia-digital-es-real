export default {
    // en
    'nombre_en': "Enrique Riera",
    'bio_en': `
At the time of giving testimony, he was serving as Minister of Education.

Kriskovich's witness. He testified via written testimony (questions were sent to him to answer in writing)
    `,
    'cargo_en': 'Senator',

    // es
    'nombre_es': "Enrique Riera",
    'bio_es': `
En el momento de prestar declaración testifical se desempeñaba como Ministro de Educación.

Testigo de Kriskovich. Testificó vía oficio (se le remitieron las preguntas para que conteste en forma escrita)
    `,
    'cargo_es': 'Senador',

    'documentoEnlace': 'https://violenciadigital.tedic.org/docs/2014_11_26_Reiteraci%C3%B3n_de_Denuncia_y_Aporte_de_Documento.pdf'
};
