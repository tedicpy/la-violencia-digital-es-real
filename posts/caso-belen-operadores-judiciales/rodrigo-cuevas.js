export default {
    // en
    'nombre_en': "Rodrigo Cuevas",
    'bio_en': `
Belén's current lawyer
    `,
    'cargo_en': "Belén's Lawyer",

    // es
    'nombre_es': "Rodrigo Cuevas",
    'bio_es': `
Actual abogado de Belén.
    `,
    'cargo_es': 'Abogado de Belén',

    'enlaceExterno': 'https://www.rdn.com.py/2019/07/02/caso-kriskovich-abogado-denuncia-que-jueces-apanan-a-acusado/'
};
