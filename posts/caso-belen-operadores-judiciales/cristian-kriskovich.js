export default {
  // en
  'nombre_en': "Cristian Kriskovich",
  'bio_en': `
Professor at the Catholic University of Asunción and member of the Council of the Judiciary since September 2013, reelected in August 2016. Since June 2016, also a member of the Jury for the Prosecution of Magistrates. His term expired in March 2021 but he remains in office nevertheless, due to tacit renewal.
  `,
  'cargo_en': 'Respondent',

  // es
  'nombre_es': "Cristian Kriskovich",
  'bio_es': `
Profesor de la UCA y miembro del Consejo de la Magistratura desde Septiembre del 2013, siendo reelecto en el mes de agosto del 2016. Desde junio del 2016 es también miembro del Jurado de Enjuiciamiento de Magistrados. En marzo del presente año, feneció su mandato pero el mismo sigue en el cargo por reconducción tácita.
  `,
  'cargo_es': 'Denunciado'
};
