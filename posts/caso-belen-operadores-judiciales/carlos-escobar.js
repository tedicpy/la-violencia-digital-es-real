export default {
    // en
    'nombre_en': "Carlos Escobar",
    'bio_en': `
Judge at the Court that resolves the appeal of the first instance resolution denying the examination of the mobile phones involved. On May 7, 2019, Belén’s defense attorneys presented a written complaint addressed to the Superintendent of the Supreme Court of Justice, against justices Carlos Escobar and Linneo Ynsfrán Saldivar, alleging poor performance of their duties.

Member of the sixth district Court of Appeals who resolves through A.I. No. 531, dated December 19, 2018, to sustain the first instance resolution denying the mobile expertise, considering it “superfluous and overabundant” for having been debated in a criminal court.
    `,
    'cargo_en': 'Judge',

    // es
    'nombre_es': "Carlos Escobar",
    'bio_es': `
Juez integrante del Tribunal que resuelve la apelación de la resolución de primera instancia que niega la pericia de los teléfonos celulares. En fecha 7 de Mayo del 2019, los abogados defensores de Belén presentan escrito de denuncia alegando mal desempeño de sus funciones en contra de los magistrados Carlos Escobar y Linneo Ynsfran Saldivar dirigido al Superintendente de la Corte Suprema de Justicia.

Integra el Tribunal de Apelaciones de la sexta sala que resuelve mediante A.I. Nro. 531 de fecha 19 de diciembre del 2018 confirmar la resolución de primera instancia que niega la pericia de los teléfonos celulares por considerarla ´superflua y sobreabundante´ en atención a haber sido debatida en sede penal.
    `,
    'cargo_es': 'Juez',

    'enlaceExterno': 'https://www.rdn.com.py/2019/07/02/caso-kriskovich-abogado-denuncia-que-jueces-apanan-a-acusado/'
};
