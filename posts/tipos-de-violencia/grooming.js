export default {
    // en
    'title_en': "Grooming",
    'content_en': `
    When an adult establishes a relationship of trust with children or adolescents, in order to make sexual requests. They often use gifts and seek to distance the victims from their support networks in order to establish a relationship of domination, control and sexual violence.
    `,
    // es
    'title_es': "Grooming",
    'content_es': `
    Cuando una persona adulta entabla una relación de confianza con infancias o adolescentes, con el fin de hacerle peticiones de tipo sexual. Muchas de las veces utilizan regalos y buscan alejar a las víctimas de sus redes de apoyo para entablar una relación de dominación, control y violencia sexual.
    `
};
