export default {
    // en
    'title_en': "Online sexual harassment",
    'content_en': `
    This type of harassment involves various attacks with a sexual emphasis, such as sending intimate or sexually aggressive images or videos, unsolicited sexual comments, and others.
    `,
    // es
    'title_es': "Acoso sexual online",
    'content_es': `
    Cuando además del hostigamiento, se utilizan diversos tipos de ataques con énfasis sexual: envío de imágenes o videos íntimos o sexualmente agresivos, comentarios sexuales no solicitados, entre otros.
    `
};
