export default {
    // en
    'title_en': "Cyberflashing",
    'content_en': `
    Sending unsolicited photographs, typically explicit images of one's genitals.
    `,
    // es
    'title_es': "Exhibicionismo digital (cyberflashing)",
    'content_es': `
    Envío de fotografías no solicitadas, usualmente son personas enviando fotografías de sus genitales.
    `
};
