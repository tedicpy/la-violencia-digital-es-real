export default {
    // en
    'title_en': "Control and manipulation of information",
    'content_en': `
    The collection or theft of information may involve a loss of information, as well as its unauthorized modification.  `,
    // es
    'title_es': "Control y manipulación de la información",
    'content_es': `
    La recopilación o robo de información puede implicar una pérdida de información, así como su modificación sin autorización.
    `
};
