export default {
    // en
    'title_en': "Shadowban",
    'content_en': `
    It refers to the practice of limiting a user's visibility on a social media platform, typically by blocking their posts without the user’s knowledge or notification. The platform does not delete the content but reduces its relevance by decreasing its visibility to almost zero. For example, posts may not appear in hashtag-related feeds and may only be displayed to the user's followers.
    `,
    // es
    'title_es': "Shadowban",
    'content_es': `
    Se denomina shadowban a la invisibilización de una persona usuaria en una plataforma, que ocurre sin notificación y sin que la persona se entere del bloqueo. La plataforma no elimina el contenido pero le resta relevancia disminuyendo su visibilidad casi a cero.  Ejemplos: no mostrar las publicaciones en una lista de posteo de un hashtag relacionado, sino solo a sus seguidores.
    `
};
