export default {
    // en
    'title_en': "Defamation and reputational damage",
    'content_en': `
      Defamation and reputational damage through false and offensive online comments. It Involves spreading false and derogatory information about someone, in order to humiliate them and expose them publicly.
    `,
    // es
    'title_es': "Difamación y daño de la reputación",
    'content_es': `
      Difamación y daño de la reputación a través de comentarios online falsos y ofensivos. Dedicado a difundir información falsa y denostativa sobre una persona, con el fin de humillarla y exponerla públicamente.
    `
};