export default {
    // en
    'title_en': "Algorithmic Bias by Artificial Intelligence",
    'content_en': `
    Artificial Intelligence (AI) raises human rights concerns by reflecting and amplifying gender biases present in society. Biased algorithms can reinforce harmful stereotypes and perpetuate discrimination on a global scale, limiting women's access to opportunities and resources. It is urgent to develop AI ethically to prevent this technology from becoming a perpetrator of gender-based violence, rather than a step forward.
    `,
    // es
    'title_es': "Sesgo algorítmico por Inteligencia Artificial",
    'content_es': `
    La Inteligencia Artificial (IA) plantea preocupaciones en torno a los derechos humanos al reflejar y amplificar prejuicios de género presentes en la sociedad. Algoritmos sesgados pueden reforzar estereotipos dañinos y perpetuar la discriminación a escala global, limitando el acceso de las mujeres a oportunidades y recursos. Urge desarrollar IA de forma ética para evitar que esta tecnología, en lugar de ser un avance, se convierta en un nuevo tipo de perpetrador de violencia de género.
    `
};
