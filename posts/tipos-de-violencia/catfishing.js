export default {
    // en
    'title_en': "Catfishing",
    'content_en': `
    Refers to the act of using fake information and photos to create a false online identity with the intent to deceive, harass, or scam someone. It often occurs on social media, dating apps, or websites as a strategy to establish relationships under false pretenses, sometimes to lure victims into financial scams.
    `,
    // es
    'title_es': "Catfishing",
    'content_es': `
    Se refiere a cuando una persona utiliza información e imágenes falsas para crear una identidad falsa en línea con la intención de engañar, acosar o estafar a otra persona. Se da con frecuencia en las redes sociales o en aplicaciones y sitios web de citas como una táctica común utilizada para entablar relaciones en línea bajo falsos pretextos, a veces para atraer a la gente hacia estafas financieras.
    `
};
