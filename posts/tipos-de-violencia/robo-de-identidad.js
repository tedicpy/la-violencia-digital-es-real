export default {
    // en
    'title_en': "Identity theft/fake profiling",
    'content_en': `
    The use of someone's identity without their consent, or the creation and disclosure of false personal data, with the intention of damaging the reputation of a person or organization.
    `,
    // es
    'title_es': "Robo de identidad/creación de perfiles falsos",
    'content_es': `
    El uso de la identidad de alguien sin su consentimiento, o la creación y divulgación de datos personales falsos, con la intención de dañar la reputación de una persona u organización.
    `
};