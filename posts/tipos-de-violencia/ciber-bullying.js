export default {
    // en
    'title_en': "Cyber bullying",
    'content_en': `
    A form of harassment and intimidation on digital platforms, often carried out through social media or messaging apps, involving the dissemination of false or degrading information to target an individual or a group.
    `,
    // es
    'title_es': "Ciber bullying",
    'content_es': `
      Ciber bullying y acoso repetido a través de mensajes con tono ofensivo y/o descalificante. Es el tipo de acoso e intimaciones en plataformas digitales por medio de redes sociales o plataformas de mensajería para acosar a una persona o un grupo de personas por medio de la divulgación de información falsa o denigrante. 
    `
};