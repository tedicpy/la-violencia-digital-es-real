export default {
    // en
    'title_en': "Hate speech",
    'content_en': `
      Speech that reflects cultural patterns that incite violence, either through comments, insults, or verbal aggression.
    `,
    // es
    'title_es': "Discurso de odio",
    'content_es': `
    Discurso que refleja modelos culturales que incitan violencia, ya sea a través de comentarios, insultos, o agresiones verbales.
    `
};