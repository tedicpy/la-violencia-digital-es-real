export default {
    // en
    'title_en': "Coordinated attacks",
    'content_en': `
    These are those carried out in a coordinated manner and by more than one person towards another, a specific publication or a page on the networks. The purpose can be varied: dissemination of personal data to cause harassment and bullying, or even achieve the elimination of the victim's profile, create false identities in order to spread false publications and news.
    `,
    // es
    'title_es': "Ataques coordinados",
    'content_es': `
    Son aquellos que se realizan de manera coordinada y por más de una persona hacia otra, una publicación específica o una página en las redes. La finalidad puede ser variada:difusión de datos personales para causar hostigamiento y acoso, o incluso lograr la eliminación de los perfiles de las víctimas, crear identidades falsas para así poder esparcir publicaciones y noticias falsas.
    `
};