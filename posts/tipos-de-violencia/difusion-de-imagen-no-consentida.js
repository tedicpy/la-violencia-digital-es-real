export default {
    // en
    'title_en': "Dissemination of intimate photos or private information",
    'content_en': `
      Disseminating private (or sensitive, controversial) information with the intent to damage reputation.
    `,
    // es
    'title_es': "Difusión de fotos íntimas o información privada",
    'content_es': `
      Diseminar información privada (o sensible, controversial) con la intención de dañar la reputación.
    `
  };
  