export default {
  // en
  'title_en': "Unauthorized account access and control (also known as hacking)",
  'content_en': `
    Unauthorized attacks to gain access to the accounts or devices of others. This may involve the unauthorized collection of information, as well as the blocking or deactivation of the victim's account; or the use of the hacked account to engage in behavior that discredits or discredits the account holder.
  `,
  // es
  'title_es': "Acceso y control no autorizado de cuentas (también conocido como hackeo)",
  'content_es': `
Ataques no autorizados para ganar acceso a las cuentas o dispositivos de otres. Esto puede implicar la recopilación no autorizada de información, así como el bloqueo o desactivación de la cuenta de la víctima; o la utilización de la cuenta hackeada para ejercer comportamientos que generen desprestigio o desacreditación de le titular de la cuenta.    
  `
};
