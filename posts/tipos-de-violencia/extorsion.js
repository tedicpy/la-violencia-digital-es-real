export default {
    // en
    'title_en': "Extortion",
    'content_en': `
    Forcing a person to act according to the will of another person, through threats and intimidation.
    `,
    // es
    'title_es': "Extorsión",
    'content_es': `
    Forzar a una persona a actuar de acuerdo a la voluntad de otra persona, a través de amenazas e intimidación.
    `
};