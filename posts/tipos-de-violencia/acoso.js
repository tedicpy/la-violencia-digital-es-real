export default {
    // en
    'title_en': "Harassment",
    'content_en': `
      Repeated and unsolicited acts against a person or organization that are perceived as intrusive or threatening.
    `,
    // es
    'title_es': "Acoso",
    'content_es': `
    Actos repetidos y no solicitados contra una persona u organización que son percibidos como intrusivos o amenazadores
    `
};