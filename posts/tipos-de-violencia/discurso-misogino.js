export default {
    // en
    'title_en': "Misogynist speech",
    'content_en': `
    Expresses hatred towards women through posts that incite both virtual and physical attacks. It is typically found in content where a woman or group of women is placed in a degrading, stereotyped and/or objectifying position.
    `,
    // es
    'title_es': "Discurso misógino",
    'content_es': `
    Aquel que expresa odio hacia las mujeres por su condición en publicaciones que incitan al ataque directo virtual y físico. Suele identificarse en aquellos materiales en donde la protagonista sea una mujer o un grupo de mujeres en posición denigrante, estereotipada y/o objetivizante.
    `
};
