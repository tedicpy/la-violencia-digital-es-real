export default {
    // en
    'title_en': "Use of GPS",
    'content_en': `
      Use of GPS or other geolocation services to track movements.
    `,
    // es
    'title_es': "Uso de GPS",
    'content_es': `
    Uso de GPS u otros servicios de geolocalización para rastreo de movimientos.
      
    `
};