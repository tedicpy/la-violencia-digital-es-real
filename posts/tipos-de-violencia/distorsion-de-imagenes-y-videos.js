export default {
    // en
    'title_en': "Distortion of images or videos, or other false content",
    'content_en': `
    Creating false, manipulated or out-of-context content, and disseminating it in order to discredit and damage a person or group.
    `,
    // es
    'title_es': "Distorsión de imágenes o vídeos, u otro contenido falso",
    'content_es': `
    Elaboración de contenido falso, manipulado o fuera de contexto, y su divulgación con el fin de desprestigiar y dañar a una persona o grupo.
    `
};
