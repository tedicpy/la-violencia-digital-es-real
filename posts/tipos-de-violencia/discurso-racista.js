export default {
    // en
    'title_en': "Racist speech",
    'content_en': `
    Speech that insults or belittles someone based on their background (such as nationality, race, ethnicity, origin, among others). It threatens the safety of migrants and indigenous peoples and has become increasingly evident within extremist and conservative groups.
    `,
    // es
    'title_es': "Discurso racista",
    'content_es': `
    Enfocado a denostar a una/as persona/as por su procedencia (ya sea nacionalidad, raza, étnica, origen, entre otro). Atenta contra la seguridad de las personas migrantes y pueblos originarios y se ha evidenciado con mayor regularidad en grupo de extremos y conservadores.
    `
};
