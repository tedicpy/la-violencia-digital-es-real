export default {
    // en
    'title_en': "Use of spyware",
    'content_en': `
    Use of spyware (software to spy on and obtain information from other devices) or access to accounts without the user's consent.
    `,
    // es
    'title_es': "Uso de spyware",
    'content_es': `
     Uso de spyware (software para espiar y obtener información de otros dispositivos) o acceso a cuentas sin el consentimiento del usuarie      
    `
};