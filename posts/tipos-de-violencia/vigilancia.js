export default {
    // en
    'title_en': "Surveillance",
    'content_en': `
    The constant monitoring of a person's online activities, daily life, or information, whether public or private.
    `,
    // es
    'title_es': "Vigilancia",
    'content_es': `
    El monitoreo constante de las actividades en línea de la persona, su vida diaria, o información, sea pública o privada.
    `
};