export default {
    // en
    'title_en': "Shaming or public humiliation",
    'content_en': `
    Happens when sensitive information, like intimate images or videos, is shared without the affected person’s consent. This practice involves the public dissemination of private or intimate information aimed at humiliating, shaming, or belittling the victim.
    `,
    // es
    'title_es': "Shaming o humillación pública",
    'content_es': `
    El shaming, también conocido como “humillación pública”, ocurre cuando se comparte información sensible, como imágenes o videos íntimos, sin el consentimiento de la persona afectada. Esta práctica implica la difusión pública de información privada o íntima con el objetivo de humillar, avergonzar o denigrar a la víctima.
    `
};
