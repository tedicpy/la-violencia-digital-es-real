export default {
    // en
    'title_en': "Speech against diversity",
    'content_en': `
    Refers to attacks on a group of people within the LGBTQ+ community, judging, harming, or belittling their integrity in the digital environment. These attacks can be homophobic, lesbophobic, biphobic, transphobic, etc.
    `,
    // es
    'title_es': "Discurso en contra de la diversidad",
    'content_es': `
    Aquellos que atentan a un grupo de personas de la diversidad sexual, enjuiciado, dañando, o denostando su integridad en el ambiente digital. Pueden ser homofóbico, lesbofóbico, bifófico, transfóbico, etc.
    `
};
