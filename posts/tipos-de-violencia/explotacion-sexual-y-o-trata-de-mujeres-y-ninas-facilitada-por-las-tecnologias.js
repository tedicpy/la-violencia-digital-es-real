export default {
    // en
    'title_en': "Sexual exploitation and trafficking of women and girls facilitated by technology (TFT)",
    'content_en': `
    This form of violence involves the use of technology to exert power over a victim with the aim of committing abuse or sexual exploitation of their image and/or body against their will.
    `,
    // es
    'title_es': "Explotación sexual y/o trata de mujeres y niñas facilitada por las tecnologías",
    'content_es': `
    Esta forma de violencia conlleva la intermediación de las tecnologías para el ejercicio de poder sobre una víctima con el objeto de cometer abuso o explotación sexual de su imagen y/o de su cuerpo contra su voluntad.
    `
};
