export default [
  {
    'key': "acceso-no-autorizado-de-cuentas",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "control-y-manipulacion-de-la-informacion",
    'bgColor': "brand-pink-3"
  },
  {
    'key': "difusion-de-imagen-no-consentida",
    'bgColor': "brand-green-2"
  },
  {
    'key': "doxeo",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "vigilancia",
    'bgColor': "brand-pink-3"
  },
  {
    'key': "uso-de-spyware",
    'bgColor': "brand-green-2"
  },
  {
    'key': "robo-de-identidad",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "distorsion-de-imagenes-y-videos",
    'bgColor': "brand-green-2"
  },
  {
    'key': "difamacion-y-dano-a-la-reputacion",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "acoso",
    'bgColor': "brand-pink-3"
  },
  {
    'key': "ciber-bullying",
    'bgColor': "brand-green-2"
  },
  {
    'key': "discursos-de-odio",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "amenazas",
    'bgColor': "brand-pink-3"
  },
  {
    'key': "mobbing",
    'bgColor': "brand-green-2"
  },
  {
    'key': "extorsion",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "shaming-o-humillacion-publica",
    'bgColor': "brand-green-2"
  },
  {
    'key': "ataques-coordinados",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "uso-de-gps",
    'bgColor': "brand-pink-3"
  },
  {
    'key': "shadowban",
    'bgColor': "brand-green-2"
  },
  {
    'key': "sesgo-algoritmico-por-inteligencia-artificial",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "grooming",
    'bgColor': "brand-pink-3"
  },
  {
    'key': "gordofobia-digital",
    'bgColor': "brand-green-2"
  },
  {
    'key': "explotacion-sexual-y-o-trata-de-mujeres-y-ninas-facilitada-por-las-tecnologias",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "exhibicionismo-digital-cyberfashing",
    'bgColor': "brand-pink-3"
  },
  {
    'key': "discurso-racista",
    'bgColor': "brand-green-2"
  },
  {
    'key': "discurso-en-contra-de-la-diversidad",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "discurso-misogino",
    'bgColor': "brand-pink-3"
  },
  {
    'key': "catfishing",
    'bgColor': "brand-green-2"
  },
  {
    'key': "acoso-sexual-online",
    'bgColor': "brand-indigo-3"
  },
];