export default {
    // en
    'title_en': "Doxing",
    'content_en': `
    Investigating and disseminating information that allows the identification of a person without their consent, often with the intention of gaining access or contact with the person for harassment or other harmful purposes.
    `,
    // es
    'title_es': "Doxeo",
    'content_es': `
    Investigar y difundir información que permita identificar a una persona sin su consentimiento, muchas veces con la intención de tener acceso o contacto con la persona con fines de acoso u otros fines nocivos.
    `
};