export default {
    // en
    'title_en': "Threats",
    'content_en': `
    Speech and content (verbal or written, in images, etc.) with an aggressive and/or threatening tone. Direct threats of violence of any kind.
    `,
    // es
    'title_es': "Amenazas",
    'content_es': `
     Discurso y contenido (verbal o escrito, en imágenes, etc.) con un tono agresivo y/o amenazador. Amenazas directas de violencia de cualquier índole.
    `
};