export default {
    // en
    'title_en': "Digital fat shaming",
    'content_en': `
    The practice of shaming or humiliating a person, most often women, based on their weight. It involves any action of rejection and hatred towards fat bodies. On the Internet, it can take the form of humiliating behavior. The practice has become normalized through a Eurocentric stereotype of what is considered "acceptable."
    `,
    // es
    'title_es': "Gordofobia digital",
    'content_es': `
    Práctica de avergonzar o humillar a una persona, en la mayoría de los casos a mujeres, en base a su peso. Es toda acción de rechazo y odio hacia los cuerpos gordos. En Internet, puede manifestarse humillantes. La práctica se ha normalizado en nombre de un estereotipo euro céntrico de lo que debe ser "aceptado". En inglés se le conoce como fat shaming.
    `
};
