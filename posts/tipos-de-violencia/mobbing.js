export default {
    // en
    'title_en': "Mobbing",
    'content_en': `
    Consists of workplace harassment of an individual or a group. This behavior occurs both outside and inside digital spaces
    `,
    // es
    'title_es': "Mobbing",
    'content_es': `
    Consiste en el acoso laboral ejercido tanto a una persona como a un grupo. Esta conducta se da tanto fuera como dentro de los espacios digitales.
    `
};