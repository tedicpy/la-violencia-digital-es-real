export default {
  // en
  'title_en': "MENTA SIRUELA",
  'content_en': `
    Para combatir a la violencia en línea, debemos abordar el problema de forma integral. Para combatir a la violencia en línea.    
  `,

  // es
  'title_es': "MENTA SIRUELA",
  'content_es': `
    Para combatir a la violencia en línea, debemos abordar el problema de forma integral. Para combatir a la violencia en línea.    
  `,

  'images': [
    { file: "menta-siruela-cuadro.png" },
    { file: "mentasiruelaflyers-01.png" },
    { file: "mentasiruelaflyers-02.png" },
    { file: "mentasiruelaflyers-03.png" },
    { file: "mentasiruelaflyers-04.png" },
    { file: "mentasiruelaflyers-05.png" },
  ]
};
