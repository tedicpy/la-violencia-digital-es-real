export default {
  // en
  'title_en': "MORENA TORO",
  'content_en': `
Para combatir a la violencia en línea, debemos abordar el problema de forma integral. Para combatir a la violencia en línea.
    `,

  // es
  'title_es': "MORENA TORO",
  'content_es': `
Para combatir a la violencia en línea, debemos abordar el problema de forma integral. Para combatir a la violencia en línea.
  `,

  'images': [
    { file: "morena-toro-cuadro.png" },
    { file: "morenatoroflyers_morena toro 1.png" },
    { file: "morenatoroflyers_morena toro 2.png" },
    { file: "morenatoroflyers_morena toro 3.png" },
    { file: "morenatoroflyers_morena toro 4.png" },
    { file: "morenatoroflyers_morena toro 5.png" },
  ]
};
