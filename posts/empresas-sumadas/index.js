export default [
  {
    'key': "morena-toro",
    'imageFile': "morena-toro.png",
    'imageClass': '',
    'bgColor': "brand-pink-3",
  },
  {
    'key': "menta-siruela",
    'imageFile': "menta-siruela.png",
    'imageClass': '',
    'bgColor': "brand-green-2",
  },
  {
    'key': "bliss",
    'imageFile': "bliss.png",
    'imageClass': '',
    'bgColor': "brand-indigo-3",
  },
  {
    'key': "albertina",
    'imageFile': "albertina.png",
    'imageClass': '',
    'bgColor': "brand-red-2",
  }
];