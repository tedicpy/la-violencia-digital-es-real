export default {
  // en
  'title_en': "BLISS",
  'content_en': `
Para combatir a la violencia en línea, debemos abordar el problema de forma integral. Para combatir a la violencia en línea.
    `,

  // es
  'title_es': "BLISS",
  'content_es': `
Para combatir a la violencia en línea, debemos abordar el problema de forma integral. Para combatir a la violencia en línea.
  `,

  'images': [
    { file: "bliss-cuadro.png" },
    { file: "afiches bliss_1.jpg" },
    { file: "afiches bliss_2.jpg" },
    { file: "afiches bliss_3.jpg" },
    { file: "afiches bliss_4.jpg" },
    { file: "afiches bliss_5.jpg" },
  ]
};
  