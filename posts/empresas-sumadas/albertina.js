export default {
  // en
  'title_en': "ALBERTINA",
  'content_en': `
Para combatir a la violencia en línea, debemos abordar el problema de forma integral. Para combatir a la violencia en línea.
  `,
  
  // es
  'title_es': "ALBERTINA",
  'content_es': `
Para combatir a la violencia en línea, debemos abordar el problema de forma integral. Para combatir a la violencia en línea.
  `,

  'images': [
    { file: "albertina-cuadro.png" }, 
    { file: "afiches albertina_1.jpg" },
    { file: "afiches albertina_2.jpg" },
    { file: "afiches albertina_3.jpg" },
    { file: "afiches albertina_4.jpg" },
    { file: "afiches albertina_5.jpg" },
  ]
};
  