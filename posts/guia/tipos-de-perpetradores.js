export default {
  // en
  'title_en': 'Types of perpetrators and their motivations',
  'content_en': `
Understanding and addressing technology-facilitated gender-based violence is a complex challenge that faces multiple barriers, especially in identifying and analyzing perpetrators. [Research](https://www.tedic.org/wp-content/uploads/2024/07/Perpetradores-de-violencia-de-genero-online-1.pdf) by the World Wide Web Foundation and member organizations of Women's Rights Online (WRO), including TEDIC, highlights several key obstacles in this area:

#### Lack of methodologies and information

A key problem is the absence of standardized definitions and tools, which makes it difficult to effectively compare TFGVB behaviors and tactics in different contexts. Additionally, the lack of detailed data on perpetrators, such as their demographics, motivations and behavioral patterns, limits the understanding of this phenomenon. Most of the information is in English, restricting the training of monitoring models that include different languages and platforms. The digital divide and lack of research on violence against specific social groups, such as LGBTQIA+ people, further complicate data collection.

#### Challenges in identifying and understanding perpetrators

The profiles of perpetrators of TFGBV are diverse and complex.They include not only individuals, but also groups, State actors and other entities, such as the technology platforms themselves. Whether for ideological, economic, or cultural reasons, the motivations of these perpetrators are often hidden and require deeper understanding. The relationship between socioeconomic and cultural factors —like unemployment or norms of masculinity—and TFGBV also needs further exploration.

#### Lack of accountability and effective action

Impunity for perpetrators remains a significant challenge, exacerbated by the ineffectiveness of justice systems in identifying and prosecuting them. This issue is further compounded by the inaction of technology platforms, which frequently fail to implement effective measures against online violence. Additionally, the platforms’ limited transparency regarding data access hinders the development of concrete research and solutions.

#### Need for comprehensive and multidisciplinary approaches

To effectively address TFGBV, it is crucial to understand both online and offline behaviors, as they are interconnected. This requires collaboration among governments, technology companies, civil society organizations and other stakeholders. It is also critical to research how to implement effective strategies for behavior change to influence perpetrators.

#### Minors as perpetrators
  
The lack of data on minors committing TFGBV limits the understanding of how cyberbullying affects this group and what interventions are needed. The connection between online and offline behaviors in minors also needs further research in order to develop appropriate prevention and support measures.

### How do we identify perpetrators?

Online and ICT-facilitated gender-based violence affects women in diverse ways, and from an intersectional perspective, certain groups face particularly high risk. Women from marginalized communities—such as racialized women, LGTBIQA+ individuals, migrants, and women with disabilities—often face disproportionate attacks on digital platforms, where multiple forms of discrimination converge. Additionally, young women and feminist activists who engage in public visibility spaces are frequent targets of harassment and threats due to their roles and opinions. This intersectional perspective is essential for understanding how identity factors intertwine to heighten vulnerability to digital violence, highlighting the need for specific and inclusive responses in the protection and support of these groups.

To define the types of aggressors, a list was designed based specifically on [ABRAJI](https://abraji.org.br/)'s proposal regarding violence against women journalists, UN Women's research on violence against women politicians  [(ONU Mujeres, 2021)](https://lac.unwomen.org/es/digiteca/publicaciones/2021/03/violencia-contra-las-mujeres-en-politica), and research on digital violence against women journalists [(Acuña and Sequera, 2023)](https://www.tedic.org/wp-content/uploads/2023/10/Violencia-Genero-Periodistas-TEDIC-2023-ENG-web-1.pdf).

The following list is not exhaustive but captures a wide variety of types of aggressors or perpetrators of technology-facilitated gender-based violence that women and diverse groups face:
  `,

  // es
  'title_es': 'Tipos de perpetradores y sus motivaciones',
  'content_es': `
Comprender y abordar la violencia de género facilitada por la tecnología es un reto complejo, que enfrenta múltiples barreras, especialmente en la identificación y análisis de los perpetradores. [La investigación](https://www.tedic.org/wp-content/uploads/2024/07/Perpetradores-de-violencia-de-genero-online-1.pdf) de la World Wide Web Foundation y las organizaciones miembro de Women’s Rights Online (WRO) donde TEDIC forma parte, destaca varios obstáculos clave en este ámbito:

#### Falta de metodologías e información

Un problema esencial es la ausencia de definiciones y herramientas estandarizadas, lo que dificulta una comparación efectiva de los comportamientos y tácticas de la VGFT en diferentes contextos. Además, la falta de datos detallados sobre los perpetradores, como su demografía, motivaciones y patrones de comportamiento, limita la comprensión de este fenómeno. La mayoría de la información está en inglés, restringiendo el entrenamiento de modelos de monitoreo que abarquen diferentes idiomas y plataformas. La brecha digital y la falta de investigación sobre la violencia en ciertos grupos sociales, como las personas LGBTQIA+, complican aún más la recopilación de datos.

#### Dificultades en la identificación y comprensión de los perpetradores

Los perfiles de los perpetradores de VGFT son variados y complejos. No solo incluyen a individuos, sino también a grupos, actores estatales y otras entidades, como las plataformas tecnológicas mismas. Las motivaciones de estos perpetradores a menudo están ocultas, ya sea por razones ideológicas, económicas o culturales, y requieren una comprensión más profunda. La relación entre factores socioeconómicos y culturales —como el desempleo o las normas de masculinidad— y la VGFT también necesita ser investigada.

#### Falta de responsabilidad y respuesta efectiva

La impunidad de los perpetradores sigue siendo un desafío central, exacerbado por la ineficacia de los sistemas de justicia en identificar y procesarlos. A esto se suma la falta de acción de las plataformas tecnológicas, que a menudo no implementan medidas eficaces para combatir la violencia en línea. La transparencia limitada de las plataformas en cuanto al acceso a datos complica el desarrollo de investigaciones y soluciones concretas.

#### Necesidad de enfoques integrales y multidisciplinarios

Para abordar eficazmente la VGFT, es crucial entender tanto los comportamientos en línea como fuera de línea, ya que están interconectados. Esto exige la colaboración de gobiernos, empresas tecnológicas, organizaciones de la sociedad civil y otros actores. También es fundamental investigar cómo implementar estrategias efectivas de cambio de comportamiento para influir en los perpetradores.

#### Menores como perpetradores

La falta de datos sobre menores que cometen actos de VGFT limita la comprensión de cómo el ciberacoso afecta a este grupo y qué intervenciones son necesarias. La conexión entre comportamientos en línea y fuera de línea en menores también necesita mayor investigación para desarrollar medidas de prevención y apoyo adecuadas.

### ¿Cómo identificamos a las personas agresoras?

La violencia de género en línea y facilitada por las TIC afecta a mujeres de diversas maneras, y desde una mirada interseccional, ciertos grupos enfrentan un riesgo particularmente elevado. La Mujeres de comunidades marginadas, como las mujeres racializadas, LGTBIQA+, migrantes, y mujeres con discapacidades, suelen experimentar ataques desproporcionados en plataformas digitales, donde convergen múltiples formas de discriminación. Además, mujeres jóvenes y activistas feministas que participan en espacios de visibilidad pública son blanco frecuente de acoso y amenazas debido a sus roles y opiniones. Esta perspectiva interseccional es esencial para comprender cómo factores de identidad se entrelazan para aumentar la vulnerabilidad frente a la violencia digital, lo que subraya la necesidad de respuestas específicas e inclusivas en la protección y apoyo de estos grupos.

Para definir los tipos de agresores se diseñó una lista basada específicamente en la propuesta de [ABRAJI](https://abraji.org.br/) sobre violencia a mujeres periodistas, en las identificaciones de ONU Mujeres sobre violencia a mujeres políticas [(ONU Mujeres, 2021)](https://lac.unwomen.org/es/digiteca/publicaciones/2021/03/violencia-contra-las-mujeres-en-politica)y en la investigación sobre la violencia digital a mujeres periodistas [(Acuña y Sequera, 2023)](https://www.tedic.org/wp-content/uploads/2023/10/Violencia-Genero-Periodistas-TEDIC-2023-web-2.pdf). La siguiente lista no es exhaustiva pero recoge la mayor variedad de tipos de agresores o perpetradores de violencia de género facilitada por la tecnología que reciben las mujeres y las diversidades:
  `,
}