export default {
  // en
  'title_en': "Digital Rights",
  'content_en': `
When we talk about digital rights, we refer to human rights in the digital spheres. There is a tendency to think that online and offline spaces are separate spaces, but we have already seen that they are fully related and constantly affect each other. Therefore, no new rights are created from the digital, but rather pre-existing fundamental rights apply equally in these spaces.

By not having free access to ICTs and the digital ecosystem, digital rights for full development such as the right to freedom of expression, the right to privacy, the right to information, sexual and reproductive rights and the right to non-discrimination are lost. That is, any woman or girl with or without access to technology is exposed to be a victim of this type of violence, which has a direct impact on the ability of empowerment or equality for women.

## Some of the digital rights are:

### The right to live free of gender-based violence

![A person holding the scales of justice with one side on fire {image-left}](/blog-files/guia/blog-03.png)

According to Article 1 of the [Convention of Belém do Pará](https://www.oas.org/juridico/english/treaties/a-61.html), technology-
facilitated gender-based violence against women and girls is defined as any act or behavior based on gender that causes death, physical, sexual, psychological, economic or symbolic harm. This violence can be committed, instigated, or exacerbated, in whole or in part, through the use of information and communication technologies, which expands its scope and severity in the digital environment. An updated interpretation of Articles 3 and 6 of the Convention of Belém do Pará establishes that women have the right to live free from violence in their online interactions, both in public and private spaces. This right includes protection against discrimination on the
Internet, recognition free from gender stereotypes, and the obligation of States not only to prevent actions that violate this right but also to
implement positive measures to ensure that women and girls can fully exercise it in digital spaces.  

Since 2017, the Committee of Experts of the Follow-up Mechanism of the Convention of Belém do Pará (MESECVI), in its [Third Hemispheric Report](https://www.oas.org/en/mesecvi/docs/TercerInformeHemisferico-en.pdf) on the Implementation of the Convention of Belém do Pará, has recognized the right of women to a life free of violence facilitated by new technologies. [This recognition aligns with General Recommendation No. 35 of the CEDAW Committee](https://docs.un.org/en/CEDAW/C/GC/35), which notes that violence against women also manifests in digital environments, redefining the boundaries between the public and the private.

Furthermore, in 2018, [the RELE stated that violence through electronic media](https://www.oas.org/es/cidh/expresion/docs/informes/MujeresPeriodistas.pdf) is a form of gender discrimination that States must address as part of their obligations under the American Convention on Human Rights.

The right of women and girls to live free from violence in digital environments has also been recognized by the United Nations System. This right has been referenced in resolutions by the General Assembly and in reports from the [Human Rights Council](https://docs.un.org/en/A/HRC/res/47/16), reinforcing the need to protect women and girls in digital spaces as part of global efforts to eradicate gender-based violence.

### The right to freedom of expression and access to information

Technology-facilitated gender-based violence (TFGBV) not only constitutes a violation of their fundamental rights, but also restricts their freedom of expression, a right recognized in Article 13 of the [American Convention on Human Rights (ACHR)](https://www.oas.org/dil/treaties_b-32_american_convention_on_human_rights.pdf) and Article 19 of the [International Covenant on Civil and Political Rights](https://www.ohchr.org/en/instruments-mechanisms/instruments/international-covenant-civil-and-political-rights) (ICCPR). This right includes the ability to seek, receive, and disseminate information and ideas freely on the Internet without censorship or other interference.

The Internet has become a key tool for political, social and economic development, enabling people to access information, participate in decision- making processes and exercise their rights. It also facilitates women's access to resources that enable them to make [informed and autonomous decisions regarding their bodies, lives and health, including sexual and reproductive health and reproductive rights](https://www.unodc.org/lpomex/noticias/marzo-2023/naciones-unidas-insta-a-eliminar-las-brechas-que-privan-a-mujeres-y-ninas-a-acceder-plenamente-a-la-era-digital.html). However, online gender-based violence prevents many women from fully exercising these rights, generating self-censorship and reducing their participation in the digital space. The [RELE has emphasized](https://www.oas.org/en/iachr/expression/docs/publications/internet_2016_eng.pdf) that access to the Internet "constitutes a sine qua non condition for the effective exercise of human rights today", including freedom of expression,
opinion, association and education. However, online violence has created a "chilling effect on the exercise of freedom of expression", limiting the presence of women's voices in the media and reinforcing gender inequality.

This situation has serious consequences not only for the affected women but also for society as a whole. The absence of women in public debate "undermines the social dimension of the right to freedom of expression", weakening democratic deliberation and good governance. The [UN Special Rapporteur on Violence against Women](https://docs.un.org/en/A/HRC/38/L.6) has warned that this situation generates "a society in which women no longer feel safe online
or offline, due to widespread impunity for perpetrators of gender-based violence".

[TFGBV also exacerbates the gender digital divide](https://lac.unwomen.org/sites/default/files/2022-11/MUESTRA%20Informe%20Violencia%20en%20linea%202.1%20\(2\)_Aprobado%20\(Abril%202022\)_0.pdf),
as many women withdraw from online spaces due to intimidation and harassment.This not only affects their right to information, but also negatively impacts their access to economic opportunities and digital literacy. Additionally, it restricts their ability to access information on ["sexual and reproductive health, while respecting confidentiality and eliminating stigma-related barriers"](https://docs.un.org/en/A/HRC/35/9). Certain groups of women are particularly affected by this situation, such as activists, human rights defenders, journalists, and politicians, who greatly rely on ICT for their work. In this regard, the [United Nations High Commissioner for Human Rights](https://docs.un.org/en/A/HRC/32/38) has emphasized that States "have an obligation to combat violence against women online and to protect freedom of opinion and expression". To achieve this, it
is essential to adopt effective measures to ensure a safe and inclusive digital environment for all women.

### The right to privacy and personal data protection

The right of women and girls to privacy, recognized in Article 11 of the ACHR nd Article 17 of the ICCPR, is also threatened by online violence. Practices such as surveillance, control of personal accounts, non-consensual sharing of intimate images, and cyberbullying disproportionately affect women, limiting their autonomy and security in digital spaces. [The Inter-American Court of Human Rights has noted that](https://corteidh.or.cr/docs/casos/articulos/resumen_252_esp.pdf) the concept of private life is a broad term not susceptible to exhaustive definitions, but which includes, among other protected areas, sexual life and the right to establish and develop relationships with other human beings.

In this regard, the right to privacy on the Internet implies the possibility
of using tools such as [data encryption, anonymity or the use of pseudonyms in social networks in order to minimize the risk of interference in private life](https://docs.un.org/en/A/RES/68/167). This is especially relevant for women human rights defenders and women seeking information considered taboo in their societies.

[Anonymity and encryption](https://rm.coe.int/CoERMPublicCommonSearchServices/DisplayDCTMContent?documentId=09000016804d5b31) can contribute to the full enjoyment of individuals' rights, including the rights to freedom of opinion and expression, and to privacy, in accordance with international law. [The MESECVI has warned that women's right to privacy](https://undocs.org/A/RES/68/167) is under threat due to mass digitization and the use of surveillance technologies. Furthermore, surveillance and collection of personal data particularly impact women due to structural discrimination. The lack of privacy in digital spaces reinforces the notion that women need to be controlled, which amplifies their vulnerability and exposure.

### The right to assembly and freedom of association

TFGBV also affects women's right to assembly and freedom of association, which are protected by Article 4(h) of the Convention of Belém do Pará, Articles 15 and 16 of the ACHR, and Articles 21 and 22 of the ICCPR. These rights must be guaranteed "regardless of whether they are exercised in person, through contemporary technical means, or with those that may be invented in the future".

Women [have the right to use the Internet to associate freely and develop activities with a lawful purpose](https://docs.un.org/en/A/HRC/41/41) without pressure or interference that would alter their purpose. This includes the ability to choose digital platforms to mobilize, protest, and participate in debates on public policy and legislative initiatives. As highlighted by the UN Special Rapporteur on the rights to freedom of peaceful assembly and association in [his 2012 report](https://docs.un.org/en/A/HRC/41/41), the Internet and social media have become fundamental tools for exercising these rights, which are essential to other civil, cultural, economic, political and social rights.

For women and girls, who have historically been marginalized from public spaces, the Internet has been key to advancing the feminist movement and exposing gender-based violence. [Examples of this include the #MeToo, #NiUnaMenos, #MiPrimerAcoso and #PrimeiroAssédio movements](https://adc.org.ar/wp-content/uploads/2019/06/Latin-American-Report-on-Online-Gender-Violence-final_v2.pdf), which have successfully
amplified reports of abuse and combined online and offline activism.

However, further [research](https://www.tedic.org/wp-content/uploads/2024/09/WEB.pdf) in the region has shown that women are exposed to new forms of TFGBV such as the dissemination of sexualized images, disinformation aimed at damaging their reputation, threats of sexual violence and femicide, and doxxing. These attacks impact their ability to gather and build virtual communities, undermining their visibility and participation in
public life. As a result, many women are forced to leave the digital space, which not only affects their safety, but also limits their access to support networks and counseling. In light of this reality, States must adopt effective measures to protect women and girls from online gender-based violence, fulfilling their obligation to prevent, investigate, punish, and remedy these acts, thereby ensuring the full exercise of their rights to freedom of assembly and association in the digital environment.

###  The right to personal integrity

TFGBV violates the right of women and girls to physical, psychological and moral integrity, as protected by Article 4(b) of the Convention of Belém do Pará and Article 5 of the ACHR. These rights guarantee that women are treated with dignity and are not subjected to torture or cruel, inhuman or degrading treatment or punishment. The effects of TFGBV can lead to serious short and long-term consequences for victims, impacting their individual development. Beyond psychological harm, physical repercussions stemming from online violence have also been documented. The right to physical integrity can be compromised when attacks such as doxxing lead to offline violence or when the non-consensual dissemination of intimate images causes such extreme levels of revictimization that they may drive the victim to suicide.

The [MESECVI has recognized that](https://belemdopara.org/guia-practica-para-la-aplicacion-del-sistema-de-indicadores-de-progreso-para-la-medicion-de-la-implementacion-de-la-convencion-de-belem-do-paramesecvi-2014/) violations of the right to physical and psychological integrity take various forms, ranging from torture to other cruel, inhuman, or degrading treatment, with consequences that vary in intensity depending on multiple factors.

In light of this reality, it is essential for States to acknowledge the impact of online gender-based violence and adopt effective measures to prevent, punish and remedy these acts, ensuring the protection of women's and girl's physical and psychological integrity.

###  Other human rights

TFGBV can also lead to the violation of other fundamental rights, including:

#### The right to personal freedom and security 

Recognized in Article 4(c) of the Convention of Belém do Pará. Digital attacks against women and girls affect their ability to organize their personal and social lives according to their own beliefs and decisions. According to the [CEVI this right entails each individual's capacity to self-determine and freely choose the circumstances that give meaning to their existence](https://belemdopara.org/guia-practica-para-la-aplicacion-del-sistema-de-indicadores-de-progreso-para-la-medicion-de-la-implementacion-de-la-convencion-de-belem-do-paramesecvi-2014/).

#### The right to respect for honor and recognition of dignity

Established in Article 11 of the ACHR and Article 17 of the ICCPR. This protection aims to prevent the dissemination of false information or the non-consensual publication of intimate images and videos that may damage women's reputations and limit their social participation, reinforcing gender stereotypes about sexuality.

#### Violations of sexual and reproductive rights and the right to sexual freedom

In terms of sexual autonomy, in cases of non-consensual dissemination of intimate images. [These acts of violence tend to generate a permanent re-victimization by society,](https://www.tedic.org/wp-content/uploads/2021/09/Imagen-no-consentida-Tedic-web.pdf) punishing women for the free exercise of their sexuality.

#### The right to freedom of movement

Established in Article 22 of the ACHR and Article 12 of the ICCPR. This right is impacted when a woman is a victim of doxxing or receives threats of physical harm, death, or sexual violence, forcing her to change her residence or limit her mobility out of fear of retaliation.

#### The right to protection against labor discrimination based on gender

Established in Articles 3 and 6 of the Additional Protocol to the ACHR and Articles 6 and 7 of the International Covenant on Economic, Social and Cultural Rights. [This happens when a victim loses their job or a job opportunity due to online attacks that damage their reputation, such as the spread of false information](https://repositorio.redalas.net/sites/default/files/2021-02/La_violencia_de_genero_en_Mexico_y_las_t.pdf).

#### The right to access to justice

Recognized in Articles 8 (judicial guarantees) and 25 (judicial protection) of the ACHR. [TFGBV often lead to the systematic denial of justice, omissions in due process, and judicial obstacles to identifying and punishing those responsible, preventing a comprehensive remedy for victims](https://www.apc.org/en/pubs/online-gender-based-violence-submission-association-progressive-communications-united-nations).

### Tension between rights

The fight against TFGBV presents tensions between human rights, heightened by abusive government restrictions on the Internet. The conflicting rights include freedom of expression, privacy and the right to live free from violence. Often, the balance of these rights is struck without a proper gender perspective, leading to the prioritization of some over others. Although human rights are interdependent and indivisible, their exercise is not absolute and may be subject to legal, necessary and proportional restrictions (ACHR, UNESCO). In practice, TFGBV tends to be considered less of a priority compared to other rights. UNESCO [has noted the lack of a gender perspective](https://unesdoc.unesco.org/%0Aark:/48223/p%EE%80%BF0000246527)
in these debates and the need to acknowledge women's experiences to inform more equitable policies. It has been shown that technology is used to control and silence women, perpetuating gender discrimination both online and offline.
This contributes to their marginalization and limits their access to the benefits of ICTs.

Article 13.5 of the ACHR prohibits advocacy of hatred that incites violence, including online hate speech directed against women. The RELE [has established that in order to criminalize such speech, it must be public](https://www.oas.org/en/iachr/expression/docs/publications/INTER-AMERICAN%20LEGAL%20FRAMEWORK%20OF%20THE%20RIGHT%20TO%20FREEDOM%20OF%20EXPRESSION%20FINAL%20PORTADA.pdf),
represent a real and imminent danger, and demonstrate intent to harm. While freedom of expression is fundamental in a democracy, its exercise carries responsibilities, allowing for narrowly defined restrictions to prevent hate speech and violence.

The Inter-American System for the Protection of Human Rights has developed a ["tripartite test" to evaluate the validity of restrictions on freedom of expression](https://www.oas.org/en/iachr/expression/docs/publications/internet_2016_eng.pdf): these must be clearly defined by law, necessary in a democratic society, and proportional to the intended objective. Any restriction must be ordered by a competent judge and respect due process ([RELE](https://www.oas.org/es/cidh/expresion/docs/publicaciones/MARCO%20JURIDICO%20INTERAMERICANO%20DEL%20DERECHO%20A%20LA%20LIBERTAD%20DE%20EXPRESION%20ESP%20FINAL%20portada.doc.pdf)).

In exceptional cases, measures to block and filter illegal content are allowed, as long as they are specific, proportional, and under judicial oversight. They must have safeguards against abuse, ensure transparency and be the only option available for a legitimate objective. States have the obligation to guarantee equal access to rights on the Internet (Convention of Belém do Pará, art. 8 (g)),which includes promoting content guidelines that help eradicate violence against women and girls. The lack of response to online violence legitimizes these behaviors and fosters impunity. The IACHR Rapporteurship has urged States to prohibit hate speech that incites violence and to ensure a safe and inclusive digital environment with a gender perspective. For expressions that do not directly incite violence, [the application of non-criminal sanctions, such as economic reparations or alternative measures, is recommended](https://www.eff.org/files/2015/10/31/manila_principles_1.0.pdf).
    `,

  // es
  'title_es': "Derechos digitales",
  'content_es': `
  
Cuando hablamos de derechos digitales nos referimos a los derechos humanos en las esferas digitales. Se tiende a pensar que los espacios en línea y aquellos fuera de línea son espacios separados, pero ya vimos que están plenamente relacionados e inciden constantemente entre sí. Por tanto, no se crean nuevos derechos a partir de lo digital, sino que los derechos fundamentales pre existentes se aplican de igual forma en esos espacios.

Al no contar con un libre acceso a las TICs y el ecosistema digital, se pierden derechos digitales para el pleno desarrollo como son el derecho a la libertad de expresión, el derecho a la intimidad, el derecho a la información, los derechos sexuales y reproductivos y el derecho a la no discriminación. Es decir, cualquier mujer o niña con o sin acceso a la tecnología está expuesta a ser víctima de este tipo de violencia, lo que repercute directamente en la capacidad de empoderamiento o igualdad para las mujeres.

## Algunos de los derechos digitales son:

###  El derecho a vivir libre de violencia de género

![Una persona sosteniendo la balanza de la justicia con un lado incendiado {image-left}](/blog-files/guia/blog-03.png)

De acuerdo con el artículo 1 de la [Convención de Belém do Pará](https://www.oas.org/juridico/spanish/tratados/a-61.html), la violencia de género facilitada por la tecnología contra mujeres y niñas se define como cualquier acto o comportamiento basado en el género que cause muerte, daño físico, sexual, psicológico, económico o simbólico. Esta violencia puede ser cometida, instigada o exacerbada, total o parcialmente, mediante el uso de
tecnologías de la información y comunicación, lo que amplía su alcance y gravedad en el entorno digital. Una interpretación actualizada de los artículos 3 y 6 de la Convención de Belém do Pará establece que las mujeres
tienen el derecho a vivir sin violencia en sus interacciones en línea, tanto en el ámbito público como privado. Este derecho incluye la protección contra la discriminación en Internet, la valoración libre de estereotipos de género y la obligación de los Estados no solo de evitar acciones que violen este derecho, sino también de implementar medidas positivas para garantizar que las mujeres y niñas puedan ejercerlo plenamente en el espacio digital.

Desde 2017, el Comité de Expertas del Mecanismo de Seguimiento de la Convención de Belém do Pará (MESECVI), en su [Tercer Informe Hemisférico](https://www.oas.org/es/mesecvi/docs/TercerInformeHemisferico-ES.pdf) sobre la implementación de la Convención de Belém do Pará, hareconocido el derecho de las mujeres a una vida libre de violencia facilitada por las nuevas tecnologías. [Este reconocimiento se alinea con la Recomendación General Núm. 35 del Comité CEDAW](https://docs.un.org/es/CEDAW/C/GC/35), que señala que la violencia contra las mujeres también se manifiesta en entornos digitales, redefiniendo las fronteras entre lo público y lo privado.
Además, en 2018, [El RELE destacó que la violencia por medios electrónicos](https://www.oas.org/es/cidh/expresion/docs/informes/MujeresPeriodistas.pdf) es una forma de discriminación de género que debe ser abordada por los Estados como parte de sus obligaciones bajo la Convención Americana de Derechos
Humanos.

El derecho de las mujeres y niñas a vivir libres de violencia en entornos digitales también ha sido reconocido por el Sistema de Naciones Unidas. Este derecho ha sido referenciado en resoluciones de la Asamblea General y en
informes del [Consejo de Derechos Humanos,](https://digitallibrary.un.org/record/3937534/files/A_HRC_RES_47_16-ES.pdf) lo que refuerza la necesidad de proteger a las mujeres y niñas en el ámbito digital como parte de los esfuerzos globales para erradicar la violencia degénero.

### Derecho a la libertad de expresión y acceso a la información

La violencia de género facilitada por la tecnología (VGFT) no solo constituye una violación a sus derechos fundamentales, sino que también limita su libertad de expresión, un derecho reconocido en el artículo 13 de la
[Convención Americana sobre Derechos Humanos](https://www.oas.org/dil/esp/1969_Convenci%C3%B3n_Americana_sobre_Derechos_Humanos.pdf)
(CADH) y el artículo 19 del [Pacto Internacional de Derechos Civiles y Políticos](https://www.ohchr.org/es/instruments-mechanisms/instruments/international-covenant-civil-and-political-rights) (PIDCP). Este derecho incluye la posibilidad de "buscar, recibir y difundir informaciones e ideas libremente en Internet sin censura u otro tipo de injerencia".

Internet se ha convertido en una herramienta clave para el desarrollo político, social y económico, permitiendo a las personas acceder a información, participar en procesos de decisión y ejercer sus derechos.
Además, facilita a las mujeres el acceso a recursos que les permiten tomar "[decisiones fundamentadas y autónomas en los asuntos relativos a su cuerpo, su vida y su salud, incluida la salud sexual y reproductiva y los derechos reproductivos](https://www.unodc.org/lpomex/noticias/marzo-2023/naciones-unidas-insta-a-eliminar-las-brechas-que-privan-a-mujeres-y-ninas-a-acceder-plenamente-a-la-era-digital.html)". Sin embargo, la violencia de género en línea impide que muchas mujeres ejerzan plenamente estos derechos, generando autocensura y reduciendo su participación en el espacio digital. La [RELE ha
enfatizado](https://www.oas.org/es/cidh/expresion/docs/publicaciones/INTERNET_2016_ESP.pdf) que
el acceso a Internet "constituye una condición sine qua non para el ejercicio efectivo de los derechos humanos hoy en día", incluyendo la libertad de expresión, opinión, asociación y educación. No obstante, la violencia en línea ha creado un "efecto inhibitorio en el ejercicio de la libertad de expresión",
limitando la presencia de voces femeninas en los medios y reforzando la
desigualdad de género. Esta situación tiene consecuencias graves no solo para
las mujeres afectadas, sino también para la sociedad en su conjunto. La
ausencia de mujeres en el debate público "socava la dimensión social del
derecho a la libertad de expresión", debilitando la deliberación democrática y
el buen gobierno. La [Relatora Especial de la ONU sobre la Violencia contra la Mujer](https://docs.un.org/es/A/HRC/38/L.6) ha advertido que esta situación genera "una sociedad en que las mujeres ya no se sienten seguras en línea o fuera de línea, debido a la impunidad generalizada de los autores de la
violencia de género".

La [VGFT también agrava la brecha digital de género](https://lac.unwomen.org/sites/default/files/2022-11/MUESTRA%20Informe%20Violencia%20en%20linea%202.1%20%282%29_Aprobado%20%28Abril%202022%29_0.pdf), ya que muchas mujeres abandonan el espacio en línea debido a la intimidación y el acoso. Esto no solo afecta su derecho a la información, sino que también impacta negativamente en su acceso a oportunidades económicas y en su alfabetización digital. Además, restringe su posibilidad de acceder a información sobre "[salud sexual y reproductiva, sin dejar de respetar la confidencialidad y eliminar los obstáculos relacionados con la estigmatización](https://documents.un.org/doc/undoc/gen/g17/111/86/pdf/g1711186.pdf?OpenElement)". Ciertos grupos de mujeres se ven particularmente afectados por esta situación, como las activistas, defensoras de derechos humanos, periodistas y políticas, quienes dependen en gran medida de las TIC para su trabajo. En este sentido, el [Alto Comisionado de las Naciones Unidas para los Derechos Humanos](https://undocs.org/es/A/HRC/32/38) ha enfatizado que los Estados "tienen la obligación de combatir la violencia contra la mujer en la red y de proteger la libertad de opinión y expresión". Para ello, es fundamental adoptar medidas efectivas que garanticen un entorno digital seguro e inclusivo para todas las mujeres.

### El derecho a la privacidad y a la protección de los datos personales

El derecho de las mujeres y niñas a la vida privada, reconocido en el artículo 11 de la CADH y el artículo 17 del PIDCP, también se ve amenazado por la violencia en línea. Prácticas como la vigilancia y el control de cuentas
personales, la difusión no consentida de imágenes íntimas y el ciberacoso afectan de manera diferenciada a las mujeres, limitando su autonomía y su seguridad en el entorno digital. [La Corte Interamericana de Derechos Humanos
ha señalado que](https://corteidh.or.cr/docs/casos/articulos/resumen_252_esp.pdf) "el concepto de vida privada es un término amplio no susceptible de definiciones exhaustivas, pero que comprende, entre otros ámbitos protegidos, la vida sexual y el derecho a establecer y desarrollar relaciones con otros seres humanos".

En este sentido, el derecho a la intimidad en Internet implica la posibilidad
de utilizar herramientas como "[el cifrado de datos, el anonimato o el uso de
seudónimos en las redes sociales con el fin de reducir al mínimo el riesgo de
injerencia en la vida privada](https://undocs.org/A/RES/68/167)". Esto es
particularmente relevante para defensoras de derechos humanos y mujeres que
buscan información considerada tabú en sus sociedades. [El anonimato y el
cifrado](https://rm.coe.int/16804c177e) "pueden contribuir al pleno disfrute
de los derechos de las personas, incluido el derecho a la libertad de opinión
y expresión y el derecho a la privacidad, de conformidad con el derecho
internacional". El [MESECVI ha advertido que el derecho a la privacidad de las
mujeres](https://undocs.org/A/RES/68/167) se encuentra bajo amenaza debido a
la digitalización masiva y al uso de tecnologías de vigilancia. Además, la
vigilancia y recopilación de datos personales afectan a las mujeres de manera
particular debido a la discriminación estructural. La falta de privacidad en
los espacios digitales reproduce la idea de que las mujeres deben ser
controladas, lo que amplifica su vulnerabilidad y exposición a la violencia en
línea.

### Derecho a la reunión y libertad de asociación

La VGFT también afecta el derecho de reunión y la libertad de asociación de
las mujeres, derechos protegidos por el artículo 4.h de la Convención de Belém
do Pará, los artículos 15 y 16 de la CADH y los artículos 21 y 22 del PIDCP.
Estos derechos deben garantizarse “con independencia de que se los ejerza
personalmente, con los medios técnicos contemporáneos o con los que se
inventen en el futuro”.

Las mujeres [tienen derecho a utilizar Internet para asociarse libremente y
desarrollar actividades con un fin lícito,](https://documents.un.org/doc/undoc/gen/g19/141/05/pdf/g1914105.pdf)
sin presiones o intromisiones que alteren su propósito. Esto incluye la posibilidad de elegir plataformas digitales para movilizarse, protestar y participar en debates sobre políticas públicas e iniciativas legislativas. Como destacó el Relator Especial de la ONU sobre el derecho a la libertad de
reunión y asociación en [su informe de 2012](https://docs.un.org/es/A/HRC/41/41), Internet y las redes sociales se han convertido en herramientas fundamentales para ejercer estos derechos, los cuales son esenciales para otros derechos civiles, culturales, económicos, políticos y sociales.

En el caso de mujeres y niñas, históricamente marginadas del espacio público, Internet ha sido clave para impulsar el movimiento feminista y denunciar la violencia de género. [Ejemplo de ello son los movimientos #MeToo, #NiUnaMenos, #MiPrimerAcoso y #PrimeiroAssédio](https://adc.org.ar/wp-content/uploads/2019/06/Latin-American-Report-on-Online-Gender-Violence-final_v2.pdf), que han logrado articular denuncias y combinar activismo en línea y fuera de línea.

Sin embargo, más [investigaciones](https://www.tedic.org/wp-content/uploads/2024/09/Violencia-de-genero-a-mujeres-politicas-WEB-1.pdf) en la región también ha expuesto a las mujeres a nuevas formas de VGFT como la difusión de imágenes sexualizadas, desinformación para dañar su reputación,
amenazas de violencia sexual y feminicidio, y _doxxing_. Estos ataques afectan
su capacidad de reunirse y construir comunidades virtuales, socavando su
visibilidad y participación en la vida pública. Como consecuencia, muchas
mujeres se ven obligadas a abandonar el espacio digital, lo que no solo afecta
su seguridad, sino que también limita su acceso a redes de apoyo y
asesoramiento. Ante esta realidad, los Estados deben adoptar medidas efectivas
para proteger a las mujeres y niñas contra la violencia de género en línea,
cumpliendo con su obligación de prevenir, investigar, sancionar y reparar
estos actos, garantizando así el ejercicio pleno de sus derechos a la libertad
de reunión y asociación en el entorno digital.

### Derecho a la integridad personal

La VGFT vulnera el derecho de las mujeres y niñas a la integridad física,
psíquica y moral, protegido por el artículo 4.b de la Convención de Belém do
Pará y el artículo 5 de la CADH. Estos derechos garantizan que las mujeres
sean tratadas con dignidad y no sean sometidas a torturas ni a penas o tratos
crueles, inhumanos o degradantes. Los efectos de la VGFT pueden generar graves
consecuencias a corto y largo plazo en las víctimas, afectando su desarrollo
individual. Más allá de los daños psicológicos, también se han documentado
repercusiones físicas derivadas de la violencia en línea. El derecho a la
integridad física puede verse comprometido cuando ataques como el _doxxing_
conducen a agresiones fuera de Internet o cuando la difusión no consentida de
imágenes íntimas provoca niveles de revictimización tan extremos que pueden
llevar al suicidio de la víctima.

El [MESECVI ha reconocido que](https://belemdopara.org/guia-practica-para-la-aplicacion-del-sistema-de-indicadores-de-progreso-para-la-medicion-de-la-implementacion-de-la-convencion-de-belem-do-paramesecvi-2014/) “la infracción del derecho a la integridad física y psíquica de las personas es una clase de violación que tiene diversas connotaciones de grado y que abarca desde la
tortura hasta otro tipo de vejámenes o tratos crueles, inhumanos o
degradantes”, cuyas consecuencias varían en intensidad dependiendo de
múltiples factores.

Ante esta realidad, es fundamental que los Estados reconozcan el impacto de la
violencia de género en línea y adopten medidas efectivas para prevenir,
sancionar y reparar estos actos, garantizando la protección de la integridad
física y psicológica de las mujeres y niñas.

### Otros derechos humanos

La VGFT también puede conllevar la vulneración de otros derechos
fundamentales, entre ellos:

#### El derecho a la libertad y seguridad personales

Reconocido en el artículo 4.c de la Convención de Belém do Pará. Los ataques
digitales contra mujeres y niñas afectan su capacidad de organizar su vida
personal y social conforme a sus propias creencias y decisiones. Según el
[CEVI, este derecho implica la facultad de cada individuo para
autodeterminarse y elegir libremente las circunstancias que dan sentido a su
existencia.](https://belemdopara.org/guia-practica-para-la-aplicacion-del-sistema-de-indicadores-de-progreso-para-la-medicion-de-la-implementacion-de-la-convencion-de-belem-do-paramesecvi-2014/)

#### El derecho al respeto a la honra y al reconocimiento de la dignidad

Establecido en el artículo 11 de la CADH y el artículo 17 del PIDCP. Esta protección busca evitar la difusión de información falsa o la publicación no consentida de imágenes y videos íntimos que puedan dañar la reputación de las
mujeres y limitar su participación social, reforzando estereotipos de género sobre la sexualidad.

#### Violaciones a los derechos sexuales y reproductivos y al derecho a la libertad sexual

En términos de autonomía sexual, en casos de difusión no consentida de imágenes íntimas. [Estos actos de violencia suelen generar una revictimización permanente por parte de la sociedad,](https://www.tedic.org/wp-content/uploads/2021/09/Imagen-no-consentida-Tedic-web.pdf) castigando a lasmujeres por el ejercicio libre de su sexualidad.

#### El derecho a la libre circulación

Previsto en el artículo 22 de la CADH y el artículo 12 del PIDCP. Este derecho
se ve afectado cuando una mujer es víctima de doxxing o recibe amenazas de
daño físico, muerte o violencia sexual, lo que la obliga a cambiar de
residencia o restringe su movilidad por miedo a represalias.

#### El derecho a ser protegida de discriminación laboral por razón de género

Conforme a los artículos 3 y 6 del Protocolo Adicional a la CADH y los
artículos 6 y 7 del Pacto Internacional de Derechos Económicos, Sociales y Culturales. [Esto ocurre cuando una víctima pierde](https://repositorio.redalas.net/sites/default/files/2021-02/La_violencia_de_genero_en_Mexico_y_las_t.pdf)[su empleo o una oportunidad laboral debido a ataques en línea que afectan su reputación, como la difusión de información falsa](https://repositorio.redalas.net/sites/default/files/2021-02/La_violencia_de_genero_en_Mexico_y_las_t.pdf).

#### El derecho al acceso a la justicia

Reconocido en los artículos 8 (garantías judiciales) y 25 (protección judicial) de la CADH. [La VGFT frecuentemente resulta en una negativa sistemática de justicia, omisiones en el debido proceso y obstáculos judiciales para identificar y sancionar a los responsables, impidiendo una reparación integral para las
víctimas.](https://www.apc.org/en/pubs/online-gender-based-violence-submission-association-progressive-communications-united-nations)

## Tensión entre derechos

El combate a la VGFT plantea tensiones entre derechos humanos, exacerbadas por
restricciones gubernamentales abusivas en Internet. Entre los derechos en
conflicto están la libertad de expresión, la privacidad y el derecho a una
vida libre de violencia. A menudo, la ponderación de estos derechos se realiza
sin una adecuada perspectiva de género, lo que lleva a priorizar algunos sobre
otros. Aunque los derechos humanos son interdependientes e indivisibles, su
ejercicio no es absoluto y puede estar sujeto a restricciones legales,
necesarias y proporcionales (CADH, UNESCO). En la práctica, la VGFT tiende a
ser considerada menos prioritaria en comparación con otros derechos. UNESCO [ha señalado la falta de enfoque de género](https://unesdoc.unesco.org/%0Aark:/48223/p%EE%80%BF0000246527) en
estos debates y la necesidad de validar las experiencias de las mujeres para
informar políticas más equitativas. Se ha evidenciado que la tecnología es
utilizada para controlar y silenciar a las mujeres, perpetuando la
discriminación de género tanto en línea como fuera de ella. Esto contribuye a
su marginalización y limita su acceso a los beneficios de las TIC.

El artículo 13.5 de la CADH prohíbe la apología del odio que incite a la violencia, lo que incluye el discurso de odio en línea dirigido contra las mujeres. El RELE [ha establecido que para penalizar dicho discurso, este debe ser público](https://www.oas.org/es/cidh/expresion/docs/publicaciones/MARCO%20JURIDICO%20INTERAMERICANO%20DEL%20DERECHO%20A%20LA%20LIBERTAD%20DE%20EXPRESION%20ESP%20FINAL%20portada.doc.pdf), representar un peligro real e inminente y demostrar intención de daño. Si bien
la libertad de expresión es fundamental en democracia, su ejercicio conlleva responsabilidades, permitiendo restricciones estrictamente definidas para prevenir discursos de odio y violencia.

El Sistema Interamericano de Protección de los Derechos Humanos ha
desarrollado un ["test tripartito" para evaluar la validez de limitaciones a
la libertad de expresión](https://www.oas.org/es/cidh/expresion/docs/publicaciones/internet_2016_esp.pdf):
estas deben estar claramente definidas en la ley, ser necesarias en una sociedad democrática y ser proporcionales al objetivo perseguido. Cualquier restricción debe ser ordenada por un juez competente y respetar el debido proceso ([RELE](https://www.oas.org/es/cidh/expresion/docs/publicaciones/MARCO%20JURIDICO%20INTERAMERICANO%20DEL%20DERECHO%20A%20LA%20LIBERTAD%20DE%20EXPRESION%20ESP%20FINAL%20portada.doc.pdf)).

En casos excepcionales, se admiten medidas de bloqueo y filtrado de contenidos
ilícitos, siempre que sean específicas, proporcionales y bajo supervisión
judicial. Deben contar con salvaguardas contra abusos, garantizar
transparencia y ser la única opción disponible para un objetivo legítimo. Los
Estados tienen la obligación de garantizar la igualdad de acceso a los
derechos en Internet (Convención de Belém do Pará, art. 8.g), lo que incluye
fomentar directrices de contenido que contribuyan a erradicar la violencia
contra mujeres y niñas. La falta de respuesta frente a la violencia en línea
legitima estas conductas y propicia impunidad. La Relatoría de la CIDH ha
instado a los Estados a prohibir discursos de odio que inciten a la violencia
y a garantizar un entorno digital seguro e inclusivo con perspectiva de
género. Para expresiones que no inciten directamente a la violencia, [se recomienda la aplicación de sanciones no penales, como reparaciones económicas o medidas alternativas.](https://www.eff.org/files/2015/06/23/manila_principles_1.0_es.pdf)
    `,
};
