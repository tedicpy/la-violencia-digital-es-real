export default [
  {
    'en': "is-internet-a-neutral-space",
    'es': "es-internet-un-espacio-neutro",
    'mainImage': "hero-b.png",
    'bgColor': "brand-indigo-3",
    'titleColor': 'text-white'
  },
  {
    'en': "types-of-gender-digital-violence",
    'es': "tipos-de-violencia-de-genero-digital",
    'mainImage': "hero-c.png",
    'bgColor': "brand-indigo-3",
    'titleColor': 'text-white'
  },
  {
    'en': "types-of-aggressors",
    'es': "tipos-de-perpetradores",
    'mainImage': "hero-d.png",
    'bgColor': "brand-indigo-3",
    'titleColor': 'text-white'
  },
  {
    'en': "digital-rights",
    'es': "derechos-digitales",
    'mainImage': "hero-e.png",
    'bgColor': "brand-indigo-3",
    'titleColor': 'text-white'
  },
  {
    'en': "effects-of-online-violence",
    'es': "efectos-de-la-violencia-en-linea",
    'mainImage': "hero-f.png",
    'bgColor': "brand-indigo-3",
    'titleColor': 'text-white'
  },
  {
    'en': "challenges-in-addressing-tfgbv",
    'es': "desafios-para-responder-a-la-vgft",
    'mainImage': "hero-g.png",
    'bgColor': "brand-indigo-3",
    'titleColor': 'text-white'
  },
];