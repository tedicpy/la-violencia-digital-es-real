export default {
  // en
  'title_en': "Challenges in addressing TFGBV",
  'content_en': `
To effectively combat online violence, a holistic approach is necessary. This involves addressing the psychosocial well-being of victims, enhancing reporting capabilities through awareness of national and international laws, and leveraging available response mechanisms.

Taking control of our digital information and processes is a subversive way to confront the systematic violence directed at women and marginalized groups.
  `, 

  // es
  'title_es': "Desafíos para responder a la VGFT",
  'content_es': `
Para combatir a la violencia en línea, debemos abordar el problema de forma integral: es importante pensar desde el bienestar psico-social de las personas que son víctimas de violencia, así como en la capacidad de denuncia a través del conocimiento de las leyes nacionales e internacionales y en los mecanismos de respuesta con los que contamos para hacerle frente.

Tomar control sobre nuestra información y nuestros procesos digitales, es la forma más subversiva para hacerle frente a la violencia sistemática dirigida hacia mujeres y colectivos.
`,
};
