export default [
  {
    'key': "normalizacion-de-la-violencia",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "baja-representacion-mujeres-tic",
    'bgColor': "brand-pink-3"
  },
  {
    'key': "impunidad",
    'bgColor': "brand-green-2"
  },
  {
    'key': "culpabilizacion-a-las-victimas",
    'bgColor': "brand-indigo-3"
  },
  {
    'key': "falta-de-mecanismos-de-denuncias-adecuados",
    'bgColor': "brand-pink-3"
  },
];