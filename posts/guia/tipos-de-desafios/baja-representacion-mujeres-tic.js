export default {
  'title_es': "Baja representación de mujeres en las TIC",
  'content_es': `Uno de los mayores desafíos es la escasa representación de mujeres en el sector tecnológico, lo que provoca la incorporación de desigualdades y prejuicios sistemáticos en las TIC. A esto se suma la falta de un diseño inclusivo, de una moderación adecuada de contenidos y de mecanismos eficaces para detectar abusos.`,
  'title_en': "Low representation of women in ICTs",
  'content_en': "One of the biggest challenges is the low representation of women in the technology sector, which leads to the incorporation  of inequalities and systematic biases in ICTs. Moreover, there is a lack of inclusive design, proper content moderation, and effective mechanisms for detecting abuse."
};