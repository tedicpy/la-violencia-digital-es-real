export default {
  'title_es': "Impunidad",
  'content_es': `Aunque en los últimos años se han implementado nuevas leyes y se ha resuelto un número creciente de casos, la respuesta de las autoridades sigue siendo insuficiente, dejando que esta forma de violencia permanezca en gran medida impune.`,
  'title_en': "Impunity",
  'content_en': "Although in recent years new laws have been implemented and more cases are being resolved, the authorities' response remains insufficient, allowing this form of violence to go largely unpunished."
};