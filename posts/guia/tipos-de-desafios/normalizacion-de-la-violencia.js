export default {
  'title_es': "Normalización de la violencia",
  'content_es': `Los medios de comunicación, las plataformas de internet, las autoridades y, en general, las comunidades tienden a trivializar y normalizar la violencia digital. Esta actitud contribuye a la invisibilización de este problema, legitimándolo y fomentando un entorno de impunidad que desestima las experiencias de las víctimas y las lleva al silencio.`,
  'title_en': "Normalization of violence",
  'content_en': "The media, internet platforms, authorities, and communities in general often trivialize and normalize digital violence. This attitude contributes to erasing the visibility of the issue, legitimizing it, and fostering an environment of impunity that dismisses victims' experiences and drives them into silence."
};