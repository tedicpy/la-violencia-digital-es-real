export default {
  'title_es': "Falta de mecanismos de denuncias adecuados",
  'content_es': `Entre otras dificultades, se observa la ausencia de mecanismos de denuncia efectivos, una insuficiencia de recursos técnicos y económicos para atender los casos, deficiencias importantes en la formación y sensibilización del personal encargado de la justicia, y la falta de mecanismos de reparación para las víctimas que superen las sanciones penales impuestas a los agresores.`,
  'title_en': "Lack of proper reporting mechanisms",
  'content_en': "Among other challenges, there is a noticeable absence of effective reporting mechanisms, insufficient technical and financial resources to handle cases, significant shortcomings in the training and awareness of justice personnel, and a lack of reparative mechanisms for victims that extend beyond the criminal sanctions imposed on the aggressors."
};