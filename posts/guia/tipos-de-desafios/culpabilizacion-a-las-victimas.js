export default {
  'title_es': "Culpabilización a las víctimas",
  'content_es': `Las mujeres que son víctimas de ciberviolencia suelen ser responsabilizadas por los actos de violencia que sufren y, con poca frecuencia, obtienen reconocimiento, apoyo o acceso a la justicia.`,
  'title_en': "Victim blaming",
  'content_en': "Women who are victims of cyber-violence are often blamed for the acts of violence they suffer and rarely obtain recognition, support or access to justice."
};