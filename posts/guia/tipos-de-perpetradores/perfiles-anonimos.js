export default {
  'title_es': "Perfiles anónimos",
  'content_es': `Se refieren a perfiles desconocidos en línea que ocultan la identidad real de la persona que las utiliza. Estos perfiles se crean sin revelar información personal identificable, como el nombre real, la dirección, el número de teléfono, etc. En su lugar, las personas usuarias pueden optar por utilizar seudónimos, apodos o simplemente permanecer completamente anónimos.
Este tipo de perfiles están protegidos por el [derecho de la libertad de expresión](https://www.derechosdigitales.org/10211/para-que-necesitamos-anonimato-y-por-que-es-importante-defenderlo/). Sin embargo, esto no justifica los ataques o acciones no ilegales.`,
  'title_en': "Anonymous profiles",
  'content_en': `Refers to unknown online profiles that hide the real identity of the person using them. These profiles are created without disclosing personally identifiable information, such as real name, address, phone number, etc. Instead, users can choose to use pseudonyms, nicknames or simply remain completely anonymous.

  Such profiles are protected by the [right to freedom of expression](https://www.derechosdigitales.org/10211/para-que-necesitamos-anonimato-y-por-que-es-importante-defenderlo/). However, this does not justify attacks or illegal actions.`
};