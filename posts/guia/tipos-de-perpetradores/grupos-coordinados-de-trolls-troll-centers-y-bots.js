export default {
'title_es': "Grupos coordinados de Trolls, troll centers y bots",
'content_es': `A los Trolls se los define como cuentas que realizan actividades en línea destinadas a causar daño, intimidación, o generar discordia a través de plataformas digitales y redes sociales. Estos grupos, a menudo organizados y coordinados, emplean tácticas como la difusión de desinformación, el acoso en línea, el ciberbullying, la manipulación de opiniones y la propagación de mensajes de odio.

[Un ejemplo](https://www.tedic.org/desinformacion-y-violencia-digital-el-caso-de-menchi-barriocanal/) que se puede ajustar a esta clasificación fueron los ataques coordinados contra la periodista Mercedes Barriocanal a su contacto de teléfono personal con discurso de odio. 

Los troll centers son especialmente preocupantes, ya que pueden estar respaldados por organizaciones con agendas específicas, como gobiernos, partidos políticos o grupos de interés, lo que les permite tener recursos y capacidades para llevar a cabo campañas de desinformación y manipulación a gran escala. [Un ejemplo](https://www.pagina12.com.ar/722285-un-atentado-digital-contra-una-editorial-especializada-en-de) documentado fue el caso de ataque en defensa de la dictadura militar contra las abuelas de la plaza de mayo en Argentina.

Los bots, cuentas anónimas automatizadas que son utilizadas para replicar contenidos y generar ruidos en las redes sociales para impactar en la ubicación del contenido. 

[Un ejemplo](https://theconversation.com/ser-mujer-politica-y-recibir-constantes-ataques-machistas-en-la-red-203816) que permite observar de manera aplicada los conceptos ilustrados más arrba es el caso de las primarias presidenciales de E.E.U.U. De 2020. Un estudio de Marvelous AI en 2019, que empleó análisis de datos, concluyó que cuentas consideradas de baja credibilidad, incluyendo bots y trolls, dirigieron más ataques hacia las candidatas en las primarias presidenciales demócratas de EE.UU. que hacia sus contrapartes masculinas. Este hallazgo subraya la persistente disparidad de género en la política y destaca la necesidad de abordar la violencia política dirigida específicamente hacia las mujeres en el ámbito electoral.`,

'title_en': "Coordinated groups of trolls, troll centers and bots",
'content_en': `Trolls are defined as accounts that engage in online activities intended to cause harm, intimidation, or generate discord through digital platforms and social networks. These groups, often organized and coordinated, employ tactics such as spreading misinformation, online harassment, cyberbullying, opinion manipulation, and propagating hate messages.

[An example](https://www.tedic.org/en/desinformation-and-digital-violence-the-case-of-menchi-barriocanal/) that fits this classification was the coordinated attacks with hate speech against journalist Mercedes Barriocanal on her personal phone contact. 

Troll centers are particularly concerning, as they can be backed by organizations with specific agendas, such as governments, political parties or interest groups, giving them the resources and abilities  to carry out large-scale disinformation and manipulation campaigns. [A documented example](https://www.pagina12.com.ar/722285-un-atentado-digital-contra-una-ditorial-especializada-en-de) is the case of the attack in defense of the military dictatorship and against the grandmothers of Plaza de Mayo in Argentina.

Bots are automated anonymous accounts that are used to replicate content and generate buzz on social media to impact the visibility and ranking of the content.

[An example](https://theconversation.com/ser-mujer-politica-y-recibir-constantes-ataques-machistas-en-la-red-203816) that illustrates the concepts outlined above is the case of the 2020 U.S. presidential primaries. A 2019 study conducted by Marvelous AI, using data analytics, concluded that accounts with low credibility —such as bots and trolls— directed more attacks toward female candidates in the U.S. Democratic presidential primaries than toward their male counterparts.This finding underscores the persistent gender disparity in politics and highlights the need to address political violence directed specifically toward women in the electoral arena.`
};