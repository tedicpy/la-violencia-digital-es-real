const colors = ["brand-indigo-3", "brand-pink-3", "brand-green-2"];

export default [
  {
    'key': "violencia-familiar-o-de-pareja",
    'bgColor': colors[0]
  },
  {
    'key': "autoridades-del-estado",
    'bgColor': colors[1]
  },
  {
    'key': "medios-de-comunicacion",
    'bgColor': colors[2]
  },
  {
    'key': "lideres-religiosos",
    'bgColor': colors[0]
  },
  {
    'key': "lideres-sindicales",
    'bgColor': colors[1]
  },
  {
    'key': "partidos-politicos",
    'bgColor': colors[2]
  },
  {
    'key': "opositores-politicos",
    'bgColor': colors[0]
  },
  {
    'key': "servicios-de-inteligencia",
    'bgColor': colors[1]
  },
  {
    'key': "miembros-de-las-fuerzas-armadas-militares",
    'bgColor': colors[2]
  },
  {
    'key': "cuerpos-policiales",
    'bgColor': colors[0]
  },
  {
    'key': "colegas-y-companeros-de-trabajo",
    'bgColor': colors[1]
  },
  {
    'key': "perfiles-de-militantes-antiderechos",
    'bgColor': colors[2]
  },
  {
    'key': "plataformas-de-redes-sociales-que-moderan-los-contenidos",
    'bgColor': colors[0]
  },
  {
    'key': "perfiles-anonimos",
    'bgColor': colors[1]
  },
  {
    'key': "grupos-coordinados-de-trolls-troll-centers-y-bots",
    'bgColor': colors[2]
  },
  {
    'key': "grupos-de-crimen-organizado-narcotrafico-guerrilla-y-paramilitares",
    'bgColor': colors[0]
  },
  {
    'key': "empresas-y-empresarios",
    'bgColor': colors[1]
  },
];