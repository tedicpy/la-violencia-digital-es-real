export default {
  // en
  'title_en': 'Is Internet a neutral space?',
  'content_en': `

In the beginning, the Internet was introduced as a neutral space where the possibility of diversification of voices and thoughts would contribute to equality. To speak of a neutral space implies that there are no biases or prejudices that influence or determine its functioning. Although the most basic characteristics of the Internet propose its openness, horizontality and independence, the reality is that this is often not the case. From a gender perspective, the Internet offers enormous potential for empowerment; however, it often ends up reinforcing pre-existing inequalities, since it was created without taking into account power relations and different contexts.

Consequently, [the same social problems that exist outside the digital sphere are reflected](https://cyborgfeminista.tedic.org/brecha-digital-de-genero-abordando-el-desafio-de-un-ecosistema-libre-de-violencia/): some of them are the practices of exclusion and violence that we can see in the gender gap.

#### What is the digital gender gap?

![Título de la imágen {image-left}](/blog-files/guia/blog-01.png)
It is a dichotomic concept that was first used in the 1990s to refer to the gap that appeared between countries, social groups and individuals who had access to digital technologies and those who did not. **([Selwyn, 2004](https://telearn.archives-ouvertes.fr/hal-00190497/document)).**

The digital gender gap refers to the differences between men and women in access to computer equipment and in the use of electronic devices and the Internet (ICT). As we all know, many "digital divides" hide behind this term. Divisions between countries, between urban and rural areas, between women and men, old and young, rich and poor, high and low educated, literate and illiterate. There are degrees of use that depend on where it can be accessed, through what devices, with what bandwidth, at what price.

Simplifying into a binary divide tends to flatten these disaggregations, encouraging approaches that underestimate the importance of exploring differences in people's Internet experience. To be effective, policies and business plans must consider different groups on their own terms, respond to different reasons for valuing the Internet and different barriers to achieving value, and explore the relationship between those different factors. **([APC, 2021](https://www.apc.org/en/blog/inside-digital-society-digital-divide-or-digital-continuum)).**

#### 1. Access to the internet:
According to [a publication by the International Telecommunications Union (ITU)](http://www.itu.int/es/mediacentre/paginas/2016-PR30.aspx) in 2020 the proportion of women using the Internet globally was 48% compared to 55% of men. However, this sample has some gaps, as it claims to measure all people who have used the Internet once or more in the last 3 months. **([ITU 2021](https://www.itu.int/en/ITU-D/Statistics/Documents/facts/FactsFigures2020.pdf)).**

These measurements do not include an analysis of the low percentage of women pursuing ICT-based careers, nor do they include an analysis of the working conditions on assembly lines in technology maquilas, nor do they include an analysis of the inequalities in their remuneration with respect to their male peers. **([Sequera & Gimenez. 2018](https://cyborgfeminista.tedic.org/brecha-digital-de-genero-abordando-el-desafio-de-un-ecosistema-libre-de-violencia/))**

For its part, [Intel Corporation](https://twitter.com/ONUMujeres/status/1416462717012127747?s=20) published research in 2021 that women in developing countries are 23% less likely than men to connect to the Internet..

This means that in the same territory, some people are widely connected, others are less connected, and still others are disconnected.  Therefore, the degree of connectivity and the quality of connection must be taken into account when measuring Internet access.

Public policy and business addressing connectivity, usage and impact must take into account the wants and connectivity needs of people in general, rather than focusing only on connecting the unconnected: on enabling those at all levels of engagement to achieve what they seek, while mitigating risks.

#### 2. The way we inhabit the Internet:

Of the people who have access, their use and way of transiting these spaces differs considerably in terms of gender. [In the research conducted in Colombia **(WebFoundation, 2015)**](http://webfoundation.org/2015/09/lets-make-sure-women-are-included-in-colombias-digital-future/), it is pointed out that most women who access the Internet use it for relationships and social activities. It is also evident that a very low percentage use it as a political tool to inform and be informed. This is mainly due to the imposed hierarchy of gender roles that divides the private from the public sphere, and establishes the exclusion of women and dissenting groups from participation and decision-making. The fact that a very low percentage of women use the Internet as a means of political influence shows that gender exclusion in digital spaces responds to the same problem that generates exclusion in offline spaces. In turn, the most active women on the Internet (bloggers, journalists and activists in general) are systematically attacked online. The attacks take the form of aggressions, threats and disqualifications that allude to gender prejudices and stereotypes, as well as reinforce historical violence against women, such as threats of rape. This constitutes online gender-based violence. Such attacks generate self-censorship or cancellation of their profiles in networks. **([Sequera & Gimenez. 2018](https://cyborgfeminista.tedic.org/brecha-digital-de-genero-abordando-el-desafio-de-un-ecosistema-libre-de-violencia/)).**

> Social inequalities are transferred to the digital spheres and in turn, violence is transferred offline. The aggressions that manifest themselves in the virtual world have a direct effect on the body and mind of those who experience them.

Thus, although the Internet is presented as a neutral space, it is not, since it is traversed by power asymmetries and political, cultural and historical moments, which subject it to the contexts in which they are produced. This lack of neutrality dates back to the moment of its creation, since hegemonic groups (cis, white, heterosexual men, belonging to high or middle social classes) are generally the ones who produce technologies. As women and minority communities are not part of the creation and production processes, inequality manifests itself not only in how we use technology but also in how technologies and the Internet operate and in what ways they are made available to society.

> The regional report submitted to the Special Rapporteur on violence against women at the United Nations, provides a regional diagnosis of the situation of gender violence in the online environment, as a "continuum" of aggressions experienced by women in physical spaces -streets, universities, homes- which become more complex and amplified through the use of technology.
  
  
  `,

  // es
  'title_es': '¿Es Internet un espacio neutro?',
  'content_es': `
En sus inicios Internet se presentó como un espacio neutro donde la posibilidad de la diversificación de voces y pensamientos contribuiría a la igualdad. Hablar de un espacio neutro implica que no existen sesgos o prejuicios que influencien o determinen su funcionamiento. Si bien las características más básicas de Internet proponen la apertura, horizontalidad e independencia de la misma, la realidad es que muchas veces eso no se cumple. Desde una perspectiva de género, Internet ofrece una enorme potencialidad de empoderamiento, sin embargo muchas veces termina reforzando desigualdades pre existentes, en tanto fue creada sin tener en cuenta las relaciones de poder y los distintos contextos.

En consecuencia [se reflejan los mismos problemas sociales que existen fuera del ámbito digital](https://cyborgfeminista.tedic.org/brecha-digital-de-genero-abordando-el-desafio-de-un-ecosistema-libre-de-violencia/): algunos de ellos son las prácticas de exclusión y violencia que podemos apreciar en la brecha de género. 

#### ¿Qué es la brecha digital de género?

![Título de la imágen {image-left}](/blog-files/guia/blog-01.png)
Es un concepto dicotómico que se utilizó por primera vez en los años 90 para hacer referencia a la brecha que se estaba creando entre los países, los grupos sociales y las personas que tenían acceso a las tecnologías digitales y los que no lo tenían **([Selwyn, 2004](https://telearn.archives-ouvertes.fr/hal-00190497/document)).**

La brecha digital de género se refiere a las diferencias entre hombres y mujeres en el acceso a equipos informáticos y en el uso de dispositivos electrónicos e Internet (TIC). Como todos sabemos, detrás de este término se esconden muchas "brechas digitales". Divisiones entre países, entre zonas urbanas y rurales, entre mujeres y hombres, viejos y jóvenes, ricos y pobres, con alto y bajo nivel educativo, alfabetizados y analfabetos. Hay grados de uso que dependen de dónde se pueda acceder, a través de qué dispositivos, con qué ancho de banda, a qué precio.

La simplificación en una división binaria tiende a aplanar esas desagregaciones, fomentando enfoques que subestiman la importancia de explorar las diferencias en la experiencia de Internet de las distintas personas. Para ser eficaces, las políticas y los planes de negocio deben considerar a los diferentes grupos en sus propios términos, responder a las diferentes razones para valorar Internet y a las diferentes barreras para conseguir el valor, y explorar la relación entre esos diferentes factores. **([APC, 2021](https://www.apc.org/en/blog/inside-digital-society-digital-divide-or-digital-continuum)).**

#### 1. El acceso a Internet:

Según [una publicación de la Unión Internacional Telecomunicaciones (ITU)](http://www.itu.int/es/mediacentre/paginas/2016-PR30.aspx) en 2020 la proporción de mujeres que utilizaron Internet de manera global fue de 48% frente a 55% de los hombres. Sin embargo esta muestra tiene algunos huecos, ya que pretende medir a todas las personas que han utilizado una vez o más en los últimos 3 meses **([ITU 2021](https://www.itu.int/en/ITU-D/Statistics/Documents/facts/FactsFigures2020.pdf)).**

Estas mediciones no incluyen un análisis sobre el bajo porcentaje de mujeres que realizan carreras basadas en TIC, así como tampoco sobre las condiciones de trabajo en líneas de ensamblado en maquilas tecnológicas, y tampoco sobre las desigualdades de sus remuneraciones con respecto a sus pares hombres **([Sequera & Gimenez. 2018](https://cyborgfeminista.tedic.org/brecha-digital-de-genero-abordando-el-desafio-de-un-ecosistema-libre-de-violencia/))**.

Por su parte [Intel Corporation](https://twitter.com/ONUMujeres/status/1416462717012127747?s=20) publicó una investigación en 2021 que las mujeres en países de desarrollo tienen 23% menos posibilidades que los hombres de conectarse a Internet. Eso quiere decir que en un mismo territorio, algunas personas están muy conectadas, otras están menos conectadas y otras están desconectadas.  Por tanto, ell grado de conectividad y la calidad de conexión deben tenerse en cuenta para medir el acceso a Internet.

Las políticas públicas y el sector empresarial que se ocupan de la conectividad, el uso y el impacto deben tener en cuenta los deseos y las necesidades de conectividad de las personas en general, en lugar de centrarse sólo en conectar a los que están desconectados: en permitir que los que están en todos los niveles de compromiso logren lo que buscan, al tiempo que mitigan los riesgos.

#### 2. La forma en que habitamos Internet:

De las personas que acceden, su uso y forma de transitar estos espacios difiere considerablemente en términos de género. En la investigación realizada en Colombia [**(WebFoundation, 2015)**](http://webfoundation.org/2015/09/lets-make-sure-women-are-included-in-colombias-digital-future/), se señala que la mayoría de las mujeres que acceden a Internet la utilizan para relaciones y actividades sociales. Además se evidencia que un porcentaje muy bajo la utiliza como herramienta política para informar y ser informada. Esto se debe principalmente a la jerarquía impuesta de roles de género que divide el ámbito privado de lo público, y establece la exclusión de las mujeres y grupos disientes de la participación y toma de decisiones. El hecho de que sea muy bajo el porcentaje que utiliza Internet como medio de incidir políticamente, demuestra que la exclusión de género en espacios digitales responde a la misma problemática que genera exclusión en los espacios fuera de línea. A su vez, las mujeres más activas en Internet (blogueras, periodistas y activistas en general) sufren ataques en línea de manera sistemática. Los ataques se manifiestan en forma de agresiones, amenazas y descalificativos que aluden a prejuicios y estereotipos de género, así mismo refuerzan violencias históricas hacia las mujeres, como por ejemplo amenazas de violación. Ello configura violencia de género en línea. Dichos ataques generan autocensura o cancelación de sus perfiles en redes **([Sequera & Gimenez. 2018](https://cyborgfeminista.tedic.org/brecha-digital-de-genero-abordando-el-desafio-de-un-ecosistema-libre-de-violencia/)).**

> Las desigualdades sociales se trasladan a las esferas digitales y a su vez la violencia se traslada fuera de la red. 
> Las agresiones que se manifiestan en el mundo virtual tienen un efecto directo sobre el cuerpo y la mente de quienes la viven.

Entonces, a pesar que Internet se presente como un espacio neutro, no lo es en tanto está atravesado por asimetrías de poder y momentos políticos, culturales e históricos, que lo sujetan a contextos donde se producen. Esta falta de neutralidad se remonta al momento de su creación ya que quien produce generalmente las tecnologías son grupos hegemónicos (hombres cis, blancos, heterosexuales, pertenecientes a clases sociales altas o medias). Al no formar parte las mujeres ni las comunidades minoritarias en los procesos de creación y producción, la desigualdad se manifiesta no solamente en cómo usamos la tecnología sino también en cómo operan las tecnologías e Internet y de qué maneras son puestas a disposición de la sociedad.

> El reporte regional entregado a la relatora especial sobre la violencia contra 
> la mujer en la Organización de las Naciones Unidas, arroja un diagnóstico 
> regional de la situación de violencia de género en el entorno en línea, 
> como un “continuum” de las agresiones que viven las mujeres en espacios físicos
> –calles, universidades, casa– y que se complejizan y se amplían a través del uso de la tecnología. 
`,
}