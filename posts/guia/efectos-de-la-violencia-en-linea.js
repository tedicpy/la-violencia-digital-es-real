export default {
  // en
  'title_en': "Effects of online violence",
  'content_en': `
Online gender-based violence can have various effects both personally and socially. At a personal level, it affects the life of the person who suffers the violence, both physically and mentally; at a social level, it can cause women to censor themselves and refrain from speaking freely. As a consequence, there is a restriction on the ability of women to be present and take part in the various online activism movements and communities. In other words, these situations limit the degree of participation of women in debates of public interest or decision-making processes, and perpetuate the way in which digital citizenship spaces were built: [based on the exclusion of women and other minority groups.](https://hiperderecho.org/2019/02/discursos-de-odio-y-violencia-de-genero-en-internet/#more-5975)

There are several dimensions from which to analyze the consequences and impact of digital violence on the victims:

#### Physical impact

- Sweating.
- Pain in different parts of the body (head, back, stomach).
- Loss of/increased appetite.
- Tension.
- Crying.

#### Emotional impact

- Stress.
- Anxiety.
- Rage.
- Anger.
- Fear.
- Helplessness.
- Frustration.
- Depression.
- Paranoia.
- Tiredness and confusion.

#### Diverse impacts

- Fear of going out and exposing oneself.
- Self-limitation of mobility.
- Abandoning the use of technologies.
- Self-censorship.
- Feeling of being constantly monitored and surveilled.

> GENDER VIOLENCE FACILITATED BY TECHNOLOGY IS REAL
> WHETHER ONLINE OR OFFLINE
> THE VIOLENCE IS ONE
> THE IMPACT IS THE SAME
  `,

  // es
  'title_es': "Efectos de la violencia en línea",
  'content_es': `
La violencia de género en línea puede producir diversos efectos tanto a nivel personal como social. Personal en cuanto a las afectaciones físicas y psíquicas que inciden en la vida de la persona que padece la violencia; y social en cuanto puede provocar que las mujeres se autocensuren y se abstengan de hablar libremente. Como consecuencia, hay una restricción de la capacidad de presencia y de ser parte de los diversos movimientos y comunidades de activismo. En otras palabras, estas situaciones limitan el grado de participación de las mujeres en debates de interés público, proceso de toma de decisiones, y perpetúa la manera en la que se construyen los espacios de ciudadanía digital: [en base a la exclusión de las mujeres y otros grupos minoritarios](https://hiperderecho.org/2019/02/discursos-de-odio-y-violencia-de-genero-en-internet/#more-5975).
Existen varias dimensiones para analizar las consecuencias e impactos para las víctimas de violencia digital:

#### Impacto físico

- Sudoración.
- Dolor de distintas partes del cuerpo (cabeza, espalda, estómago).
- Pérdida o exceso de apetito.
- Tensión.
- Llanto.
- Angustia.

#### Impacto emocional

- Estrés.
- Angustia.
- Ira.
- Enojo.
- Miedo.
- Impotencia.
- Frustración.
- Depresión.
- Paranoia .
- Cansancio.
- Confusión.

#### Impactos varios

- Temor a salir y exponerse.
- Auto-limitación de movilidad.
- Abandono de uso de las tecnologías.
- Autocensura.
- Sensación de constante monitoreo y vigilancia.

> LA VIOLENCIA DE GÉNERO FACILITADA POR LA TECNOLOGÍA ES REAL
> SEA EN LÍNEA O FUERA DE LÍNEA
> LA VIOLENCIA ES UNA
> EL IMPACTO EL MISMO
  `,
};
