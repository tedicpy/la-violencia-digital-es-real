import React from 'react';
// props = {title}
export default class CollapsableLarge extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isCollapsed: true };
    };

    toggleCollapsed () {
        this.setState({
            isCollapsed: !this.state.isCollapsed
        })
    }

    render() {
        return (
            <div className="collapsable text-white my-10 w-full">
                <div className="collapse-title border border-r-0 border-l-0 border-brand-indigo-2 py-3 cursor-pointer" onClick={this.toggleCollapsed.bind(this)}>
                    <button className="btn-text text-lg font-semibold text-left float-left w-4/5" >
                        {this.props.title}
                    </button>
                    <div className="float-right w-1/5 text-right">
                        <div className="icon font-bold">
                            {this.state.isCollapsed ? '+' : '-'}
                        </div>
                    </div>
                    <div className="clear-both"></div>
                </div>
                {!this.state.isCollapsed ? (
                    <div className="collapse-content font-regular mt-5">
                        {this.props.children}
                    </div>
                ) : ''}
            </div>
        )
    }
}