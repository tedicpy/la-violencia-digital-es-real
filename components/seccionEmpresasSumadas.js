import React from "react";
import Link from 'next/link'
import useTranslation from '../lib/translations/useTranslation.hook'

export default function SeccionEmpresasSumadas( {empresasSumadasContent, pagesContentIndex}) {
  const { t, locale } = useTranslation()

  const Btn = ({ className }) => (
    <Link href={`${locale}/b/paginas/${pagesContentIndex[1][locale]}`}>
      <a className={`inline-block w-32 px-4 py-2.5 mt-5 text-base text-center font-medium text-white bg-brand-pink-3 rounded-lg mr-5 hover:bg-brand-green-2 active:bg-brand-pink-1 ${className}`}>
        {t('common.conoce-mas')}
      </a>
    </Link>
  )

  return (
    <section className="relative">
      {/* bg effect */}
      <div className="absolute bottom-0 -mb-5 md:-mb-14 w-full">
        <svg className={`header-bg-svg text-brand-red-4 w-full fill-current`} viewBox="0 0 611 88.7"><polygon className="cls-1" points="0 0 0 71.05 305.5 88.7 611 71.05 611 0 0 0"/></svg>
      </div>

      <div className="relative pb-10 pt-12 md:pb-0 md:pt-24 lg:pb-5 lg:pt-32 px-16 lg:px-28 xl:px-56 bg-brand-red-4">
        <div className="grid grid-cols-1 md:grid-cols-2">
          <div className='text-2xl mx-1 pb-5 text-center md:text-left md:mx-6 lg:mt-10'>
            <h3 className="uppercase md:text-2xl lg:text-3xl">
              <span>{t('common.empresas-de-moda')}</span>
            </h3>
            <Btn className="hidden md:block w-40 md:mt-14 md:text-lg lg:w-48 lg:text-xl"></Btn>
          </div>
          <div className="p-0 md:pl-18 text-center">
            <div className="grid grid-cols-2 gap-6 lg:gap-10 justify-items-center items-center m-6">
              {empresasSumadasContent.map(({ imageFile, imageClass }, index) => (
                <img src={`/blog-files/empresas-sumadas/${imageFile}`} key={index} className={`${imageClass}`}/>
              ))}
            </div>
            <Btn className="md:hidden"></Btn>
          </div>
        </div>
      </div>
    </section>
  );
}