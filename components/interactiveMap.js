import React, { useEffect, createRef, useState } from 'react'
import VectorMap from './vectorMap'
import Paraguay from '../public/paraguay.json'

export default function InteractiveMap ({ className, formatFloatLabel }) {
  const [floatLabels, setFloatLabels] = useState(Paraguay.layers)
  const [dimensions, setDimensions] = useState({})
  const mapRef = createRef(null)
  
  Paraguay.layers = Paraguay.layers.map((layer) => {
    return { reference: createRef(null), ...layer}
  })
  
  useEffect(() => {
    const handleResize = function handleResize() {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth
      })
    }
    window.addEventListener("resize", handleResize)
    return _ => {
      window.removeEventListener("resize", handleResize)
    }
  })
  
  const recalcFloatLabels = () => {
    const newFloatLabels = Paraguay.layers.map(layer => {
      if (layer.reference && layer.reference.current) {
        const { top, left } = layer.reference.current.getBoundingClientRect()
        return {
          ...layer,
          position: {
            top: top + window.scrollY,
            left: left + window.scrollX  
          },
        }
      }
    })
    setFloatLabels(newFloatLabels)
  }

  useEffect(() => {
    recalcFloatLabels()
  }, [dimensions])

  return (
    <div className={`interactive-map ${className}`}>
      <VectorMap reference={mapRef} {...Paraguay} />
      {/* float labels */}
      {floatLabels.map((label, index) => label && formatFloatLabel(label))}
    </div>
  )
}