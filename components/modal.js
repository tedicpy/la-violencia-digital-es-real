export default function Modal ({
  open,
  setOpen=null,
  onClose=null,
  children,
  modalClass='',
  modalStyle={},
  mainStyle={},
  closeIconClass=''
}) {
  const close = () => {
    setOpen && setOpen(false); 
    onClose && onClose();
  }

  return (
    <div className={`${open ? ' visible' : ' invisible'} modal z-50 absolute w-full h-full top-0 left-0 flex justify-center`} style={mainStyle}>
      {/* backdrop */}
      {open ? (
        // la clase fixed anula onClick
        <button onClick={close} className="modal-overlay fixed w-full h-full bg-brand-indigo-2 opacity-90 top-0 left-0 cursor-pointer"></button>
      ) : ''}
      {/* container */}
      <div className="fixed w-full h-full overflow-y-scroll top-0 left-0" onClick={close}>
        {/* modal */}
        <div className={`relative top-0 left-0 w-4/5 mx-auto mb-48 bg-white rounded-lg shadow-lg justify-center text-2xl ${modalClass}`} style={modalStyle} onClick={(e) => e.stopPropagation()}>
          <button className="absolute right-0 top-3 w-8 h-10" onClick={close}>
            <svg className={`fill-current w-4 h-4 ${closeIconClass}`} viewBox="0 0 13 13">
              <g transform="translate(-277.000000, -234.000000)">
                <path d="M289.746133,234.253815 C289.407633,233.915395 288.858902,233.915395 288.520402,234.253815 L283.499624,239.274615 L278.478846,234.253815 C278.14025,233.916037 277.592044,233.916369 277.253857,234.254557 C276.915671,234.592745 276.915339,235.140953 277.253115,235.47955 L282.273895,240.500349 L277.253115,245.521148 C276.915339,245.859745 276.915671,246.407954 277.253857,246.746142 C277.592044,247.08433 278.14025,247.084662 278.478846,246.746884 L283.499624,241.726083 L288.520402,246.746884 C288.858998,247.084662 289.407204,247.08433 289.745391,246.746142 C290.083577,246.407954 290.083909,245.859745 289.746133,245.521148 L284.725353,240.500349 L289.746133,235.47955 C289.908681,235.317011 290,235.096555 290,234.866683 C290,234.636811 289.908681,234.416354 289.746133,234.253815 L289.746133,234.253815 Z" id="Path"></path>
              </g>
            </svg>
          </button>

          {children}
        </div>
      </div>
    </div>
  )
}