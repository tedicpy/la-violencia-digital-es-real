import { useContext, useEffect } from 'react'
import Head from 'next/head'
import Navbar from './navbar'
import { LayoutContext } from './layout.context'
import BackToTop from './backToTop'
import config from '../config'
import { useMatomo } from '@datapunt/matomo-tracker-react'


export default function Layout({ 
  children, 
  home, 
  title, image, 
  withPadding, withPaddingY, fullMenuHeader,
  localedPathMap,
  postCategoryItems
}) {
  const pageTitle = title ? `${title} | ${config.siteTitle}` : config.siteTitle
  const { contentRef, isNavbarOpen, setIsNavbarOpen } = useContext(LayoutContext)
  const { trackPageView } = useMatomo()

  // Track page view
  useEffect(() => {
    trackPageView()
  }, [])

  let mainClasses = 'overflow-x-hidden w-sceen'
  if (withPaddingY) {
    mainClasses += ' pt-24 pb-20'
  }

  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>{pageTitle}</title>
        <meta
          name="description"
          content={pageTitle}
        />
        <meta
          property="og:image"
          content={image}
        />
        <meta name="og:title" content={pageTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <div id="top"></div>
      <Navbar home={home} fullMenuHeader={fullMenuHeader} localedPathMap={localedPathMap} postCategoryItems={postCategoryItems}></Navbar>
      <BackToTop></BackToTop>
      <button className={`fixed z-4000 w-full h-screen bg-black opacity-50 lg:hidden ${isNavbarOpen ? 'transition-opacity-full' : 'transition-opacity-none'}`} onClick={() => {setIsNavbarOpen(false)}} type="button"></button>
      <main className={mainClasses} ref={contentRef}>{children}</main>
      
      <style global jsx>{``}</style>
    </>
  )
}