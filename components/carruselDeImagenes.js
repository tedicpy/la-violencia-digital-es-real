import React from "react";

export default function CarruselDeImagenes() {
  const carruselData = {
    title: 'Morena Toro',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam maximus tempus arcu',
  }
  return (
    <section>
        <h1 className="text-4xl">{carruselData.title}</h1>
        <p>{carruselData.description}</p>
        <div className="text-4xl">---Carrusel de Imagenes---</div>
    </section>
  );
}