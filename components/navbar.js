import { Fragment, useContext, useEffect, useState } from 'react'
import Link from 'next/link'
import { Link as LinkScroll } from 'react-scroll'
import ActiveLink from './activeLink'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'
import filter from 'lodash.filter'
import LocaleSwitcher from '../components/localeSwitcher'

export default function NavbarHome({ home, fullMenuHeader, localedPathMap, postCategoryItems }) {
  const { isNavbarOpen, isScrollUnderOffset, setIsNavbarOpen, setScrollOnInit } = useContext(LayoutContext)
  const [ isShowSubMenu, setShowSubMenu ] = useState(false);
  const [timeoutId, setTimeoutId] = useState(null);
  const { t, locale } = useTranslation()

  const showFullHeader = () => fullMenuHeader || isNavbarOpen || isScrollUnderOffset

  const classes = {
    background: 'bg-brand-indigo-2',
    toggleBtn: 'text-brand-pink-3',
    toggleBtnHover: 'text-brand-pink-2',
    sideMenuMovile: 'bg-brand-indigo-2 opacity-90',
    link: '-mx-4 px-5 py-1 text-brand-pink-5 font-normal hover:bg-brand-indigo-1 hover:text-brand-green-4',
    sublink: 'px-5 pl-10 lg:px-4 py-1 text-brand-pink-5 font-normal hover:bg-brand-indigo-1 hover:text-brand-green-4'
  }

  const pagesInMenu = [{
    postDefaultPath: 'violencia-de-genero-en-paraguay',
    name: t(`common.menu.violencia-digital`),
  }, {
    postDefaultPath: 'caso-belen',
    url: `/${locale}/b/litigios-estrategicos/${postCategoryItems && postCategoryItems.litigiosEstrategicosContentIndex[0][locale]}/`,
    name: t(`common.menu.caso-belen`),
  }, {
    postDefaultPath: 'en-paraguay',
    url: `/${locale}/b/paginas/${postCategoryItems && postCategoryItems.pagesContentIndex[0][locale]}/`,
    name: t(`common.menu.en-paraguay`),
  }]

  const queEsLink = postCategoryItems && filter(postCategoryItems.pagesContentIndex, { es: 'violencia-de-genero' })[0][locale]

  const menuItems = (
    <div className="px-4 w-full font-semibold text-sm">
      <div className="lg:float-left lg:w-4/5 lg:p-3">
        <div className="lg:float-left">
          {home ? (
            <LinkScroll to="header" smooth={true} offset={0} duration={800} href="" className={`block uppercase lg:mx-2 mt-1 lg:inline lg:mt-0 ${classes.link} font-regular`}>
              {t('common.menu.inicio')}
            </LinkScroll>
          ) : (
            <ActiveLink href={`/`} activeClass="active" scroll={false} key="homebtn-out">
              <a onClick={() => setScrollOnInit('header')} className={`block uppercase lg:mx-2 lg:inline lg:mt-0 ${classes.link}`}>
                {t('common.menu.inicio')}
              </a>
            </ActiveLink>
          )}
        </div>

        {pagesInMenu.map(({ postDefaultPath, url, name }, index) => (
          <Fragment key={index}>
            {postDefaultPath === 'violencia-de-genero-en-paraguay' ? (
              <div
                className="lg:float-left"
                onMouseEnter={() => {
                  clearTimeout(timeoutId);
                  setTimeoutId(null); // Clear any existing timeout
                  setShowSubMenu(true);
                }}
                onMouseLeave={() => {
                  const newTimeoutId = setTimeout(() => {
                    setShowSubMenu(false);
                  }, 300); // Delay closing by 300ms
                  setTimeoutId(newTimeoutId); // Store the timeout ID in state
                }}
              >
                {/* violencia de genero menu item */}
                <a
                  onClick={() => setShowSubMenu(!isShowSubMenu)}
                  className={`cursor-pointer block uppercase lg:mx-2 mt-1 lg:inline lg:mt-0 lg:ml-2 ${classes.link}`}
                >
                  {name}
                </a>
                {/* violencia de genero sub menu */}
                {isShowSubMenu ? (<>
                  <div className={`z-5000 lg:absolute top-10 lg:w-80 ${classes.background} rounded-xl rounded-tl-none lg:mt-5 lg:ml-2 -mx-4 lg:py-2`}>
                    <ActiveLink href={`/${locale}/b/paginas/${queEsLink}/`} scroll={false} activeClass="active">
                      <a onClick={() => {setScrollOnInit('header'); setShowSubMenu(false)}} className={`block lg:mt-0 ${classes.sublink}`}>
                        {t(`common.menu.que-es-violencia-genero`)}
                      </a>
                    </ActiveLink>
                    {postCategoryItems && postCategoryItems.guiaContentIndex && postCategoryItems.guiaContentIndex.map((post, innerIndex) => (
                      <>
                        <ActiveLink href={`/${locale}/b/guia/${post[locale]}/`} scroll={false} activeClass="active" key={innerIndex}>
                          <a onClick={() => {setScrollOnInit('header'); setShowSubMenu(false)}} className={`block lg:mt-0 ${classes.sublink}`}>
                            {post.title}
                          </a>
                        </ActiveLink>
                      </>
                    ))}
                  </div>
                  <button className="test-class" onClick={() => {setShowSubMenu(false)}} type="button"></button>
                </>) : ''}
              </div>
            ) : (
              // otros enlaces
              <ActiveLink href={url} scroll={false} activeClass="active">
                <a onClick={() => setScrollOnInit('header')} className={`block uppercase lg:mx-2 mt-1 lg:inline lg:mt-0 lg:ml-2 ${classes.link}`}>
                  {name}
                </a>
              </ActiveLink>
            )}
          </Fragment>
        ))}
      </div>
      
      <div className="lg:float-right">
        <LocaleSwitcher
          selectClassName="py-3 pl-2 -mx-2 bg-brand-indigo-2 text-white"
          optionClassName={classes.link}
          localedPathMap={localedPathMap}
        ></LocaleSwitcher>
      </div>

      <div className="clear-both"></div>
    </div>
  )
  
  return (
    <>
      <header id="header-menu" className={`header-menu z-5000 h-24 lg:h-12 px-4 py-1 w-full fixed top-0 left-0 ${showFullHeader() ? `shadow-lg ${classes.background}` : ''}`}>
        <div className="w-full px-2 py-2 lg:hidden">
          {/* navbar header */}
          <div className={`float-left z-5000 lg:hidden ${showFullHeader() ? 'transition-opacity-full' : 'transition-opacity-none'}`}>
            {/* logo link */}
            <Link href={`/`} scroll={false}>
              <a onClick={() => setScrollOnInit('header')} className="flex flex-row mx-auto place-items-center" title={''}>
                <div className="flex w-12 mr-2">
                  <img src="/assets/hero-portada.png" />
                </div>
                <div className="flex w-32">
                  <img src={`/assets/logo-text-white_${locale}.svg`} />
                </div>
              </a>
            </Link>
          </div>
          
          {/* navbar movile trigger */}
          <div className="lg:hidden w-1/5 float-right text-right">
            <button onClick={() => setIsNavbarOpen(!isNavbarOpen)} className="inline-block p-2 pt-5 w-12 text-center text-gray-500 focus:text-gray-700 focus:outline-none" type="button">
              <div className="w-full mx-auto h-auto sm:w-4">
                {isNavbarOpen && (
                  <svg className={`fill-current ${classes.toggleBtn} w-auto h-6 mx-auto`} viewBox="0 0 36.44 130.11">
                    <g id="Capa_2" data-name="Capa 2"><g id="Capa_1-2" data-name="Capa 1"><circle className="cls-1" cx="18.22" cy="18.22" r="18.22"/><circle className="cls-1" cx="18.22" cy="65.06" r="18.22"/><circle className="cls-1" cx="18.22" cy="111.89" r="18.22"/></g></g>
                  </svg>
                )}
                {!isNavbarOpen && (
                  <svg className={`fill-current ${classes.toggleBtnHover} w-auto h-6 mx-auto`} viewBox="0 0 36.44 130.11">
                    <g id="Capa_2" data-name="Capa 2"><g id="Capa_1-2" data-name="Capa 1"><circle className="cls-1" cx="18.22" cy="18.22" r="18.22"/><circle className="cls-1" cx="18.22" cy="65.06" r="18.22"/><circle className="cls-1" cx="18.22" cy="111.89" r="18.22"/></g></g>
                  </svg>
                )}
              </div>
            </button>
          </div>

          <div className="clear-both"></div>
        </div>

        {/* navbar content desktop */}
        <div className={`hidden lg:flex relative w-4/5 mx-auto rounded-full ${(!isScrollUnderOffset && !isNavbarOpen) ? '' : ''}`}>
          {menuItems}
        </div>
      </header>

      {/* navbar content mobile */}
      <div className={`z-4500 fixed top-0 left-0 w-4/5 sm:w-3/4 md:w-3/5 h-screen ${classes.sideMenuMovile} overflow-y-auto pt-28 pb-4 sm:pt-28 lg:hidden ${isNavbarOpen ? 'block visible' : 'hidden invisible'}`}>        <div className="mb-6">
          {menuItems}
        </div>
      </div>
    </>
  )
}