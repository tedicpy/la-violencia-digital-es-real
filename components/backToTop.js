import { useContext } from 'react'
import { Link as LinkScroll } from 'react-scroll'
import {
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar"
import "react-circular-progressbar/dist/styles.css"
import { LayoutContext } from './layout.context'

export default function BackToTop() {
  const { isScrollUnderOffset, scrollPosition } = useContext(LayoutContext)
  const getScrollPercentage = () => {
    if (process.browser) {
      return (scrollPosition + window.innerHeight) * 100 / document.documentElement.offsetHeight
    }
  }
  return (
    <LinkScroll to="header" smooth={true} offset={0} duration={800}>
      <button type="button" className={`fixed p-0.5 bg-white rounded-full w-14 h-14 bottom-7 right-7 shadow-xl ${isScrollUnderOffset ? 'transition-opacity-90' : 'transition-opacity-none'}`} style={{zIndex: 500}}>
        <CircularProgressbarWithChildren value={getScrollPercentage()} styles={buildStyles({
          pathColor: "#E6006F",
          trailColor: "white"
        })}>
          <div className="w-5">
            <img src="/assets/icon-go-to-top.svg" />
          </div>
        </CircularProgressbarWithChildren>
      </button>
    </LinkScroll>
  )
}