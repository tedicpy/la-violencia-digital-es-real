import React from 'react';
import ReactMarkdown from 'react-markdown'
// props = {title}
export default class Collapsable extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isCollapsed: true };
    };

    toggleCollapsed () {
        this.setState({
            isCollapsed: !this.state.isCollapsed
        })
    }

    render() {
        return (
            <div className="">
                <div className={`border cursor-pointer rounded-lg pt-4 px-4 text-white bg-${this.props.bgColor}`}>
                    <div className={`flex justify-between md:h-14 mb-4 leading-tight`} onClick={this.toggleCollapsed.bind(this)}>
                        <button className="text-left" >
                            {this.props.title}
                        </button>
                        <div className="self-center w-8">
                            {this.state.isCollapsed ? (<img src="/assets/flecha-abajo.svg" />) 
                            : (<img src="/assets/flecha-derecha.svg" />)}
                        </div>
                    </div>
                    {!this.state.isCollapsed ? (
                        <div className={`font-regular py-4 border-t border-black prose prose-content prose-sm max-w-screen lg:prose-base xl:prose-lg leading-loose`}>
                            <ReactMarkdown components={{
                                ul: ({ children }) => {
                                    return (
                                        <ul className="list-disc list-inside">{children}</ul>
                                    )
                                },

                                a: ({ children, href }) => {
                                    return (
                                        <a href={href} target="_blank" rel="noreferrer" className="underline">{children}</a>
                                    )
                                },
                            }}>{this.props.children}</ReactMarkdown>
                        </div>
                    ) : ''}
                </div>
            </div>
        )
    }
}

