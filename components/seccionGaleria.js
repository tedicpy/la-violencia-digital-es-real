import React from "react";
import ImageStripWithModal from "./imageStripWithModal";
import useTranslation from '../lib/translations/useTranslation.hook'


export default function SeccionGaleria({ images }) {
  const { t } = useTranslation()
  const imagesParsed = images.map(({ file }, index) => ({
    name: index,
    url: `/blog-files/paginas/${file}`
  }))

  return (
    <section className="bg-brand-green-3 py-24">
      <div className="lg:px-28 xl:px-56">
        <h1 className="text-2xl uppercase text-center mb-5 md:text-3xl lg:text-4xl">
          {t('common.galeria-de-imagenes')}
        </h1>
      </div>

      <ImageStripWithModal images={imagesParsed} stripConfig={{
          menuItemClass:      ' w-32 md:h-40 md:w-40 lg:h-48 lg:w-48 xl:h-56 xl:w-56 rounded-lg bg-brand-pink-3',
          menuItemImageClass: ' w-32 md:h-40 md:w-40 lg:h-48 lg:w-48 xl:h-56 xl:w-56 rounded-lg hover:opacity-60',
          menuItemTextClass: 'hidden invisible',
          arrowLeftClass: 'text-white text-4xl -top-6',
          arrowRightClass: 'text-white text-4xl -top-6',
          styles: ``
      }}></ImageStripWithModal>

      <div className="w-full text-center my-2">
        <a href="https://www.youtube.com/playlist?list=PLch0tB2jQVqYYes22rWazDHulb09e6x-p" 
          target="_blank" rel="noreferrer" 
          className="inline-block w-28 px-4 py-2.5 mt-5 text-sm font-medium text-white bg-brand-pink-3 rounded-lg mr-5 hover:bg-brand-green-2 active:bg-brand-pink-1"
        >
          {t('common.ver-videos')}
        </a>
      </div>
    </section>
  );
}