import React from 'react'

export default function SeccionViolenciaDeGeneroEnParaguay({ data }) {
  return (
    <section className="w-full">
      {/* Header rosado */}
      <div id="header" className="bg-pink-600 text-white lg:pt-20 lg:px-20">
        <div className="max-w-7xl mx-auto flex flex-col lg:px-4 xl:px-20 lg:flex-row items-center">
          {/* Columna izquierda: texto */}
          <div className="lg:w-1/2 w-full my-14 lg:my-0 lg:mb-0 px-8 lg:px-0 text-center lg:text-left">
            <h2 className="text-3xl lg:text-4xl font-bold mb-2">
              {data.investigaciones_title}
            </h2>
            <p className="text-base lg:text-lg">
              {data.investigaciones_description}
            </p>
          </div>

          {/* Columna derecha: imagen */}
          <div className="lg:w-1/2 w-full flex justify-center lg:justify-end">
            <img
              src="/assets/investigaciones/investigaciones-header.png"
              alt="Ilustración header"
              className="w-80 h-auto"
            />
          </div>
        </div>
      </div>

      {/* Contenido con fondo morado */}
      <div className="bg-[#6140e7] py-16 md:py-12 px-4">
        <div className="max-w-7xl mx-auto space-y-8">
          {data.investigaciones.map((investigacion, index) => (
            // Tarjeta transparente con texto blanco
            <div
              key={index}
              className="flex flex-col md:flex-row bg-transparent text-white"
            >
              {/* Imagen de la investigación */}
              <div className="md:w-1/3 flex flex-row place-content-start">
                <img
                  src={investigacion.imagenSrc}
                  alt={`Tapa ${investigacion.titulo}`}
                  className=" object-scale-down w-2/3 -m-8 xs:-m-10 md:w-full md:m-0"
                />
              </div>

              {/* Texto y botones */}
              <div className="md:w-2/3 w-full p-6 py-10 flex flex-col place-content-between">
                <div className="">
                  <div className="">
                    <h3 className="text-2xl font-brand uppercase font-bold mb-2">
                      {investigacion.titulo}
                    </h3>
                    <p className="text-sm text-white/80 italic mb-2">
                      {investigacion.fecha}
                    </p>
                  </div>
                  <p className="text-base font-thin mb-4">
                    {investigacion.descripcion}
                  </p>
                </div>

                {/* Acciones */}
                <div className="flex flex-col rounded-lg">
                  {/* Título */}
                  <p className="text-brand-indigo-4 text-lg font-normal font-brand mb-1.5">
                    {data.descargar_investigacion}
                  </p>

                  {/* Botones de idioma */}
                  <div className="flex space-x-3">
                    <a
                      href={investigacion.descargaEsUrl}
                      className="bg-[#E63950] text-white font-bold py-2 px-4 rounded-lg shadow-md hover:bg-[#C5303D]"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {data.spanish_download}
                    </a>
                    <a
                      href={investigacion.descargaEnUrl}
                      className="bg-[#6D2E75] text-white font-bold py-2 px-4 rounded-lg shadow-md hover:bg-[#5A2360]"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {data.english_download}
                    </a>
                  </div>
                </div>
                
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  )
}
