import React from "react"
import { useRouter } from 'next/router'
import Link from "next/link"
import useTranslation from '../lib/translations/useTranslation.hook'
import filter from 'lodash.filter'
import map from 'lodash.map'
import config from '../config'

export default function SeccionVisualizacionProcedimientosJudiciales({ post, categoryContentIndex }) {
  const router = useRouter()
  const { t, locale } = useTranslation()

  // obtiene los datos del post de los procedimientos judiciales asociados al post
  const visualizacionFilter = {}
  visualizacionFilter[config.defaultLocale] = post[config.defaultLocale] + '-procedimientos-judiciales'
  let timelinePostContent = filter(categoryContentIndex, visualizacionFilter)
  timelinePostContent = timelinePostContent.length ? timelinePostContent[0] : null
  
  const procedimientosDelLitigioDefaultPath = timelinePostContent && `/${locale}/b/litigios-estrategicos/${timelinePostContent[locale]}`
  const procedimientosJudicialesIndex = timelinePostContent && timelinePostContent['visualizacion']['eventosPrincipales']

  const eventsObject = {}
  procedimientosJudicialesIndex && procedimientosJudicialesIndex.forEach(post => {
    eventsObject[post.year] = eventsObject[post.year] || { year: post.year, events: [] }
    eventsObject[post.year].events.push(post)
  })
  
  return (
    <section className="relative">
      {/* bg effect */}
      <div className="absolute bottom-0 -mb-5 md:-mb-14 w-full">
        <svg className={`header-bg-svg text-brand-red-4 w-full fill-current`} viewBox="0 0 611 88.7"><polygon className="cls-1" points="0 0 0 71.05 305.5 88.7 611 71.05 611 0 0 0"/></svg>
      </div>
      <div className="absolute top-0 -mt-5 md:-mt-14 w-full">
        <svg className={`header-bg-svg text-brand-red-4 w-full fill-current`} viewBox="0 0 611 88.7"><polygon className="cls-1" points="611 88.7 611 17.66 305.5 0 0 17.66 0 88.7 611 88.7"/></svg>
      </div>

      <div className="relative bg-brand-red-4 py-10">

        <div className="text-center w-2/3 lg:w-2/3 md:text-lg lg:text-xl lg:px-28 xl:w-3/5 2xl:w-1/2 m-auto">
          <h3 className="text-2xl md:text-3xl lg:text-4xl uppercase mb-5 lg:mb-7">
            {t('common.visualizacion-procedimientos-judiciales')}
          </h3>
          <p className="m-auto">
            {timelinePostContent && timelinePostContent.visualizacion.descriptionResumen}
          </p>
        </div>
        {/* componente */}
        <div className="flex justify-center my-10 lg:my-0 lg:py-16 px-0 md:px-16 lg:px-20 xl:px-36">
          <div className="bg-white rounded-lg w-3/4 md:w-2/3 lg:w-2/3 2xl:w-1/3 p-4 shadow">
            {map(eventsObject, ({ year, events }, index) => (
              <div key={index}>
                <span className="relative inline-block date uppercase font-bold tracking-tight pb-2">
                  {year}
                </span>
                {events.map(({ title, caso, year, color }, index) => (
                  <div className="flex mb-2" key={index}>
                    {/* <div className="w-2/12">
                      <span className={`text-sm text-${color} font-bold block`}>{year}</span>
                    </div> */}
                    <div className="w-1/12">
                      <span className={`bg-${color} h-3 w-3 rounded-full block mt-2.5`}></span>
                    </div>
                    <div className="w-9/12">
                      <span className="block text-lg mb-0">{title}</span>
                      <span className={`block text-xs mb-1 font-bold uppercase tracking-tighter text-${color}`}>{caso}</span>
                    </div>
                  </div>
                ))}
              </div>
            ))}
          </div>
        </div>
        <div className="text-center ">
          <Link href={procedimientosDelLitigioDefaultPath || ''}>
            <a className=" px-6 md:text-xl py-2.5 text-sm font-medium text-white bg-brand-indigo-1 rounded-lg mr-5 hover:bg-brand-green-2 active:bg-brand-pink-1">
              {t('common.ver-en-detalle')}
            </a>
          </Link>
        </div>

      </div>

      <style global jsx>{`
        .date:after {
          content: "";
          position: absolute;
          border-top: 2px solid ${config.colors["brand-red-4"]};
          top: 12px;
          width: 600px;
        }

        .date:after {
          margin-left: 15px;
        }
      `}</style>
    </section>
  );
}