import React from 'react';

function Accordion({ title, children }) {
  return (
    <details className="accordion bg-white border rounded-lg overflow-hidden">
      <summary className="accordion-title font-semibold p-4 cursor-pointer">
        {title}
      </summary>
      <div className="accordion-content p-4">
        {children}
      </div>
    </details>
  );
}

export default Accordion;