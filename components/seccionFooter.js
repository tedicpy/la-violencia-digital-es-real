import useTranslation from '../lib/translations/useTranslation.hook'
import includes from 'lodash.includes'

export default function SeccionFooter({ page }) {
  const { t } = useTranslation()

  // const casoEspecial = includes(page, 'caso-belen') || includes(page, 'en-paraguay')

  return (
    <footer className="flex flex-col md:flex-row items-center bg-brand-indigo-1 text-white px-10 py-10 lg:py-16 md:px-10 lg:px-24 xl:px-40 ">
      <div className="text-sm w-full mb-10 md:mb-0 md:w-2/5">
        <div className="text-center md:text-left">{t('common.apoyo-de')}</div>

        {/* portada y páginas */}
        <div className="grid grid-cols-3 gap-4 lg:grid-cols-3 2xl:grid-cols-3 md:gap-3 lg:gap-2 mt-2 lg:mr-12 items-center justify-items-center xl:1/2">
          <a href="https://indela.fund/" target="_blank" rel="noreferrer">
            <img src="/assets/footer-logos/indela.png"/>
          </a>
          <a href="https://www.womensrightsonline.net/" target="_blank" rel="noreferrer">
            <img src="/assets/footer-logos/WRO.png"/>
          </a>
          <a href="https://www.apc.org/" target="_blank" rel="noreferrer">
            <img src="/assets/footer-logos/APC_blanco_200px.png"/>
          </a>
        </div>
      </div>

      <div className="flex flex-col md:flex-row w-full pl-5 md:w-auto md:pl-6 md:ml-10 lg:pl-14 md:border-l lg:border-l border-brand-indigo-3 items-center justify-items-center">
        <div className="mb-4 mt-4 w-40 m-auto md:w-44 lg:w-48 lg:mb-2">
          <a href="https://tedic.org" target="_blank" rel="noreferrer"><img src="/assets/footer-logos/tedic.png" className="w-32 lg:w-34"/></a>
        </div>

        <div className="grid grid-cols-1 gap-1 text-center mb-10 leading-tight md:p-5 md:text-left md:mt-5 md:pl-10 lg:leading-normal">
          <a href="https://www.tedic.org/terminos-de-uso-y-politica-de-privacidad/" target="_blank" rel="noreferrer" className="text-brand-pink-4 hover:text-brand-green-3 cursor-pointer ">{t('common.terminos-y-politica-de-privacidad')} &gt;&gt;</a>
          <a href="https://www.tedic.org/preguntas-frecuentes/" target="_blank" rel="noreferrer" className="text-brand-pink-4 hover:text-brand-green-3 cursor-pointer ">{t('common.preguntas-frecuentes')} &gt;&gt;</a>
          <p className="mt-4">
            {t('common.creative-commons')} <br />
            <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.es" target="_blank" rel="noreferrer">
              <span className="text-brand-pink-4 hover:text-brand-green-3 cursor-pointer">
                {t('common.creative-commons-link')}
              </span>
              <img src="https://licensebuttons.net/l/by-sa/4.0/80x15.png" alt="Licencia Creative Commons" data-mce-src="https://licensebuttons.net/l/by-sa/4.0/80x15.png" className="inline-block ml-2"></img>
            </a>
          </p>
          <div className="flex mt-2 justify-center md:justify-start">
            {t('common.hecho-con-amor')}{<img src="/assets/footer-logos/corazonsito.svg" alt="amor" className="mx-1"/>}{t('common.software-libre')}
          </div>
        </div>
      </div>
      
      <div className={`
        hidden invisible
        
        p-0 p-px p-0.5 p-1 p-1.5 p-2 p-2.5 p-3 p-3.5 p-4 p-5 p-6 p-7 p-8 p-9 p-10 p-11 p-12 p-14 p-16 p-20 p-24 p-28 p-32 p-36 p-40
        pr-0 pr-px pr-0.5 pr-1 pr-1.5 pr-2 pr-2.5 pr-3 pr-3.5 pr-4 pr-5 pr-6 pr-7 pr-8 pr-9 pr-10 pr-11 pr-12 pr-14 pr-16 pr-20 pr-24 pr-28 pr-32 pr-36 pr-40
        pl-0 pl-px pl-0.5 pl-1 pl-1.5 pl-2 pl-2.5 pl-3 pl-3.5 pl-4 pl-5 pl-6 pl-7 pl-8 pl-9 pl-10 pl-11 pl-12 pl-14 pl-16 pl-20 pl-24 pl-28 pl-32 pl-36 pl-40

        -p-0 -p-px -p-0.5 -p-1 -p-1.5 -p-2 -p-2.5 -p-3 -p-3.5 -p-4 -p-5 -p-6 -p-7 -p-8 -p-9 -p-10 -p-11 -p-12 -p-14 -p-16 -p-20 -p-24 -p-28 -p-32 -p-36 -p-40
        -pr-0 -pr-px -pr-0.5 -pr-1 -pr-1.5 -pr-2 -pr-2.5 -pr-3 -pr-3.5 -pr-4 -pr-5 -pr-6 -pr-7 -pr-8 -pr-9 -pr-10 -pr-11 -pr-12 -pr-14 -pr-16 -pr-20 -pr-24 -pr-28 -pr-32 -pr-36 -pr-40
        -pl-0 -pl-px -pl-0.5 -pl-1 -pl-1.5 -pl-2 -pl-2.5 -pl-3 -pl-3.5 -pl-4 -pl-5 -pl-6 -pl-7 -pl-8 -pl-9 -pl-10 -pl-11 -pl-12 -pl-14 -pl-16 -pl-20 -pl-24 -pl-28 -pl-32 -pl-36 -pl-40

        m-0 m-mx m-0.5 m-1 m-1.5 m-2 m-2.5 m-3 m-3.5 m-4 m-5 m-6 m-7 m-8 m-9 m-10 m-11 m-12 m-14 m-16 m-20 m-24 m-28 m-32 m-36 m-40
        mr-0 mr-mx mr-0.5 mr-1 mr-1.5 mr-2 mr-2.5 mr-3 mr-3.5 mr-4 mr-5 mr-6 mr-7 mr-8 mr-9 mr-10 mr-11 mr-12 mr-14 mr-16 mr-20 mr-24 mr-28 mr-32 mr-36 mr-40
        ml-0 ml-mx ml-0.5 ml-1 ml-1.5 ml-2 ml-2.5 ml-3 ml-3.5 ml-4 ml-5 ml-6 ml-7 ml-8 ml-9 ml-10 ml-11 ml-12 ml-14 ml-16 ml-20 ml-24 ml-28 ml-32 ml-36 ml-40
        mt-0 mt-mx mt-0.5 mt-1 mt-1.5 mt-2 mt-2.5 mt-3 mt-3.5 mt-4 mt-5 mt-6 mt-7 mt-8 mt-9 mt-10 mt-11 mt-12 mt-14 mt-16 mt-20 mt-24 mt-28 mt-32 mt-36 mt-40

        -m-0 -m-mx -m-0.5 -m-1 -m-1.5 -m-2 -m-2.5 -m-3 -m-3.5 -m-4 -m-5 -m-6 -m-7 -m-8 -m-9 -m-10 -m-11 -m-12 -m-14 -m-16 -m-20 -m-24 -m-28 -m-32 -m-36 -m-40
        -mr-0 -mr-mx -mr-0.5 -mr-1 -mr-1.5 -mr-2 -mr-2.5 -mr-3 -mr-3.5 -mr-4 -mr-5 -mr-6 -mr-7 -mr-8 -mr-9 -mr-10 -mr-11 -mr-12 -mr-14 -mr-16 -mr-20 -mr-24 -mr-28 -mr-32 -mr-36 -mr-40
        -ml-0 -ml-mx -ml-0.5 -ml-1 -ml-1.5 -ml-2 -ml-2.5 -ml-3 -ml-3.5 -ml-4 -ml-5 -ml-6 -ml-7 -ml-8 -ml-9 -ml-10 -ml-11 -ml-12 -ml-14 -ml-16 -ml-20 -ml-24 -ml-28 -ml-32 -ml-36 -ml-40

        lg:-mt-10 lg:-mt-12 lg:-mt-14 lg:-mt-16 lg:-mt-20 lg:-mt-28

        w-0 w-px w-0.5 w-1 w-1.5 w-2 w-2.5 w-3 w-3.5 w-4 w-5 w-6 w-7 w-8 w-9 w-10 w-11 w-12 w-14 w-16 w-20 w-24 w-28 w-32 w-36
        w-40 w-44 w-48 w-52 w-56 w-60 w-64 w-72 w-80 w-96 w-auto w-1/2 w-1/3 w-2/3 w-1/4 w-2/4 w-3/4 w-1/5 w-2/5 w-3/5 w-4/5 
        w-1/6 w-2/6 w-3/6 w-4/6 w-5/6 w-1/12 w-2/12 w-3/12 w-4/12 w-5/12 w-6/12 w-7/12 w-8/12 w-9/12 w-10/12 w-11/12 
        w-full w-screen w-min w-max

        lg:w-1/3 lg:w-96

        float-left float-right

        brand-red-1 bg-brand-red-1 
        text-brand-red-1 bg-brand-red-2
        text-brand-red-2  bg-brand-red-3
        text-brand-red-3  bg-brand-red-4
        text-brand-red-4 bg-brand-indigo-1
        text-brand-indigo-1 bg-brand-indigo-2
        text-brand-indigo-2 bg-brand-indigo-3
        text-brand-indigo-3 bg-brand-indigo-4
        text-brand-indigo-4 bg-brand-indigo-5
        text-brand-indigo-5 bg-brand-green-1
        text-brand-green-1 bg-brand-green-2
        text-brand-green-2 bg-brand-green-3
        text-brand-green-3 bg-brand-green-4
        text-brand-green-4 bg-brand-pink-1
        text-brand-pink-1 bg-brand-pink-2
        text-brand-pink-2 bg-brand-pink-3
        text-brand-pink-3 bg-brand-pink-4
        text-brand-pink-4 bg-brand-pink-5
        text-brand-pink-5 bg-brand-blue-1
        text-brand-blue-1 bg-brand-blue-2
        text-brand-blue-2 bg-brand-blue-3
        text-brand-blue-3
      `}></div>
    </footer>
  )
}