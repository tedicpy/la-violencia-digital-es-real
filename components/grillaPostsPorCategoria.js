import React from "react"
import Link from "next/link"
import useTranslation from '../lib/translations/useTranslation.hook'

export default function GrillaPostsPorCategoria({ category, gridData }) {
  const { t, locale } = useTranslation()

  return (
    <section className="relative z-10">
      {/* bg effect */}
      <div className="absolute -mb-5 md:-mb-14 lg:-mb-16 xl:-mb-20 w-full bottom-0">
        <svg className={`header-bg-svg text-white w-full fill-current`} viewBox="0 0 611 88.7"><polygon className="cls-1" points="0 0 0 71.05 305.5 88.7 611 71.05 611 0 0 0"/></svg>
      </div>

      <div className="relative pb-7 pt-28">
        <div className="grid grid-cols-1 gap-4 justify-center justify-items-center px-14 lg:px-28 xl:px-56 ">
          <h2 className="mb-10 text-center text-brand-indigo-2 text-xl md:text-2xl lg:text-4xl uppercase">{t('common.guia-sobre-violencia')}</h2>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
            {gridData.map(({ title, description, mainImage, destUrl }, index) => (
              <div key={index} className="flex w-full border border-brand-indigo-5 rounded-lg p-4 md:hover:shadow-md transform md:hover:-translate-y-1 transition-all duration-150">
                <div className='mr-2'>
                  <img src={` /blog-files/${category}/${mainImage}`} />
                </div>
                <div>
                  <h3 className="text-lg leading-tight text-brand-indigo-3 mb-2 font-brand-content tracking-tighter">{title}</h3>
                  <p className="leading-tight mb-2">{description}</p>
                  <Link href={destUrl}>
                    <a className="text-brand-pink-3 hover:text-brand-green-2 font-bold underline">{t('common.leer-mas')} &gt;&gt;</a>
                  </Link>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
}