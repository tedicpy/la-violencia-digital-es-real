import React, { useState, Fragment } from "react"
import filter from 'lodash.filter'
import map from 'lodash.map'
import ReactMarkdown from 'react-markdown'
import useTranslation from '../lib/translations/useTranslation.hook'

import PostModal from './postModal'

function CollapsableToggle ({ render }) {
  const [ isCollapsed, setIsCollapsed ] = useState(true)
  return render({ isCollapsed, setIsCollapsed })
}

export default function SeccionVisualizacionProcedimientosJudicialesLarge({ post, categoryContentIndex, operadoresContentIndex }) {
  const [ isModalOpen, setIsModalOpen ] = useState(false)
  const [ selectedPost, setSelectedPost ] = useState(false)
  const { t } = useTranslation()
  
  const openModal = ({ category, nombre, bio, imageFile, bgClass, cargo, documentoEnlace }) => {
    setIsModalOpen(true)
    setSelectedPost({
      imagePath: imageFile && `/blog-files/${category}/${imageFile}`,
      bgClass,
      title: nombre,
      tag: cargo,
      content: bio,
      documentLink: documentoEnlace
    })
  }

  const eventsObject = {}
  categoryContentIndex && categoryContentIndex.forEach(post => {
    if (!post) { return }
    eventsObject[post.year] = eventsObject[post.year] || { year: post.year, events: [] }
    eventsObject[post.year].events.push(post)
  })
  
  return (
    <section className="relative pt-5">
      {/* bg effect */}
      <div className="absolute -mb-10 md:-mb-14 lg:-mb-16 xl:-mb-18 w-full bottom-0">
        <svg className={`header-bg-svg text-white w-full fill-current`} viewBox="0 0 611 88.7"><polygon className="cls-1" points="0 0 0 71.05 305.5 88.7 611 71.05 611 0 0 0"/></svg>
      </div>

      <div className="relative pb-5 px-0 md:px-16 lg:px-20 xl:px-36">
        <div className="relative w-2/3 md:w-1/2 mx-auto">
          {/* events */}
          <ul className="list-none m-0 p-0">
            {map(eventsObject, ({ year, events }, key) => (
              <Fragment key={key}>
                <li className="mb-2" key={year}>
                  <div className="flex items-center mb-1">
                    {/* year */}
                    <div className="font-black text-xl w-12 text-brand-pink-3">
                      {year}
                    </div>
                  </div>
                </li>
                {map(events, ({ title, date, content, operadores, documentoEnlace, enlaceExterno }, index) => (
                  <li className="mb-5 lg:mb-8" key={index}>
                    <CollapsableToggle render={({ isCollapsed, setIsCollapsed }) => (
                      <div className={`flex mb-1`}>
                        {/* timepoint */}
                        {isCollapsed ? (
                          <div className={`z-10 relative top-7 border-4 border-brand-green-2 bg-white h-5 w-5 rounded-full`} style={{left: "76px"}}></div>
                        ) : (
                          <div className={`z-10 relative top-6 border-4 border-white bg-brand-green-2 h-8 w-8 rounded-full`} style={{left: "82px", marginLeft: "-12px"}}></div>
                        )}
                        {/* event card */}
                        <div className={`flex-1 relative ml-24 border border-brand-blue-3 p-5 rounded-lg ${isCollapsed ? '' : ' shadow-inner'}`}>
                          <div className="font-bold text-brand-pink-3 text-xs mb-1">
                            {date}
                          </div>
                          <button type="button" onClick={() => setIsCollapsed(!isCollapsed)} className="font-bold text-left text-brand-indigo-3 leading-tight tracking-tight">
                            {title}
                          </button>
                          {/* collapsed content */}
                          {!isCollapsed && (
                            <div className=" leading-tight mt-2">
                              {/* full content */}
                              <div className="mb-2 prose prose-content">
                                <ReactMarkdown>{content}</ReactMarkdown>          
                              </div>
                              {/* operadores */}
                              {operadores && operadores.map((operadorKey, index) => {
                                let operadorContent = filter(operadoresContentIndex, { key: operadorKey })
                                operadorContent = operadorContent.length ? operadorContent[0] : {}
                                let { nombre } = operadorContent
                                return (
                                  <button type="button" onClick={() => openModal(operadorContent)} className=" uppercase block text-left tracking-tight text-xs font-bold text-brand-pink-3 hover:text-brand-green-2 underline" key={index}>
                                    {nombre} &gt;&gt;
                                  </button>
                                )
                              })}
                              {/* enlaces */}
                              <div className="pt-2">
                                {documentoEnlace && !documentoEnlace.map && (
                                  <div className="justify-self-center my-2 text-base md:text-xl">
                                    <a href={documentoEnlace} target="_blank" rel="noreferrer" className="inline-block py-1 px-2 border border-brand-pink-3 rounded-md tracking-tight text-xs text-brand-pink-3 hover:text-brand-green-2">{t('common.abrir-archivo')}</a>
                                  </div>
                                )}
                                {documentoEnlace && documentoEnlace.map && (
                                  documentoEnlace.map((link, index) => (
                                    <div className="justify-self-center my-2 text-base md:text-xl" key={index}>
                                      <a href={link} target="_blank" rel="noreferrer" className="inline-block py-1 px-2 border border-brand-pink-3 rounded-md tracking-tight text-xs text-brand-pink-3 hover:text-brand-green-2">{t('common.abrir-archivo')} {index+1}</a>
                                    </div>
                                  ))
                                )}
                                {enlaceExterno && !enlaceExterno.map && (
                                  <div className="justify-self-center my-2 text-base md:text-xl">
                                    <a href={enlaceExterno} target="_blank" rel="noreferrer" className="inline-block py-1 px-2 border border-brand-pink-3 rounded-md tracking-tight text-xs text-brand-pink-3 hover:text-brand-green-2">{t('common.visitar-enlace')}</a>
                                  </div>
                                )}
                                {enlaceExterno && enlaceExterno.map && (
                                  enlaceExterno.map((link, index) => (
                                    <div className="justify-self-center my-2 text-base md:text-xl" key={index}>
                                      <a href={link} target="_blank" rel="noreferrer" className="inline-block py-1 px-2 border border-brand-pink-3 rounded-md tracking-tight text-xs text-brand-pink-3 hover:text-brand-green-2">{t('common.visitar-enlace')} {index+1}</a>
                                    </div>
                                  ))
                                )}
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    )}></CollapsableToggle>
                  </li>
                ))}
              </Fragment>
            ))}
          </ul>
          {/* vertical line */}
          <div className="border-r-4 border-brand-pink-3 absolute top-0" style={{height: '100%', left: "84px"}}></div>
        </div>
      </div>

      <PostModal 
        open={isModalOpen}
        setOpen={setIsModalOpen}
        post={selectedPost}
        onClose={() => { console.log('modal closed') }}
        modalClass="top-32"
        closeIconClass="text-brand-pink-3"
        // modalStyle={}
        // title={}
        // mainStyle={} 
      />
    </section>
  );
}