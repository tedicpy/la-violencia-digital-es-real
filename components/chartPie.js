import React from 'react'
import { ResponsivePie } from '@nivo/pie'
import useTranslation from '../lib/translations/useTranslation.hook'

export default function ChartPie({ data, colors }) {  
  const { t } = useTranslation()
  data.forEach((d, index) => {
    d.color = colors[index]
  })
  const calcPercentage = v => {
    return v
  }
  return (
    <ResponsivePie
      data={data}
      theme={{
        "fontSize": 18
      }}
      margin={{ top: 10, bottom: 70, left: 0, right: 10 }}
      valueFormat={calcPercentage}
      arcLabel={d => `${d.data.value} %`}
      colors={d => d.data.color}
      borderColor={{ from: 'color' }}
      enableArcLabels={true}
      arcLabelsTextColor="white"
      enableArcLinkLabels={false}
      animate={false}
      tooltip={({ datum }) => {
        return (
          <div className="px-3 py-2 bg-white rounded shadow-sm">
            <div>
              <span className="relative inline-block w-5 h-5 mr-2 top-1" style={{ backgroundColor: datum.color }}></span>
              {datum.data.label} <b>{datum.formattedValue} %</b>
            </div>
          </div>
        )
      }}
      legends={[
        {
          anchor: 'bottom-left',
          direction: 'column',
          justify: false,
          translateX: 0,
          translateY: 70,
          itemWidth: 0,
          itemHeight: 35,
          itemsSpacing: 0,
          symbolSize: 25,
          symbolShape: 'circle',
          itemDirection: 'left-to-right',
        }
      ]}
    />
  )
}