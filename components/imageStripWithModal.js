import React, { useState } from 'react'
import ImageStripShort from './imageStripShort'
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'

export default function ImageStripWithModal({ images, stripConfig }) {
  const [isOpen, setIsOpen] = useState(false)
  const [lightboxSlideIndex, setLightboxSlideIndex] = useState(0)

  const selectImage = (image) => {
    setLightboxSlideIndex(image)
    setIsOpen(true)
  }

  return (
    <div className="">
      <ImageStripShort imgs={images} onSelect={selectImage} {...stripConfig}></ImageStripShort>
      {isOpen && (
        <Lightbox
          mainSrc={images[lightboxSlideIndex].url}
          nextSrc={images[(lightboxSlideIndex + 1) % images.length].url}
          prevSrc={images[(lightboxSlideIndex + images.length - 1) % images.length].url}
          onCloseRequest={() => setIsOpen(false)}
          onMovePrevRequest={() => {
            let prevIndex = (lightboxSlideIndex + images.length - 1) % images.length
            setLightboxSlideIndex(prevIndex)
            console.log(prevIndex)
          }}
          onMoveNextRequest={() => {
            let nextIndex = (lightboxSlideIndex + 1) % images.length
            setLightboxSlideIndex(nextIndex)
            console.log(nextIndex)
          }}
        />
      )}
    </div>
  )
}