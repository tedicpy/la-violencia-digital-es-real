import React from 'react'
import ReactMarkdown from 'react-markdown'


export default function SeccionContenido({ category, content, contentClass, heroImage, heroImageDescription }) {
    return (
      <section id="content" className={`${contentClass}`}>
        <div className={`
          p-14 pb-20 lg:pb-24 pt-16 md:pt-20 lg:pt-24 xl:pt-40 md:px-36 lg:px-48 xl:px-64 2xl:px-72
          prose prose-content prose-lg max-w-screen lg:prose-xl xl:prose-2xl
          leading-loose
        `}>
          <ReactMarkdown components={{
            ul: ({ children }) => {
              return (
                <ul className="w-full">{children}</ul>
              )
            },

            a: ({ children, href }) => {
              return (
                <a href={href} target="_blank" rel="noreferrer">{children}</a>
              )
            },

            img: ({ title, alt, src }) => {
              let metaClass = alt.match(/{([^}]+)/)
              metaClass = metaClass && metaClass[1]
              alt = alt?.replace(/ *\{[^)]*\} */g, "")
              return (
                <img src={src} alt={alt} title={title} className={metaClass} />
              )
            }
          }}>{content}</ReactMarkdown>          
        </div>
        {heroImage && (
          <div className='text-left'>
            <img src={`/blog-files/${category}/${heroImage}`} className="w-screen h-auto"/>
            <div className='text-brand-indigo-1 -mt-10 mb-10 lg:-mt-20 lg:mb-20 lg:pl-36 bg-white'>
              <div className=" w-72 text-xs md:text-sm md:font-medium italic float-left relative pl-5">
                {heroImageDescription}
              </div>
              <div className="clear-both"></div>
            </div>
          </div>
        )}
      </section>
    )
}