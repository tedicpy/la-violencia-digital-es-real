import React from 'react'
import ImageStripWithModal from "./imageStripWithModal"
import useTranslation from '../lib/translations/useTranslation.hook'


export default function EmpresasSumadasGalery({ page, posts }) {
  const { t } = useTranslation()
  
  const imagesParsed = (images) => {
    return images.map(({ file }, index) => ({
      name: index,
      url: `/blog-files/empresas-sumadas/${file}`
    }))
  }

  return (
    <section className="relative pt-10">
      {posts.map(({ title, content, bgColor, images }, index) => (
        <div className="relative" key={index}>
          {/* bg effect */}
          <div className="absolute top-0 -mt-5 md:-mt-14 w-full">
            <svg className={`header-bg-svg text-${bgColor} w-full fill-current`} viewBox="0 0 611 88.7"><polygon className="cls-1" points="611 88.7 611 17.66 305.5 0 0 17.66 0 88.7 611 88.7"/></svg>
          </div>

          <div className="relative">
            <div className={`text-white bg-${bgColor} pb-10 pt-5 md:pb-20`}>
              <div className="px-10 md:px-16 lg:px-28 xl:px-56">
                <h1 className="text-2xl uppercase text-left mb-5">
                  {title}
                </h1>
                <p className="text-base text-left mb-5">
                  {content}
                </p>
              </div>
        
              <ImageStripWithModal images={imagesParsed(images)} stripConfig={{
                  menuItemClass:      ' h-44 w-44 lg:h-48 lg:w-48 xl:h-56 xl:w-56 rounded-base',
                  menuItemImageClass: ' h-44 w-44 lg:h-48 lg:w-48 xl:h-56 xl:w-56 rounded-base',
                  menuItemTextClass: 'hidden invisible',
                  arrowLeftClass: 'text-white text-4xl -top-6', 
                  arrowRightClass: 'text-white text-4xl -top-6',
                  styles: ``
              }}></ImageStripWithModal>
            </div>
          </div>
        </div>
      ))}
    </section>
  );
}