import React, { useState } from "react"
import useTranslation from '../lib/translations/useTranslation.hook'

import PostModal from './postModal'


export default function GrillaPostsPorCategoriaConModal({ category, gridData }) {
  const { t, locale } = useTranslation()
  const [ isModalOpen, setIsModalOpen ] = useState(false)
  const [ selectedPost, setSelectedPost ] = useState(false)
  
  const openModal = (post) => {
    setIsModalOpen(true)
    setSelectedPost(post)
  }

  return (
    // Grid Container
    <div className="grid grid-cols-1 md:grid-cols-6 md:px-16 lg:px-28 xl:px-56 leading-tight mb-20 ">
      {gridData.map(({ title, description, imagePath, tag, tagClass }, index) => (
        // Card
        <div key={index}  className={`flex h-auto md:hover:shadow-md transform md:hover:-translate-y-1 transition-all duration-150 border m-6 border-brand-indigo-5 rounded-md ${imagePath ? ' md:col-span-3' : 'md:col-span-2 ' }`}>
          {/* Card content */}
          {imagePath ? (
            <div className="w-5/12">
              <img src={imagePath} alt={title} className="w-full"/>
            </div>
          ) : ''}
          <div className={` ${imagePath ? 'w-7/12 py-4 pr-4' : 'w-full p-4' } `}>
            <h2 className="text-xl font-brand-content tracking-tight xl:text-2xl text-brand-indigo-3">{title}</h2>
            {tag && (
              <div className="my-1 md:my-2 lg:my-2">
                <span className={`${tagClass} text-xs px-2 w-auto rounded-md `}>{tag}</span>
              </div>
            )}
            <p className="mt-1 mb-2 md:my-4 lg:my-4">{description}</p>
            <button className="justify-self-start" onClick={() => openModal(gridData[index])}>
              <a className="underline font-bold text-brand-pink-3">{t('common.ver-perfil')} &gt;&gt; </a>
            </button>
          </div>
        </div>
      ))}
      
      <PostModal 
        open={isModalOpen}
        setOpen={setIsModalOpen}
        post={selectedPost}
        onClose={() => { console.log('modal closed') }}
        modalClass="top-32"
        closeIconClass="text-brand-pink-3"
        // modalStyle={}
        // title={}
        // mainStyle={} 
      />
    </div>
  );
}