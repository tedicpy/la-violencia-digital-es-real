import React from "react"
import ImageStripShort from "./imageStripShort"
import { useRouter } from 'next/router'
import useTranslation from '../lib/translations/useTranslation.hook'


export default function SeccionLitigiosEstrategicos({ posts }) {
  const router = useRouter()
  const { t, locale } = useTranslation()

  const imagesParsed = []
  posts.forEach((post) => {
    const { title, category, mainImage, mainImageThumb, ...fields } = post
    if (fields.hidden) { return }
    imagesParsed.push({
      name: title,
      url: `/blog-files/${category}/${mainImageThumb || mainImage}`,
      post
    })
  })

  const goToPostFromImage = imageIndex => {
    const selectedPost = imagesParsed[imageIndex].post
    if (selectedPost.externalPageUrl) {
      return window.open(selectedPost.externalPageUrl)
    }
    router.push({
      pathname: '/[lang]/b/[category]/[post]',
      query: { 
        lang: locale,
        category: selectedPost.category,
        post: selectedPost[locale]
       }
    })
  }

  return (
    <section className="relative bg-brand-pink-3">
      {/* bg effect */}
      <div className="absolute top-0 -mt-5 md:-mt-14 w-full">
        <svg className={`header-bg-svg text-brand-pink-3 w-full fill-current`} viewBox="0 0 611 88.7"><polygon className="cls-1" points="611 88.7 611 17.66 305.5 0 0 17.66 0 88.7 611 88.7"/></svg>
      </div>

      <div className="relative pt-10 pb-10">
        <div className="lg:px-28 xl:px-56">
          <h1 className="text-2xl uppercase text-center mb-5 leading-tight text-white md:text-3xl lg:text-4xl">
            {t('common.litigios-estrategicos')}
          </h1>
          <p className="text-white text-center my-4 mx-6 text-lg ">{t('common.litigios-estrategicos-content')}</p>
        </div>

        <div className=" lg:pl-14 xl:pl-20 2xl:pl-24">
          <ImageStripShort imgs={imagesParsed} onSelect={goToPostFromImage} {...{
              menuItemClass: 'w-48 h-52 lg:w-60 lg:h-72 bg-brand-indigo-1 rounded-lg',
              menuItemImageClass: 'bg-white w-48 rounded-lg rounded-b-none lg:w-60 lg:h-48 hover:opacity-60',
              menuItemTextClass: 'bg-brand-indigo-3 font-brand uppercase rounded-b-lg bg-opacity-100 w-48 h-24 lg:h-24 lg:w-60 lg:text-base',
              arrowLeftClass: 'text-white text-4xl -top-6',
              arrowRightClass: 'text-white text-4xl -top-6',
              styles: `
                .image-strip-short .menu-item-wrapper .item-name  {
                  display: block !important;
                }
                .image-strip-short .menu-item-wrapper:hover .item-name  {
                  display: block;
                }
              `
          }}></ImageStripShort>
        </div>
      </div>
    </section>
  );
}