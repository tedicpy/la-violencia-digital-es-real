import ScrollMenu from 'react-horizontal-scrolling-menu'

/**
 * imgs: { name, url }
 * onSelect: Fn
 */
export default function ImageStripShort({ 
  imgs,
  onSelect,
  menuItemClass,
  menuItemImageClass,
  menuItemTextClass,
  arrowLeftClass,
  arrowRightClass,
  styles
}) {
  // Menu
  const MenuItem = ({ text, url }) => {
    return <>
      <div className={`strip-menu-item h-32 w-32 lg:w-44 lg:h-48 ${menuItemClass}`}>
        <div className={`h-32 w-32 lg:w-44 lg:h-48 ${menuItemImageClass}`} alt={text} style={{backgroundImage: `url('${url}')`, backgroundSize: 'cover'}}></div>
        <div className={`item-name absolute p-2 w-32 lg:w-44  bg-brandPurple text-white text-sm rounded-b-xl whitespace-normal ${menuItemTextClass}`}>{text}</div>
      </div>
    </>
  }

  const Menu = (list) => {
    const menuList = list.map((el, index) => {
      const { name, url } = el
      return <MenuItem text={name} url={url} key={index} />
    })
    if (list.length) {
      menuList.push(<>
        <div className={`strip-menu-item h-24 w-32 opacity-0`}>
        </div>
      </>)
      return menuList
    } else {
      return <></>
    }
  }

  // Arrow
  const ArrowLeft = <>
    <div className="relative w-0">
      <div className={`arrow-prev z-10 ml-4 text-black bg-brandPurple font-bold text-lg p-4 rounded-full w-6 h-10 absolute left-0 -top-4 ${arrowLeftClass}`}>
        <span className=" relative -top-3 -left-1.5">{'<'}</span>
      </div>
    </div>
  </>
  const ArrowRight = <>
    <div className="relative w-0">
      <div className={`arrow-next z-10 mr-4 text-black bg-brandPurple font-bold text-lg p-4 rounded-full w-6 h-10 absolute right-0 -top-4 ${arrowRightClass}`}>
        <span className=" relative -top-3 right-1.5">{'>'}</span>
      </div>
    </div>
  </>

  // selection
  const menuList = Menu(imgs);

  return (
    <>
      <div className="image-strip-short">
        <ScrollMenu
          alignCenter={true}
          alignOnResize={true}
          arrowLeft={ArrowLeft}
          arrowRight={ArrowRight}
          clickWhenDrag={false}
          data={menuList}
          dragging={true}
          hideArrows={true}
          hideSingleArrow={true}
          // onFirstItemVisible={this.onFirstItemVisible}
          // onLastItemVisible={this.onLastItemVisible}
          onSelect={onSelect}
          // onUpdate={this.onUpdate}
          // ref={el => (this.menu = el)}
          scrollToSelected={false}
          // selected={selected}
          transition={+0.7}
          translate={0}
          wheel={true}
        />
      </div>

      <style global jsx>{`
        .image-strip-short .strip-menu-item {
          user-select: none;
          cursor: pointer;
          border: 1px transparent solid;
        }
        .image-strip-short .menu-item-wrapper {
          border: 1px transparent solid;
          margin: 5px;
        }
        .image-strip-short .menu-item-wrapper.active {
          
        }
        .image-strip-short .strip-menu-item.active {
          border: 1px green solid;
        }
        .image-strip-short .menu-item-wrapper .item-name  {
          display: none;
        }
        .image-strip-short .menu-item-wrapper:hover .item-name  {
          display: block;
        }

        .image-strip-short .scroll-menu-arrow {
          cursor: pointer;
          color: white;
        }
        
        .image-strip-short .scroll-menu-arrow--disabled {
          visibility: hidden;
        }
        
        .image-strip-short .menu-wrapper {
          padding: 5px 15px;
          width: 100%;
        }
        
        ${styles || ''}

        .nonce {}

      `}</style>
    </>
  )
}