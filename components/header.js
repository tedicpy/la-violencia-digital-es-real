import { useContext } from 'react'
import Link from 'next/link'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'
import ReactMarkdown from 'react-markdown'


export default function Header({ data }) {
  const { t, locale } = useTranslation()
  const { setScrollOnInit } = useContext(LayoutContext)

  return (
    <header id="header" className="relative flex flex-col shadow-lg bg-brand-indigo-3">
      {/* bg effect */}
      <div className="absolute -mb-5 md:-mb-14 lg:-mb-16 xl:-mb-20 w-full bottom-0">
        <svg className={`header-bg-svg text-brand-indigo-2 w-full fill-current`} viewBox="0 0 611 88.7"><polygon points="0 0 0 71.05 305.5 88.7 611 71.05 611 0 0 0"/></svg>
      </div>

      {/* hero */}
      <div className="relative">
        {/* inner bg effect */}
        <div className="absolute w-full bottom-0">
          <svg className={`header-bg-svg text-brand-indigo-2 w-full fill-current`} viewBox="0 0 611 98"><polygon points="611 98 0 98 0 0 611 76.35 611 98"/></svg>
        </div>

        <div className="p-10 pb-0 mt-16 md:mt-28 lg:mt-20 lg:mx-32 xl:mx-36 2xl:mt-5 2xl:mx-48">
          <Link href={`/${locale}`}>
            <h1 className="flex flex-row items-center mx-auto cursor-pointer place-content-center m-auto" alt={t('common.site-title')} title={t('common.site-title')}>
              <img className="z-20 w-36 md:w-2/5 " src="/assets/hero-portada.png"/>
              <div className="z-20 pl-5 md:w-3/5 pb-5 leading-tight md:pb-14 lg:pb-32 xl:mb-34">
                <img src={`/assets/logo-text-white_${locale}.svg`} className="h-14 sm:h-20 md:h-24 lg:h-28 xl:h-32 2xl:h-40 " alt={t('common.site-title')} />
                <span className="font-sans font-normal leading-tight text-white text-base sm:text-lg lg:text-xl xl:text-2xl 2xl:text-3xl">{t('common.site-subtitle')}</span>
              </div>
            </h1>
          </Link>
        </div>
      </div>

      {/* intro */}
      <div id="intro" className="relative pt-16 pb-5 lg:pt-28 xl:pt-48 w-auto mx-auto bg-brand-indigo-2 text-white">
        <div className="m-auto px-10 pt-5 text-lg leading-5 text-center md:flex-row lg:w-3/4 lg:text-xl xl:w-3/5  xl:text-2xl lg:leading-5 xl:leading-7">
          <div className="">
            <h2 className=" uppercase mb-10 text-brand-green-3 text-xl md:text-2xl lg:text-4xl">{data && data.title}</h2>
            <div className="px-12 mx-auto prose-sm text-white lg:prose">
              <ReactMarkdown components={{
                a: ({ children, href }) => {
                  return (
                    <a href={href} target="_blank" rel="noreferrer">{children}</a>
                  )
                }
              }}>{data && data.intro}</ReactMarkdown>
            </div>
          </div>
          <Link href={`/${locale}/b/paginas/${data && data[locale]}`} scroll={false}>
            <a onClick={() => setScrollOnInit('header')} className=" inline-block w-28 px-4 py-2.5 mt-5 text-sm font-medium text-white bg-brand-pink-3 rounded-lg mr-5 hover:bg-brand-green-2 active:bg-brand-pink-1">
              <span className="text-white">{t('common.leer-mas')}</span>
            </a>
          </Link>
        </div>
      </div>
    </header>
  )
}