import React from "react"
import useTranslation from '../lib/translations/useTranslation.hook'
import ReactMarkdown from 'react-markdown'
import Modal from './modal'

export default function PostModal({ 
  open,
  setOpen,
  post,
  onClose,
  modalClass,
  closeIconClass
}) {
  const { t, locale } = useTranslation()
  
  return (
    <Modal 
      open={open}
      setOpen={setOpen}
      onClose={onClose}
      modalClass={modalClass}
      closeIconClass={closeIconClass}
      // modalStyle={}
      // title={}
      // mainStyle={} 
    >
      <div className="items-center py-4 m-6 lg:p-14">
        {post.imagePath && (
          <div className="text-center lg:w-1/4 lg:float-left">
              <img src={post.imagePath} alt={post.title} className=" mx-auto w-56 lg:w-72"/>
          </div>
        )}
        <div className={`${post.imagePath ? 'lg:w-3/4' : 'w-full'}  lg:float-left`}>
          <div className="text-left p-2 lg:p-0">
            <h1 className="text-2xl xl:text-3xl lg:text-3xl md:text-3xl text-brand-indigo-3">{post.title}</h1>
            <div className="md:my-2">
              <span className={`${post.tagClass} text-sm lg:text-lg px-2 w-auto rounded-md`}>{post.tag}</span>
            </div>
          </div>
          <div className="p-2 lg-col-count-2 prose lg:p-0 lg:my-0 leading-tight lg:leading-normal text-xl md:text-left md:my-4">
            <ReactMarkdown>{post.content}</ReactMarkdown>
            {/* enlaces */}
            <div className="pb-6">
              {post.documentLink && !post.documentLink.map && (
                <div className="justify-self-center my-2 text-base md:text-xl">
                  <a href={post.documentLink} target="_blank" rel="noreferrer" className="underline font-bold text-brand-pink-3 hover:text-brand-green-2">{t('common.abrir-archivo')} &gt;&gt; </a>
                </div>
              )}
              {post.documentLink && post.documentLink.map && (
                post.documentLink.map((link, index) => (
                  <div className="justify-self-center my-2 text-base md:text-xl" key={index}>
                    <a href={link} target="_blank" rel="noreferrer" className="underline font-bold text-brand-pink-3 hover:text-brand-green-2">{t('common.abrir-archivo')} {index+1} &gt;&gt; </a>
                  </div>
                ))
              )}
              {post.externalLink && !post.externalLink.map && (
                <div className="justify-self-center my-2 text-base md:text-xl">
                  <a href={post.externalLink} target="_blank" rel="noreferrer" className="underline font-bold text-brand-pink-3 hover:text-brand-green-2">{t('common.visitar-enlace')} &gt;&gt; </a>
                </div>
              )}
              {post.externalLink && post.externalLink.map && (
                post.externalLink.map((link, index) => (
                  <div className="justify-self-center my-2 text-base md:text-xl" key={index}>
                    <a href={link} target="_blank" rel="noreferrer" className="underline font-bold text-brand-pink-3 hover:text-brand-green-2">{t('common.visitar-enlace')} {index+1} &gt;&gt; </a>
                  </div>
                ))
              )}
            </div>
          </div>
        </div>
        <div className="clear-left"></div>
      </div>
    </Modal>
  );
}