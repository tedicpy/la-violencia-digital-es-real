# Descripción

Repositorio para la web "La Violencia Digital es Real".

## Desarrollo

Esta herramienta ha sido desarrollada en conjunto con [WebLab](https://www.weblab.com.py/).

# Documentación

El proyecto está basado en [Next.js](https://nextjs.org/), utiliza [Yarn](https://classic.yarnpkg.com/en/) como gestor de dependencias, [TailwindCSS](https://tailwindcss.com/) como framework css y requiere de [Node.js 10.13](https://nodejs.org/en/) o superior.

Instalar las dependencias ejecutando:

```
yarn install
```

Para ejecutar el proyecto con el entorno de desarrollo y acceder desde http://localhost:3000 utilizar:

```
yarn dev
```

Para generar los archivos estáticos utilizar:

```
yarn export
```

***
# Distribución de archivos
Los datos de los artículos y secciones del sitio web con sus versiones traducidas se encuentran alojados según su estructura de datos.

**Estructura de datos y traducciones por categoría y artículo**
```
  /posts
    ├ litigios
    ├ guia        => Categoría)
      ├ files     => Archivos utilizados por los posts
      ├ index.js  => Lista de definiciones de artículos
      ├ ...
      └ derechos-digitales.js   => Textos del artículo en los diferentes idiomas

```

**Estructura de datos y traducciones por idioma**
```
  /locales
  ├ en
  └ es (idioma predeterminado)
      ├ common                  => Textos del menú, pié y secciones generales
      ├ errors                  => Textos del error 404 y otros
      ├ ...
      └ terminos-y-condiciones  => Textos de páginas generales
```


***

# Estructura de datos y traducciones por categoría y artículo

## Categorías
A cada categoría le corresponde una carpeta dentro de `/posts/[categoria]` que debe contener un archivo `index.js` en donde se indexa a los artículos y se definien los parámetros básicos para su representación. Sólo los artículos indexados serán accesibles.

**Ejemplo:** Para manejar los datos de la Guía sobre violencia y Litigios estratégicos se disponen las categorías:
- guia
- litigios-estrategicos

Las imagenes utilizadas en los artículos pueden ubicarse en la carpeta `/files` dentro de la carpeta de la categoría, serán copiados a la carpeta pública al ejecutarse `yarn dev` ó `yarn export` .


En el archivo `index.js` se debe especificar para cada artículo su `mapa de rutas` y configuraciones opcionales.

**Ejemplo:**
```
/guia/index.js

[
  {
    "en": "is-internet-a-neutral-space",    => ruta en inglés
    "es": "es-internet-un-espacio-neutro"   => ruta en español
  },
  {
    "en": "digital-rights",
    "es": "derechos-digitales",
    "mainImage": "Hero-E.png",              => demás configuraciones
    "bgClass": "bg-brand-indigo-3",         
    "titleColor": "text-white"
  },

  ... otros artículos
]

```

Para usos especiales en donde no se requieran de rutas se puenden indexar páginas especificando el dato `key`.
**Ejemplo:**
```
/empresas-sumadas/index.js

[
  {
    "key": "morena-toro",
    "imageFile": "morena-toro.png",
  },
  {
    "key": "menta-siruela",
    "imageFile": "menta-siruela.png",
  }
]
```

## Artículos
Los artículos necesitan un archivo independiente dentro de la carpeta de la categoría a la que pertenece, dentro se definen los textos en sus distintos idiomas y datos de configuración opcionales.

En el archivo para definir un *texto multidioma* se debe especificar el código del idioma como un sufijo.
Además se pueden definir datos necesarios para usos especiales sin la necesidad de sufijos.

Los datos definidos en los archivos de las páginas se adicionarán a los definidos en el index de la categoría y serán accesibles por los componentes gráficos.

**Ejemplo:**
```
{
  "title_en": "Digital Rights",      // multi idioma
  "title_es": "Derechos Digitales",

  "cargo": "",        // para caso especial

  "bgClass": ""       // para configurar color del encabezado de la página
}

En este caso serían accesibles los datos: title, cargo y bgClass
```

> **El nombre del archivo debe ser igual a la ruta en el idioma predeterminado ó el dato *key* definidos en el `index.js`**

## Páginas
Para páginas independientes con rutas y textos multiidioma se utiliza la categoría `pages` ubicada en la carpeta post `/posts/pages`.

**Ejemplo:**
```
/posts
  └ /pages
      ├ galeria.js      => caso especial, se utiliza como dato por un componente
      ├ ...
      └ violencia-...-en-paraguay.js  => página con estadísticas
```

## Rutas traducidas

Las rutas se generan a partir del `nombre de la carpeta de la categoría` y el mapa de rutas definido en el `index.js` para cada `artículo`.

**Ejemplo:**

Rutas para el artículo Derechos Digitales de la Guía sobre Violencia
```
/es/b/guia/derechos-digitales/
/en/b/guia/digital-rights/
```

Rutas para el artículo Caso Belén de Litigios Estratégicos
```
/en/b/litigios-estrategicos/belen-case/
/es/b/litigios-estrategicos/caso-belen/

```


## Estructuras de datos personalizadas
Se puede manejar otros tipos de datos con el gestor por categorías como en los siguientes casos:
- Empresas Sumadas: Los items de la categoría `empresas-sumadas` se utilizan para listarse las galerías de imágenes de la ``página de empresas sumadas``.
- Operadores y procedimientos judiciales: Cada *Litigio Estratégico* necesita manejar extensas listas asociadas, se utilizan categorías adicionales con estructuras de datos especiales para la grilla de operadores judiciales y las líneas de tiempo. **Ej:** para el *Caso Belén* se disponen de las categorías `caso-belen-operadores-judiciales` y `caso-belen-procedimientos-judiciales`
- Tipos de violencia: Los items de la categoría `tipos-de-violencia` se utilizan para listarse en el ``artículo Tipos de violencia de género digital de la Guía sobre violencia``.

Dentro del `index.js` se pueden definir variables personalizadas para configurar los componentes gráficos.


***

# Estructura de datos y traducciones por idioma
## Agregar nuevo idioma

Para agregar un nuevo idioma se deben seguir los siguientes pasos:
- Crear en la carpeta ``/locales`` una carpeta con el código del nuevo idioma.
- Incluir en la carpeta creada los archivos de traducción correspondientes (``common.js, ..., home.js``), se puede agilizar el proceso duplicando la carpeta del idioma predeterminado ``en``.
- Agregar el código del nuevo idioma en el parámetro ``locales`` del archivo de configuraciones ``/config.js``.
- Agregar el nombre del nuevo idioma en el parámetro ``locale-select`` en el archivo de traducción ``common.js`` para cada idioma disponible.
- Agregar la traducción de las rutas al nuevo idioma en el archivo ``/locales/en/pages.js``

## Páginas estáticas fijas

Para agregar nuevas páginas estáticas:
- Agregar el contenido en sus diferentes traducciones en las subcarpetas de ``/locales`` utilizando como nombre de archivo la ruta en el idioma predeterminado (Si sólo se cuenta con un idioma agregar el archivo en la carpeta del idioma predeterminado ``/locales/en/[my-new-page].js`` de esta manera se mostrará el mismo contenido para todos los idiomas).
- Agregar el nombre traducido de la nueva página en la lista ``pages`` del archivo ``common.js`` de cada idioma.
- Agregar las traducciones de la ruta en el archivo ``/locales/en/pages.js``
- Agregar la ruta con el idioma predeterminado dentro de la función ``getStaticPaths`` del archivo ``/pages/[lang]/page/[page].js`` .

## Colores
Los colores personalizados disponibles se encuentran en el archivo `config.js`, para utilizarlos se debe prefijar al color el tipo de propiedad.

**Ejemplo:**
```
Definición en config.js
{
  ...
  "brand-red-4": "#FFEFDA",
  "brand-indigo-1": "#282663",
  ...
}

Utilización en definición de artículo como color de fondo y color de texto
{
  "bgClass": "bg-brand-red-4 text-brand-indigo-1"
  ...
}

```

# Documentos

El caso tiene asociados muchos documentos PDF por lo que fueron colocados en una subcarpeta en el servidor web.

Los enlaces serán de la forma https://violenciadigital.tedic.org/docs/2015_05_28_Reitera_Pedido_de_Audiencia.pdf

y se colocan donde corresponda en la construcción del sitio.
