export default {
  'site-title': 'Digital Violence is Real',
  'site-subtitle': '¿Querés saber más sobre la violencia de género facilitada por la tecnología?',
  'close-intro': 'Cerrar y ver detalles',
  'know-more': 'Saber más',
  'leer-mas': 'Leer más',
  'conoce-mas': 'Conocé más',
  'ver-perfil': 'Ver Perfil',
  'abrir-archivo': 'Abrir Archivo',
  'visitar-enlace': 'Visitar página externa',
  'ver-videos': 'Ver videos',
  'empresas-de-moda': '¡Estas empresas de moda se suman a la campaña contra la violencia digital de género!',
  'galeria-de-imagenes':'Galería',
  'litigios-estrategicos': 'Litigios Estratégicos',
  'litigios-estrategicos-content':'Enterate más sobre los casos de violencia de género facilitada por la tecnología que TEDIC ha acompañado.',
  'guia-sobre-violencia': 'Guía sobre violencia de género facilitada por la tecnología.',
  'resumen-del-caso': 'Resumen del caso',
  'operadores-judiciales': 'Operadores Judiciales',
  'operadores-judiciales-subtitulo': 'En esta sección figuran les operadores de justicia que tuvieron intervención en las causas mencionadas.',
  'apoyo-de': 'Con el apoyo de:',
  'acuerdos-de-convivencia': 'Acuerdos de convivencia en eventos',
  'terminos-y-politica-de-privacidad': 'Términos de uso y política de privacidad',
  'preguntas-frecuentes': 'Preguntas Frecuentes',
  'creative-commons': 'Todo nuestro contenido está licenciado con',
  'creative-commons-link': 'Creative Commons',
  'hecho-con-amor': 'Hecho con ',
  'software-libre': ' y software libre.',
  'visualizacion-procedimientos-judiciales': 'Visualización de los procedimientos judiciales',
  'ver-en-detalle': 'Ver en detalle',
  'si': 'Si',
  'no': 'No',
  'menu' : {
    'inicio': 'Inicio',
    'violencia-digital': 'Violencia digital',
    'que-es-violencia-genero': '¿Qué es la violencia de género facilitada por la tecnología?',
    'caso-belen':'Caso Belén',
    'en-paraguay': 'En Paraguay'
  },
  'locale-select': {
    'es': 'Español',
    'en': 'Inglés'
  },
  'pages': {
    'politica-de-privacidad': 'Política de privacidad',
    'terminos-y-condiciones': 'Términos y Condiciones',
    'contactos': 'Contactos'
  }
}
